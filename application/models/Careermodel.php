<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Careermodel extends SB_Model 
{

	public $table = 'tb_careers';
	public $primaryKey = 'career_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT tb_careers.* FROM tb_careers   ";
	}
        
	public static function queryWhere(  ){
		
		return "  WHERE tb_careers.career_id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
        
        public function getAll($limit = 0, $limit_offset = 0){
            $query = $this->db->get('tb_careers', $limit, $limit_offset);
            return $query->result_array();
        }
	
}

?>
