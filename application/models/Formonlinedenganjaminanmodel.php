<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Formonlinedenganjaminanmodel extends SB_Model 
{

	public $table = 'tb_formonlinedenganjaminan';
	public $primaryKey = 'idFormOnline';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT tb_formonlinedenganjaminan.* FROM tb_formonlinedenganjaminan   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE tb_formonlinedenganjaminan.idFormOnline IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
