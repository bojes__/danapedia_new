<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ketentuanmodel extends SB_Model 
{

	public $table = 'tb_ketentuan';
	public $primaryKey = 'idKetentuan';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT tb_ketentuan.* FROM tb_ketentuan   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE tb_ketentuan.idKetentuan IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
