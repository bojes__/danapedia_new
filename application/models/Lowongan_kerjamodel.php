<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lowongan_kerjamodel extends SB_Model 
{

	public $table = 'tb_careers';
	public $primaryKey = 'career_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT tb_careers.* FROM tb_careers   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE tb_careers.career_id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
