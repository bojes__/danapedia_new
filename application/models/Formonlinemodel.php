<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Formonlinemodel extends SB_Model 
{

	public $table = 'tb_formonline';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT tb_formonline.* FROM tb_formonline   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE tb_formonline.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
