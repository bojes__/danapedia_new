<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Formcareersmodel extends SB_Model 
{

	public $table = 'tb_careersform';
	public $primaryKey = 'idCareersForm';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT tb_careersform.* FROM tb_careersform   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE tb_careersform.idCareersForm IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
