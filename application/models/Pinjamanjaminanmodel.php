<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pinjamanjaminanmodel extends SB_Model 
{

	public $table = 'tb_pinjamanjaminan';
	public $primaryKey = 'idPinjaman';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT tb_pinjamanjaminan.* FROM tb_pinjamanjaminan   ";
	}
	public static function queryWhere(  ){
		
		return " WHERE tb_pinjamanjaminan.idPinjaman IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
