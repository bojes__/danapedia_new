<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Formonlinetanpajaminanmodel extends SB_Model 
{

	public $table = 'tb_formonlinetanpajaminan';
	public $primaryKey = 'idFormOnline';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT tb_formonlinetanpajaminan.* FROM tb_formonlinetanpajaminan   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE tb_formonlinetanpajaminan.idFormOnline IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
