<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shmmodel extends SB_Model 
{

	public $table = 'tb_shm';
	public $primaryKey = 'idShm';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT tb_shm.* FROM tb_shm   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE tb_shm.idShm IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
