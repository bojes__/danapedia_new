<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Testimonymodel extends SB_Model 
{

	public $table = 'tb_testimonies';
	public $primaryKey = 'testimony_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT tb_testimonies.* FROM tb_testimonies   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE tb_testimonies.testimony_id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
