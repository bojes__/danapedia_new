<?php
//getPinjamanJaminan([field,value,comp])
function getPinjamanJaminan($data){
    $_this =& get_instance();
    
    for($i = 0; $i <count($data); $i++){
        $comp = '';
        if(isset($data[$i]['comp'])){
            $comp = $data[$i]['comp'];
        }
        $_this->db->where($data[$i]['field'].$comp, $data[$i]['value']); 
    }

    $query = $_this->db->get('tb_pinjamanjaminan');
    $rs = $query->result_array();
    return $rs;
}

//getCareer([field,value,comp])
function getCareer($data){
    $_this =& get_instance();
    
    for($i = 0; $i <count($data); $i++){
        $comp = '';
        if(isset($data[$i]['comp'])){
            $comp = $data[$i]['comp'];
        }
        $_this->db->where($data[$i]['field'].$comp, $data[$i]['value']); 
    }

    $query = $_this->db->get('tb_careers');
    $rs = $query->result_array();
    return $rs;
}

function getKetentuan($data){
    $_this =& get_instance();
    
    for($i = 0; $i <count($data); $i++){
        $comp = '';
        if(isset($data[$i]['comp'])){
            $comp = $data[$i]['comp'];
        }
        $_this->db->where($data[$i]['field'].$comp, $data[$i]['value']); 
    }

    $query = $_this->db->get('tb_ketentuan');
    $rs = $query->result_array();
    return $rs;
}

function getBlog(){
    $_this =& get_instance();
    
    
    $_this->db->order_by('created','desc');
    $_this->db->limit(3);
    $query = $_this->db->get('tb_blogs');
    $rs = $query->result_array();
    return $rs;
}

function getTestimonies(){
    $_this =& get_instance();
    
    
    $_this->db->order_by('testimony_id','desc');
    $_this->db->limit(3);
    $query = $_this->db->get('tb_testimonies');
    $rs = $query->result_array();
    return $rs;
}

function dateFormat($dt, $type){
    $dtAll = explode(" ",$dt);
    $dt1 = explode("-",$dtAll[0]);
    $dt2 = explode(":",$dtAll[1]);
    
    //mktime(hour,minute,second,month,day,year)
    $newdt = mktime($dt2[0],$dt2[1],$dt2[2],$dt1[1],$dt1[2],$dt1[0]);
    
    if($type == 1){
        //21 August 2016
        return date("j F Y", $newdt);
    }elseif($type == 2){
        return date("j-n-Y", $newdt);
    }
    
}