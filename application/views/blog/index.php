
<!-- Page Sub-Header -->
<div id="page_header" class="page-subheader site-subheader-cst">
  <div class="bgback"></div>

  <!-- Animated Sparkles -->
  <div class="th-sparkles"></div>
  <!--/ Animated Sparkles -->

    <!-- Background -->
    <div class="kl-bg-source">
        <!-- Gradient overlay -->
        <div class="kl-bg-source__overlay" style="background:rgba(36,36,36,1); background: -moz-linear-gradient(left, rgba(36,36,36,1) 0%, rgba(107,107,107,1) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(36,36,36,1)), color-stop(100%,rgba(107,107,107,1))); background: -webkit-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: -o-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: -ms-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: linear-gradient(135deg,rgb(54,3,132),rgb(218,11,104)); ">
        </div>
        <!--/ Gradient overlay -->
    </div>
    <!--/ Background -->

  <!-- Sub-Header content wrapper -->
  <div class="ph-content-wrap">
    <div class="ph-content-v-center">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <!-- Breadcrumbs -->
            <ul class="breadcrumbs fixclear">
              <li><a href="<?php echo site_url('') ?>">Home</a></li>
              <li class="active"><?php echo $pageTitle ?></li>
            </ul>
            <!-- Breadcrumbs -->

            <!-- Current date -->
            <span id="current-date" class="subheader-currentdate"><?php echo date("M d, Y") ?></span>
            <!--/ Current date -->

            <div class="clearfix"></div>
          </div>
          <!--/ col-sm-6 -->
          <div class="col-sm-6">
            <!-- Sub-header titles -->
            <div class="subheader-titles">
              <h2 class="subheader-maintitle"><?php echo $pageTitle ?></h2>
              <h4 class="subheader-subtitle"><?php echo $pageNote ?></h4>
            </div>
            <!--/ Sub-header titles -->
          </div>
          <!--/ col-sm-6 -->
        </div>
        <!--/ row -->
      </div>
      <!--/ container -->
    </div>
    <!--/ .ph-content-v-center -->
  </div>
  <!--/ Sub-Header content wrapper -->
</div>
<!--/ Page sub-header -->

<!-- Blog page content -->
<section class="hg_section ptop-50">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-9">
				<div class="itemListView clearfix eBlog">
					<div class="itemList">
            <?php
                  if( count( $rowData ) ){
                    foreach ($rowData as $k => $row) {
                      $row = (array) $row;
                      $htm = '
                      <div class="itemContainer">
                        <div class="itemHeader">
                          <h3 class="itemTitle">
                            <a href="'.site_url('blog/read/'.$row['slug']).'" >'.$row['title'].'</a>
                          </h3>
                          
                          <div class="post_details">
                            <span class="catItemDateCreated">'.date('F j, Y', strtotime( $row['created'] )) .'</span>
                            <span class="catItemAuthor">by '.$row['author'].'</span>
                          </div> 
                        </div>
                        
                        <div class="itemBody">
                          <div class="itemIntroText">
                            <div class="hg_post_image">
                              <a href="'.site_url('blog/read/'.$row['slug']).'" class="pull-left" title="'.$row['title'].'">
                                <img src="'.site_url('uploads/blogs/'.$row['image']).'" class="" width="457" height="320" alt="'.$row['title'].'" title="'.$row['title'].'" />
                              </a>
                            </div>
                            
                            '.word_limiter(strip_tags( $row['content'] ),40).'
                          </div>
                          
                          <div class="clear"></div>
                          
                          <div class="itemBottom clearfix">
                            <div class="itemTagsBlock">';

                                $tags = explode( ',',$row['tags']);
                                foreach ( $tags as $l => $tag ){
                                  $htm .= '
                                  <a href="'.site_url('blog/tags/'.str_replace(' ','-',trim($tag))).'" >'.trim($tag).'</a> ';
                                }

                                $htm .='    
                              <div class="clear"></div>
                            </div>
                            
                            <div class="itemReadMore">
                              <a class="btn btn-fullcolor readMore" href="'.site_url('blog/read/'.$row['slug']).'" title="">Read more</a>
                            </div>
                          </div>
                          
                          <div class="clear"></div>
                        </div>
                        
                        <ul class="itemLinks clearfix">
                          <li class="itemCategory">
                          <span class="glyphicon glyphicon-folder-close"></span>
                          <span>Published in</span>
                          <a href="'.site_url('blog/category/'.$row['CatID']).'"><strong>'.$row['CatName'].'</a></li>
                        </ul>
                        
                        <div class="clear">
                        </div>
                      </div>
                      
                      <div class="clear">
                      </div>
                      ';

                      echo $htm;

                    }
                  } else {
                    echo '
                    <div class="headline" >
                      <h3>Article Not found</h3>
                    </div>
                    ';
                  }
            ?>

					</div>
					<!--/ .itemList -->
				</div>
				<!--/ .itemListView -->
			</div>
			<!--/ col-md-9 col-sm-9 -->
			<div class="col-md-3 col-sm-3">
				<?php
    $this->load->view('blog/sidebar');
?>
			</div>
			<!--/ col-md-3 col-sm-3 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Blog page content -->
