
<div id="sidebar-widget" class="sidebar">
  <div class="widget widget_categories">
    <h3 class="widgettitle title">Categories</h3>
    <ul class="menu">
      <?php 
          foreach ( $rowsCategory as $k => $cat ){
            echo  '
            <li class="cat-item"><a href="'.site_url('blog/category/'.$cat['CatID']).'" class="" >'.$cat['name'].'</a> </li>';
          }
      ?>    
    </ul>
  </div>
</div>
