<!-- Page sub-header -->
<div id="page_header" class="page-subheader site-subheader-cst">
  <div class="bgback"></div>

  <!-- Animated Sparkles -->
  <div class="th-sparkles"></div>
  <!--/ Animated Sparkles -->

  <!-- Sub-Header content wrapper -->
  <div class="ph-content-wrap">
    <div class="ph-content-v-center">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <!-- Breadcrumbs -->
            <ul class="breadcrumbs fixclear">
              <li><a href="<?php echo site_url('') ?>">Home</a></li>
              <li><a href="<?php echo site_url('blog') ?>"><?php echo $pageTitle ?></a></li>
              <li class="active"> <?php echo $row['title'] ?> </li>
            </ul>
            <!--/ Breadcrumbs -->

            <!-- Current date -->
            <span id="current-date" class="subheader-currentdate"><?php echo date('F j, Y', strtotime( $row['created'] )) ?></span>
            <!--/ Current date -->

            <div class="clearfix"></div>
          </div>
          <!--/ col-sm-6 -->

          <div class="col-sm-6">
            <!-- Sub-header titles -->
            <div class="subheader-titles">
              <h2 class="subheader-maintitle"><?php echo $pageTitle ?></h2>
              <h4 class="subheader-subtitle"><?php echo $pageNote ?></h4>
            </div>
            <!--/ Sub-header titles -->
          </div>
          <!--/ col-sm-6 -->
        </div>
        <!--/ row -->
      </div>
      <!--/ container -->
    </div>
    <!--/ .ph-content-v-center -->
  </div>
  <!--/ Sub-Header content wrapper -->
</div>
<!--/ Page sub-header -->

<!-- Blog post page content -->
<section class="hg_section ptop-50">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-3">
        <?php
        $this->load->view('blog/sidebar');
        ?>
      </div>
      <!--/ col-md-3 col-sm-3 -->

      <div class="col-md-9 col-sm-9">
        <?php
        if( ( $row ) ){
          $row = (array) $row;
          $htm = '
          <div id="th-content-post">
            <div>
                <h1 class="page-title black">'.$row['title'].'</h1>
                
                <div class="itemView clearfix eBlog">
                  <div class="itemHeader">
                    <div class="post_details">
                      <span class="itemAuthor">
                          by <a href="#"><strong>'.$row['author'].'</strong></a>
                      </span>
                      
                      <span class="infSep"> / </span>
                      
                      <span class="itemDateCreated"><span class="glyphicon glyphicon-calendar"></span>'.date('F j, Y', strtotime( $row['created'] )).'</span>
                      
                      <span class="infSep"> / </span>
                      
                      <span class="itemCommentsBlock"></span>
                      
                      <span class="itemCategory">
                          <span class="glyphicon glyphicon-folder-close"></span>
                          Published in 
                      </span>
                      <a href="'.site_url('blog/category/'.$row['CatID']).'"><strong>'.$row['CatName'].'</a>
                    </div>
                  </div>
                  
                  <!-- Post body -->
                  <div class="itemBody">
                      <!-- Blog Image -->
                      <a data-lightbox="image" href="'.site_url('uploads/blogs/'.$row['image']).'" class="hoverBorder pull-left" style="margin-right: 20px;margin-bottom:4px;">
                          <span class="hoverBorderWrapper">
                              <img src="'.site_url('uploads/blogs/'.$row['image']).'" alt="'.$row['title'].'" title="'.$row['title'].'" />
                              <span class="theHoverBorder"></span>
                          </span>
                      </a>
                      <!--/ Blog Image -->
                      
                      <!-- Blog Content -->
                      '. $row['content'] .'
                      <!--/ Blog Content -->
                  </div>
                  <!--/ Post body -->
                  
                  <div class="clear"></div>
                  
                  <!-- tags blocks -->
                  <div class="itemTagsBlock">
                      <span>Tagged under:</span>';
                          $tags = explode( ',',$row['tags']);
                          foreach ( $tags as $l => $tag ){
                            $htm .= '
                            <a href="'.site_url('blog/tags/'.str_replace(' ','-',trim($tag))).'" >'.trim($tag).'</a> ';
                          }

                          $htm .='
                      <div class="clear"></div>
                  </div>
                  <!--/ tags blocks -->
                  <div class="clear"></div>
                  
                  <!-- Author post -->
                  <div class="post-author">
                      <div class="author-avatar">
                          <img alt="" src="'.site_url('uploads/users/'.$row['avatar']).'"class="avatar avatar-80 photo" height="100" width="100">
                      </div>
                      <div class="author-details">
                          <h4>'.$row['author'].'</h4>
                      </div>
                  </div>
                  <!-- Author post -->
                  <div class="clear"></div>
                </div>
              </div>
              ';
              echo $htm;
        } else {
          echo '
          <div class="headline" >
            <h3>Article Not found</h3>
          </div>
        ';
        }
        ?>
      </div>
      <!--/ col-md-9 col-sm-9 -->
    </div>
    <!--/ row -->
  </div>
  <!--/ container -->
</section>
<!--/ Blog post page content -->
    