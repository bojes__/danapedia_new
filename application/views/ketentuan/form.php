<div class="page-content row">
    <!-- Page header -->
<div class="page-header">
  <div class="page-title">
  <h3> <?php echo $pageTitle ?> <small><?php echo $pageNote ?></small></h3>
  </div>
  <ul class="breadcrumb">
    <li><a href="<?php echo site_url('dashboard') ?>"> Dashboard </a></li>
    <li><a href="<?php echo site_url('ketentuan') ?>"><?php echo $pageTitle ?></a></li>
    <li class="active"> Form </li>
  </ul>      
</div>
 
   <div class="page-content-wrapper m-t">     
    <div class="sbox" >
    <div class="sbox-title" >
      <h5><?php echo $pageTitle ?> <small><?php echo $pageNote ?></small></h5>
    </div>
    <div class="sbox-content" >

      
     <form action="<?php echo site_url('ketentuan/save/'.$row['idKetentuan']); ?>" class='form-vertical'  parsley-validate='true' novalidate='true' method="post" enctype="multipart/form-data" > 


<div class="col-md-12">
						<fieldset><legend> ketentuan</legend>
									
								  <div class="form-group hidethis " style="display:none;">
									<label for="ipt" class=" control-label "> IdKetentuan    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['idKetentuan'];?>' name='idKetentuan'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Tipe  <span class="asterix"> * </span>  </label>									
									  
					<?php $tipe = explode(',',$row['tipe']);
					$tipe_opt = array( 'Dengan Jaminan' => 'Dengan Jaminan' ,  'Tanpa Jaminan' => 'Tanpa Jaminan' , ); ?>
					<select name='tipe' rows='5' required  class='select2 '  > 
						<?php 
						foreach($tipe_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['tipe'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Parameter  <span class="asterix"> * </span>  </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['parameter'];?>' name='parameter'  required /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Ketentuan    </label>									
									  <textarea name='ketentuan' rows='2' id='editor' class='form-control markItUp '  
						 ><?php echo $row['ketentuan'] ;?></textarea> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Deskripsi    </label>									
									  <textarea name='deskripsi' rows='2' id='editor' class='form-control markItUp '  
						 ><?php echo $row['deskripsi'] ;?></textarea> 						
								  </div> </fieldset>
			</div>
			
			
    
      <div style="clear:both"></div>  
        
     <div class="toolbar-line text-center">    
      <input type="submit" name="apply" class="btn btn-info btn-sm" value="<?php echo $this->lang->line('core.btn_apply'); ?>" />
      <input type="submit" name="submit" class="btn btn-primary btn-sm" value="<?php echo $this->lang->line('core.btn_submit'); ?>" />
      <a href="<?php echo site_url('ketentuan');?>" class="btn btn-sm btn-warning"><?php echo $this->lang->line('core.btn_cancel'); ?> </a>
     </div>
            
    </form>
    
    </div>
    </div>

  </div>  
</div>  
</div>
       
<script type="text/javascript">
$(document).ready(function() { 
    
});
</script>     