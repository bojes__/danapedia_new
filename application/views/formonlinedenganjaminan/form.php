<div class="page-content row">
    <!-- Page header -->
<div class="page-header">
  <div class="page-title">
  <h3> <?php echo $pageTitle ?> <small><?php echo $pageNote ?></small></h3>
  </div>
  <ul class="breadcrumb">
    <li><a href="<?php echo site_url('dashboard') ?>"> Dashboard </a></li>
    <li><a href="<?php echo site_url('formonlinedenganjaminan') ?>"><?php echo $pageTitle ?></a></li>
    <li class="active"> Form </li>
  </ul>      
</div>
 
   <div class="page-content-wrapper m-t">     
    <div class="sbox" >
    <div class="sbox-title" >
      <h5><?php echo $pageTitle ?> <small><?php echo $pageNote ?></small></h5>
    </div>
    <div class="sbox-content" >

      
     <form action="<?php echo site_url('formonlinedenganjaminan/save/'.$row['idFormOnline']); ?>" class='form-vertical'  parsley-validate='true' novalidate='true' method="post" enctype="multipart/form-data" > 


<div class="col-md-12">
						<fieldset><legend> formonlinedenganjaminan</legend>
									
								  <div class="form-group hidethis " style="display:none;">
									<label for="ipt" class=" control-label "> IdFormOnline    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['idFormOnline'];?>' name='idFormOnline'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Tanggal    </label>									
									  
				<input type='text' class='form-control datetime' placeholder='' value='<?php echo $row['tanggal'];?>' name='tanggal'
				style='width:150px !important;'	   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Nama    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['nama'];?>' name='nama'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Alamat    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['alamat'];?>' name='alamat'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Hp    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['hp'];?>' name='hp'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Wa    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['wa'];?>' name='wa'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Bbm    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['bbm'];?>' name='bbm'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Facebook    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['facebook'];?>' name='facebook'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> PenghasilanPerbulan    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['penghasilanPerbulan'];?>' name='penghasilanPerbulan'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> KepemilikanRumah    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['kepemilikanRumah'];?>' name='kepemilikanRumah'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> JumlahPinjaman    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['jumlahPinjaman'];?>' name='jumlahPinjaman'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Tenor    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['tenor'];?>' name='tenor'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> JenisAgunan    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['jenisAgunan'];?>' name='jenisAgunan'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Merek    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['merek'];?>' name='merek'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Tipe    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['tipe'];?>' name='tipe'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Tahun    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['tahun'];?>' name='tahun'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> PlatNomor    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['platNomor'];?>' name='platNomor'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> KondisiPajak    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['kondisiPajak'];?>' name='kondisiPajak'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> BpkbAtasNama    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['bpkbAtasNama'];?>' name='bpkbAtasNama'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> PosisiBpkb    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['posisiBpkb'];?>' name='posisiBpkb'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> KebutuhanPrioritas    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['kebutuhanPrioritas'];?>' name='kebutuhanPrioritas'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Multifinance    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['multifinance'];?>' name='multifinance'   /> 						
								  </div> </fieldset>
			</div>
			
			
    
      <div style="clear:both"></div>  
        
     <div class="toolbar-line text-center">    
      <input type="submit" name="apply" class="btn btn-info btn-sm" value="<?php echo $this->lang->line('core.btn_apply'); ?>" />
      <input type="submit" name="submit" class="btn btn-primary btn-sm" value="<?php echo $this->lang->line('core.btn_submit'); ?>" />
      <a href="<?php echo site_url('formonlinedenganjaminan');?>" class="btn btn-sm btn-warning"><?php echo $this->lang->line('core.btn_cancel'); ?> </a>
     </div>
            
    </form>
    
    </div>
    </div>

  </div>  
</div>  
</div>
       
<script type="text/javascript">
$(document).ready(function() { 
    
});
</script>     