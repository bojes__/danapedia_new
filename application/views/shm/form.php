<div class="page-content row">
    <!-- Page header -->
<div class="page-header">
  <div class="page-title">
  <h3> <?php echo $pageTitle ?> <small><?php echo $pageNote ?></small></h3>
  </div>
  <ul class="breadcrumb">
    <li><a href="<?php echo site_url('dashboard') ?>"> Dashboard </a></li>
    <li><a href="<?php echo site_url('shm') ?>"><?php echo $pageTitle ?></a></li>
    <li class="active"> Form </li>
  </ul>      
</div>
 
   <div class="page-content-wrapper m-t">     
    <div class="sbox" >
    <div class="sbox-title" >
      <h5><?php echo $pageTitle ?> <small><?php echo $pageNote ?></small></h5>
    </div>
    <div class="sbox-content" >

      
     <form action="<?php echo site_url('shm/save/'.$row['idShm']); ?>" class='form-vertical'  parsley-validate='true' novalidate='true' method="post" enctype="multipart/form-data" > 


<div class="col-md-12">
						<fieldset><legend> shm</legend>
									
								  <div class="form-group hidethis " style="display:none;">
									<label for="ipt" class=" control-label "> IdShm    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['idShm'];?>' name='idShm'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Multifinance    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['multifinance'];?>' name='multifinance'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Jenis Aset    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['jenisAset'];?>' name='jenisAset'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Usia Aset    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['usiaAset'];?>' name='usiaAset'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Dana Cair    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['danaCair'];?>' name='danaCair'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Bunga    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['bungaFlatPerBulan'];?>' name='bungaFlatPerBulan'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Tenor    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['tenor'];?>' name='tenor'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Biaya    </label>									
									  <textarea name='biaya' rows='2' id='editor' class='form-control markItUp '  
						 ><?php echo $row['biaya'] ;?></textarea> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Lama Proses    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['lamaProses'];?>' name='lamaProses'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Asuransi    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['asuransi'];?>' name='asuransi'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Take Over    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['takeOver'];?>' name='takeOver'   /> 						
								  </div> </fieldset>
			</div>
			
			
    
      <div style="clear:both"></div>  
        
     <div class="toolbar-line text-center">    
      <input type="submit" name="apply" class="btn btn-info btn-sm" value="<?php echo $this->lang->line('core.btn_apply'); ?>" />
      <input type="submit" name="submit" class="btn btn-primary btn-sm" value="<?php echo $this->lang->line('core.btn_submit'); ?>" />
      <a href="<?php echo site_url('shm');?>" class="btn btn-sm btn-warning"><?php echo $this->lang->line('core.btn_cancel'); ?> </a>
     </div>
            
    </form>
    
    </div>
    </div>

  </div>  
</div>  
</div>
       
<script type="text/javascript">
$(document).ready(function() { 
    
});
</script>     