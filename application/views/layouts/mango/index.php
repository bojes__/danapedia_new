<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
<title> <?php echo isset($page['pageTitle']) ? $page['pageTitle'].' | '.CNF_APPNAME : CNF_APPNAME ;?> </title>
<meta name="keywords" content="<?php echo CNF_METAKEY ?>">
<meta name="description" content=""<?php echo CNF_METADESC ?>"/>

	<link href="<?php echo base_url().'dp/images/favicons/favicon-16x16.png';?>" rel="icon" type="image/png" />
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400italic,400,600,600italic,700,800,800italic" rel="stylesheet" type="text/css">
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link href="<?php echo base_url().'dp/css/bootstrap.css';?>" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo base_url().'dp/css/sliders/ios/style.css';?>" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo base_url()?>dp/css/template.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo base_url()?>dp/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>dp/css/base-sizing.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>dp/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>dp/css/style.css" rel="stylesheet" type="text/css" />
    

	<script type="text/javascript" src="<?php echo base_url()?>dp/js/modernizr.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>dp/js/jquery.js"></script>



		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
  	</head>

<body class="">

	<!-- Support Panel -->
	<input type="checkbox" id="support_p" class="panel-checkbox">
	<div class="support_panel">
		<div class="support-close-inner">
			<label for="support_p" class="spanel-label inner">
				<span class="support-panel-close">×</span>
			</label>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<!-- Title -->
					<h4 class="m_title">HOW TO SHOP</h4>

					<!-- Content - how to shop steps -->
					<div class="m_content how_to_shop">
						<div class="row">
							<div class="col-sm-4">
								<span class="number">1</span> Login or create new account.
							</div>
							<!--/ col-sm-4 -->

							<div class="col-sm-4">
								<span class="number">2</span> Review your order.
							</div>
							<!--/ col-sm-4 -->

							<div class="col-sm-4">
								<span class="number">3</span> Payment &amp; <strong>FREE</strong> shipment
							</div>
							<!--/ col-sm-4 -->
						</div>
						<!--/ row -->

						<p>If you still have problems, please let us know, by sending an email to support@website.com . Thank you!</p>
					</div>
					<!--/ Content - how to shop steps -->
				</div>
				<!--/ col-sm-9 -->

				<div class="col-sm-3">
					<!-- Title -->
					<h4 class="m_title">SHOWROOM HOURS</h4>

					<!-- Content -->
					<div class="m_content">
						Mon-Fri 9:00AM - 6:00AM<br>
						Sat - 9:00AM-5:00PM<br>
						Sundays by appointment only!
					</div>
					<!--/ Content -->
				</div>
				<!--/ col-sm-3 -->
			</div>
			<!--/ row -->
		</div>
		<!--/ container -->
	</div>
	<!--/ Support Panel -->

	<!-- Page Wrapper -->
	<div id="page_wrapper">
		<!-- Header style 1 -->
		<header id="header" class="site-header style1 cta_button">
			<!-- header bg -->
			<div class="kl-header-bg"></div>
			<!--/ header bg -->

			<!-- siteheader-container -->
			<div class="container siteheader-container">
				<!-- top-header -->
				<div class="kl-top-header clearfix">
					<!-- HEADER ACTION -->
					<div class="header-links-container ">
						<ul class="topnav navRight topnav">
							<!-- Support panel trigger -->
							<li>
								<label for="support_p" class="spanel-label">
									<i class="glyphicon glyphicon-info-sign icon-white support-info visible-xs xs-icon"></i>
									<span class="hidden-xs">SUPPORT</span>
								</label>
							</li>
							<!--/ Support panel trigger -->
						</ul>
					</div>
					<!--/ HEADER ACTION -->

					<!-- HEADER ACTION left -->
					<div class="header-leftside-container ">
						<!-- Header Social links -->
						<ul class="social-icons sc--clean topnav navRight">
							<li><a href="#" target="_self" class="icon-facebook" title="Facebook"></a></li>
							<li><a href="#" target="_self" class="icon-twitter" title="Twitter"></a></li>
							<li><a href="#" target="_blank" class="icon-linkedin" title="Linked In"></a></li>
						</ul>
						<!--/ Header Social links -->

						<div class="clearfix visible-xxs">
						</div>

						<!-- header contact text -->
						<span class="kl-header-toptext">QUESTIONS? CALL: <a href="#" class="fw-bold">0812 7277 6204</a></span>
						<!--/ header contact text -->
					</div>
					<!--/ HEADER ACTION left -->
				</div>
				<!--/ top-header -->

				<!-- separator -->
				<div class="separator"></div>
				<!--/ separator -->

				<!-- left side -->
				<!-- logo container-->
				<div class="logo-container hasInfoCard logosize--yes">
					<!-- Logo -->
					<h1 class="site-logo logo" id="logo">
						<a href="<?php echo base_url() ?>" title="">
							<img src="<?php echo base_url().'dp/images/DANAPEDIA-logo.png' ?>" class="logo-img" alt="Danapedia" title="Danapedia" />
						</a>
					</h1>
					<!--/ Logo -->

					<!-- InfoCard -->
					<div id="infocard" class="logo-infocard">
						<div class="custom">
							<div class="row">
								<div class="col-sm-5">
									<p>&nbsp;
										
									</p>
									<p style="text-align: center;">
										<img src="<?php echo base_url().'dp/images/danapedia_icon.png' ?>" class="" alt="Danapedia" title="Danapedia" />
									</p>
									<p style="text-align: center;">
										Danapedia adalah perusahaan Financial E-Commerce Resmi.
									</p>
								</div>
								<!--/ col-sm-5 -->

								<div class="col-sm-7">
									<div class="custom contact-details">
										<p>
											<strong>T (+62) 812 7277 6204</strong><br>
											Email:&nbsp;<a href="mailto:info@danapedia.com">info@danapedia.com</a>
										</p>
										<p>
											Danapedia<br>
											Taman Topi Square, lt 1 Blok G no 6. Jln Kapten Muslihat - Paledang, Kota Bogor
										</p>
										<a href="https://goo.gl/maps/GWhLqzyCWhq" class="map-link" target="_blank" title="">
											<span class="glyphicon glyphicon-map-marker icon-white"></span>
											<span>Open in Google Maps</span>
										</a>
									</div>
									<div style="height:20px;">
									</div>
									<!-- Social links clean style -->
									<ul class="social-icons sc--clean">
										<li><a href="#" target="_self" class="icon-twitter" title="Twitter"></a></li>
										<li><a href="#" target="_self" class="icon-facebook" title="Facebook"></a></li>
										<li><a href="#" target="_self" class="icon-instagram" title="Instagram"></a></li>
										<li><a href="#" target="_blank" class="icon-google" title="Google Plus"></a></li>
									</ul>
									<!--/ Social links clean style -->
								</div>
								<!--/ col-sm-7 -->
							</div>
							<!--/ row -->
						</div>
						<!--/ custom -->
					</div>
					<!--/ InfoCard -->

				</div>
				<!--/ logo container-->

				<!-- separator -->
				<div class="separator visible-xxs"></div>
				<!--/ separator -->

				<!-- responsive menu trigger -->
				<div id="zn-res-menuwrapper">
					<a href="#" class="zn-res-trigger zn-header-icon"></a>
				</div>
				<!--/ responsive menu trigger -->

				<!-- main menu -->
				<div id="main-menu" class="main-nav zn_mega_wrapper ">
					<?php $this->load->view('layouts/topbar');?>

				</div>
				<!--/ main menu -->

				<!-- right side -->
				<!-- Call to action ribbon Free Quote -->
				<a href="<?php echo site_url('ajukan-sekarang-jaminan') ;?>" id="ctabutton" class="ctabutton kl-cta-ribbon" title="GET A FREE QUOTE" target="_self"><strong>AJUKAN</strong>SEKARANG<svg version="1.1" class="trisvg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" preserveaspectratio="none" width="14px" height="5px" viewbox="0 0 14.017 5.006" enable-background="new 0 0 14.017 5.006" xml:space="preserve"><path fill-rule="evenodd" clip-rule="evenodd" d="M14.016,0L7.008,5.006L0,0H14.016z"></path></svg></a>
				<!--/ Call to action ribbon Free Quote -->



			</div>
			<!--/ siteheader-container -->
		</header>
		<!-- / Header style 1 -->

		<?php echo $content ?>

		<!-- Footer - Default Style -->
		<footer id="footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div>
							<h3 class="title m_title">HUBUNGI KAMI</h3>
							<div class="contact-details"><p><strong>T (212) 555 55 00</strong><br>
									Email: <a href="#">sales@yourwebsite.com</a></p>
								<p>DANAPEDIA<br>
									Taman Topi Square, lt 1 Blok G no 6. Jln Kapten Muslihat - Paledang, Kota Bogor</p>
								<p><a href="https://goo.gl/maps/GWhLqzyCWhq"></i> Open in Google Maps</a></p>
							</div>
						</div>
					</div>
					<!--/ col-sm-3 -->

					<div class="col-sm-5">
						
					</div>
					<!--/ col-sm-5 -->

					<div class="col-sm-4">
						<div class="newsletter-signup">
							<h3 class="title m_title">DAPATKAN TIPS KEUANGAN GRATIS</h3>
							<p>Daftarkan alamat email anda untuk mendapatkan tips keuangan terupdate.</p>
							<form action="http://YOUR_USERNAME.DATASERVER.list-manage1.com/subscribe/post-json?u=YOUR_API_KEY&amp;id=LIST_ID&amp;c=?" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
								<input type="email" value="" name="EMAIL" class="nl-email form-control" id="mce-EMAIL" placeholder="your.address@email.com" required>
		                        <input type="submit" name="subscribe" class="nl-submit" id="mc-embedded-subscribe" value="DAFTARKAN">
		                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		                        <div style="position: absolute; left: -5000px;">
		                            <input type="text" name="b_9ed253451c5b914e9c34e388b_6e3ba686ba" value="">
		                        </div>
							</form>
							<div id="notification_container"></div>
							<p>We never spam!</p>
						</div><!-- end newsletter-signup -->
					</div>
					<!-- col-sm-4 -->


				</div>
				<!--/ row -->

				<div class="row">



					<!--/ col-sm-6 -->
				</div>
				<!--/ row -->

				<div class="row">
					<div class="col-sm-12">
						<div class="bottom clearfix">
							<!-- social-icons -->
							<ul class="social-icons sc--clean clearfix">
								<li class="title">GET SOCIAL</li>
								<li><a href="#" target="_self" class="icon-facebook" title="Facebook"></a></li>
								<li><a href="#" target="_self" class="icon-twitter" title="Twitter"></a></li>
								<li><a href="#" target="_blank" class="icon-linkedin" title="Linked In"></a></li>
							</ul>
							<!--/ social-icons -->

							<!-- copyright -->
							<div class="copyright">
								<a href="index.html">
									<img src="<?php echo base_url().'dp/images/DANAPEDIA-logo.png' ?>" alt="Danapedia">
								</a>
								<p>© 2016 Danapedia All rights reserved.</p>
							</div>
							<!--/ copyright -->
						</div>
						<!--/ bottom -->
					</div>
					<!--/ col-sm-12 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ container -->
		</footer>
		<!--/ Footer - Default Style -->
	</div>
	<!--/ Page Wrapper -->

	<!-- Bubble-box with notification-box style -->
	<div class="bubble-box notification-box bg-purple" data-reveal-at="1200" data-hide-after="9000">
		<div class="bb--inner">
			<p>This is just a simple notice. Everything is in order and this is a <a href="#">simple link.</a></p>
		</div>
		<span class="bb--close"><i class="glyphicon glyphicon-remove"></i></span>
	</div>
	<!-- / Bubble-box with notification-box style -->


	<!-- Login Panel content -->
	<div id="login_panel" class="mfp-hide loginbox-popup auth-popup">
		<div class="inner-container login-panel auth-popup-panel">
			<h3 class="m_title m_title_ext text-custom auth-popup-title tcolor">SIGN IN YOUR ACCOUNT TO HAVE ACCESS TO DIFFERENT FEATURES</h3>
			<form class="login_panel" name="login_form" method="post" action="#">
				<div class="form-group kl-fancy-form">
					<input type="text" id="kl-username" name="log" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="eg: james_smith">
					<label class="kl-font-alt kl-fancy-form-label">USERNAME</label>
				</div>
				<div class="form-group kl-fancy-form">
					<input type="password" id="kl-password" name="pwd" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="type password">
					<label class="kl-font-alt kl-fancy-form-label">PASSWORD</label>
				</div>
				<label class="auth-popup-remember" for="kl-rememberme"><input type="checkbox" name="rememberme" id="kl-rememberme" value="forever" class="auth-popup-remember-chb"> Remember Me </label>
				<input type="submit" id="login" name="submit_button" class="btn zn_sub_button btn-fullcolor btn-md" value="LOG IN">
				<input type="hidden" value="login" class="" name="form_action"><input type="hidden" value="login" class="" name="action">
				<input type="hidden" value="#" class="" name="submit">
				<div class="links auth-popup-links">
					<a href="#register_panel" class="create_account auth-popup-createacc kl-login-box auth-popup-link">CREATE AN ACCOUNT</a><span class="sep auth-popup-sep"></span><a href="#forgot_panel" class="kl-login-box auth-popup-link">FORGOT YOUR PASSWORD?</a>
				</div>
			</form>
		</div>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	</div>
	<div id="register_panel" class="mfp-hide loginbox-popup auth-popup">
		<div class="inner-container register-panel auth-popup-panel">
			<h3 class="m_title m_title_ext text-custom auth-popup-title">CREATE ACCOUNT</h3>
			<form class="register_panel" name="login_form" method="post" action="#">
				<div class="form-group kl-fancy-form ">
					<input type="text" id="reg-username" name="user_login" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="type desired username"><label class="kl-font-alt kl-fancy-form-label">USERNAME</label>
				</div>
				<div class="form-group kl-fancy-form">
					<input type="text" id="reg-email" name="user_email" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="your-email@website.com"><label class="kl-font-alt kl-fancy-form-label">EMAIL</label>
				</div>
				<div class="form-group kl-fancy-form">
					<input type="password" id="reg-pass" name="user_password" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="*****"><label class="kl-font-alt kl-fancy-form-label">PASSWORD</label>
				</div>
				<div class="form-group kl-fancy-form">
					<input type="password" id="reg-pass2" name="user_password2" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="*****"><label class="kl-font-alt kl-fancy-form-label">CONFIRM PASSWORD</label>
				</div>
				<div class="form-group">
					<input type="submit" id="signup" name="submit" class="btn zn_sub_button btn-block btn-fullcolor btn-md" value="CREATE MY ACCOUNT">
				</div>
				<div class="links auth-popup-links">
					<a href="#login_panel" class="kl-login-box auth-popup-link">ALREADY HAVE AN ACCOUNT?</a>
				</div>
			</form>
		</div>
	</div>
	<div id="forgot_panel" class="mfp-hide loginbox-popup auth-popup forgot-popup">
		<div class="inner-container forgot-panel auth-popup-panel">
			<h3 class="m_title m_title_ext text-custom auth-popup-title">FORGOT YOUR DETAILS?</h3>
			<form class="forgot_form" name="login_form" method="post" action="#">
				<div class="form-group kl-fancy-form">
					<input type="text" id="forgot-email" name="user_login" class="form-control inputbox kl-fancy-form-input kl-fw-input" placeholder="...">
					<label class="kl-font-alt kl-fancy-form-label">USERNAME OR EMAIL</label>
				</div>
				<div class="form-group">
					<input type="submit" id="recover" name="submit" class="btn btn-block zn_sub_button btn-fullcolor btn-md" value="SEND MY DETAILS!">
				</div>
				<div class="links auth-popup-links">
					<a href="#login_panel" class="kl-login-box auth-popup-link">AAH, WAIT, I REMEMBER NOW!</a>
				</div>
			</form>
		</div>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	</div>
	<!--/ Login Panel content -->


	<!-- ToTop trigger -->
	<a href="#" id="totop">TOP</a>
	<!--/ ToTop trigger -->

	<!-- JS FILES // These should be loaded in every page -->
	<script type="text/javascript" src="<?php echo base_url()?>dp/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>dp/js/kl-plugins.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>dp/js/plugins/scrollme/jquery.scrollme.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>dp/js/plugins/_sliders/ios/jquery.iosslider.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>dp/js/trigger/slider/ios/kl-ios-slider.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>dp/js/plugins/_sliders/caroufredsel/jquery.carouFredSel-packed.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>dp/js/trigger/kl-screenshot-box.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>dp/js/trigger/kl-partners-carousel.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>dp/js/trigger/kl-logo-carousel.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>dp/js/kl-scripts.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>dp/js/kl-custom.js"></script>

	<script type="text/javascript">
		//use the modernizr load to load up external scripts. This will load the scripts asynchronously, but the order listed matters. Although it will load all scripts in parallel, it will execute them in the order listed
		Modernizr.load([
			{
				// test for media query support, if not load respond.js
				test : Modernizr.mq('only all'),
				// If not, load the respond.js file
				nope : '//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js'
			}
		]);
	</script>
</body>
</html>