<div class="page-content row">
    <!-- Page header -->
<div class="page-header">
  <div class="page-title">
  <h3> <?php echo $pageTitle ?> <small><?php echo $pageNote ?></small></h3>
  </div>
  <ul class="breadcrumb">
    <li><a href="<?php echo site_url('dashboard') ?>"> Dashboard </a></li>
    <li><a href="<?php echo site_url('lowongan_kerja') ?>"><?php echo $pageTitle ?></a></li>
    <li class="active"> Form </li>
  </ul>      
</div>
 
   <div class="page-content-wrapper m-t">     
    <div class="sbox" >
    <div class="sbox-title" >
      <h5><?php echo $pageTitle ?> <small><?php echo $pageNote ?></small></h5>
    </div>
    <div class="sbox-content" >

      
     <form action="<?php echo site_url('lowongan_kerja/save/'.$row['career_id']); ?>" class='form-vertical'  parsley-validate='true' novalidate='true' method="post" enctype="multipart/form-data" > 


<div class="col-md-12">
						<fieldset><legend> Lowongan Kerja</legend>
									
								  <div class="form-group hidethis " style="display:none;">
									<label for="ipt" class=" control-label "> Career Id    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['career_id'];?>' name='career_id'   /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Title  <span class="asterix"> * </span>  </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['title'];?>' name='title'  required /> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Desc  <span class="asterix"> * </span>  </label>									
									  <textarea name='desc' rows='2' id='editor' class='form-control markItUp '  
						required ><?php echo $row['desc'] ;?></textarea> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Skill Requirement  <span class="asterix"> * </span>  </label>									
									  <textarea name='skill_requirement' rows='2' id='editor' class='form-control markItUp '  
						required ><?php echo $row['skill_requirement'] ;?></textarea> 						
								  </div> 					
								  <div class="form-group  " >
									<label for="ipt" class=" control-label "> Location    </label>									
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['location'];?>' name='location'   /> 						
								  </div> </fieldset>
			</div>
			
			
    
      <div style="clear:both"></div>  
        
     <div class="toolbar-line text-center">    
      <input type="submit" name="apply" class="btn btn-info btn-sm" value="<?php echo $this->lang->line('core.btn_apply'); ?>" />
      <input type="submit" name="submit" class="btn btn-primary btn-sm" value="<?php echo $this->lang->line('core.btn_submit'); ?>" />
      <a href="<?php echo site_url('lowongan_kerja');?>" class="btn btn-sm btn-warning"><?php echo $this->lang->line('core.btn_cancel'); ?> </a>
     </div>
            
    </form>
    
    </div>
    </div>

  </div>  
</div>  
</div>
       
<script type="text/javascript">
$(document).ready(function() { 
    
});
</script>     