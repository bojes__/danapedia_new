<!-- Slideshow - iOS Slider element with animateme scroll efect, custom height and bottom mask style 2 -->
<div class="kl-slideshow iosslider-slideshow uh_light_gray maskcontainer--shadow_ud iosslider--custom-height scrollme kl-slider-loaded pb-47">
	<!-- Loader -->
	<div class="kl-loader">
		<svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewbox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946 s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634 c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"></path><path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0 C22.32,8.481,24.301,9.057,26.013,10.047z" transform="rotate(98.3774 20 20)"><animatetransform attributetype="xml" attributename="transform" type="rotate" from="0 20 20" to="360 20 20" dur="0.5s" repeatcount="indefinite"></animatetransform></path></svg>
	</div>
	<!-- Loader -->

	<div class="bgback">
	</div>

	<!-- Animated Sparkles -->
	<div class="th-sparkles">
	</div>
	<!--/ Animated Sparkles -->

	<!-- iOS Slider wrapper with animateme scroll efect -->
	<div class="iosSlider kl-slideshow-inner animateme" data-trans="6000" data-autoplay="1" data-infinite="true" data-when="span" data-from="0" data-to="0.75" data-translatey="300" data-easing="linear">
		<!-- Slides -->
		<div class="kl-iosslider hideControls">
			<!-- Slide 1 -->
			<div class="item iosslider__item">
				<!-- Image -->
				<div class="slide-item-bg" style="background-image:url(<?php echo base_url().'dp/images/sliders/slide3.jpg' ?>);">
				</div>
				<!--/ Image -->

				<!-- Gradient overlay -->
				<div class="kl-slide-overlay" style="background:rgba(32,55,152,0.4); background: -moz-linear-gradient(left, rgba(32,55,152,0.4) 0%, rgba(17,93,131,0.25) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(32,55,152,0.4)), color-stop(100%,rgba(17,93,131,0.25))); background: -webkit-linear-gradient(left, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); background: -o-linear-gradient(left, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); background: -ms-linear-gradient(left, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); background: linear-gradient(to right, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); ">
				</div>
				<!--/ Gradient overlay -->

				<!-- Captions container -->
				<div class="container kl-iosslide-caption kl-ioscaption--style5 fromleft klios-alignleft kl-caption-posv-middle">
					<!-- Captions animateme wrapper -->
					<div class="animateme" data-when="span" data-from="0" data-to="0.75" data-opacity="0.1" data-easing="linear">
						<!-- Main Big Title -->
						<h3 class="main_title has_titlebig "><span>Dana Cair 1 Juta s/d 10 Milyar.<br>
						Bunga mulai 0,8% /bulan<br>
						Proses 1 hari<br>
						Tanpa BI checking</span></h3>
						<!--/ Main Big Title -->

						<!-- Big Title -->
						<h4 class="title_big">PELAJARI PRODUK PINJAMAN TERBAIK</h4>
						<!--/ Big Title -->

						<!-- Link more buttons -->
						<div class="more">
							<!-- Button full color style -->
							<a class="btn btn-fullcolor " href="<?php echo site_url('product1') ;?>" target="_self">DENGAN JAMINAN</a>
							<!--/ Button full color style -->

							<!-- Button full lined style -->
							<a class="btn btn-lined " href="<?php echo site_url('product2') ;?>" target="_self">TANPA JAMINAN</a>
							<!--/ Button full lined style -->
						</div>
						<!--/ Link more buttons -->

						<!-- Small Title -->
						<h4 class="title_small">DAFTAR DAN DAPATKAN PENAWARAN TERBAIK DARI KAMI.</h4>
						<!--/ Small Title -->
					</div>
					<!--/ Captions animateme wrapper -->
				</div>
				<!--/ Captions container -->
			</div>
			<!--/ Slide 1 -->

			<!-- Slide 2 -->
			<div class="item iosslider__item">
				<!-- Image -->
				<div class="slide-item-bg" style="background-image:url(<?php echo base_url().'dp/images/sliders/slide2.jpg' ?>);">
				</div>
				<!--/ Image -->

				<!-- Gradient overlay -->
				<div class="kl-slide-overlay" style="background:rgba(32,55,152,0.4); background: -moz-linear-gradient(left, rgba(32,55,152,0.4) 0%, rgba(17,93,131,0.25) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(32,55,152,0.4)), color-stop(100%,rgba(17,93,131,0.25))); background: -webkit-linear-gradient(left, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); background: -o-linear-gradient(left, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); background: -ms-linear-gradient(left, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); background: linear-gradient(to right, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); ">
				</div>
				<!--/ Gradient overlay -->

				<!-- Captions container -->
				<div class="container kl-iosslide-caption kl-ioscaption--style5 fromleft klios-alignleft kl-caption-posv-middle">
					<!-- Captions animateme wrapper -->
					<div class="animateme" data-when="span" data-from="0" data-to="0.75" data-opacity="0.1" data-easing="linear">
						<!-- Main Big Title -->
						<h3 class="main_title has_titlebig "><span>Dana Cair 1 Juta s/d 10 Milyar.<br>
						Bunga mulai 0,8% /bulan<br>
						Proses 1 hari<br>
						Tanpa BI checking</span></h3>
						<!--/ Main Big Title -->

						<!-- Big Title -->
						<h4 class="title_big">PELAJARI PRODUK PINJAMAN TERBAIK</h4>
						<!--/ Big Title -->

						<!-- Link more buttons -->
						<div class="more">
							<!-- Button full color style -->
							<a class="btn btn-fullcolor " href="#" target="_self">DENGAN JAMINAN</a>
							<!--/ Button full color style -->

							<!-- Button full lined style -->
							<a class="btn btn-lined " href="#" target="_self">TANPA JAMINAN</a>
							<!--/ Button full lined style -->
						</div>
						<!--/ Link more buttons -->

						<!-- Small Title -->
						<h4 class="title_small">DAFTAR DAN DAPATKAN PENAWARAN TERBAIK DARI KAMI.</h4>
						<!--/ Small Title -->
					</div>
					<!--/ Captions animateme wrapper -->
				</div>
				<!--/ Captions container -->
			</div>
			<!--/ Slide 2 -->

			<!-- Slide 3 -->
			<div class="item iosslider__item">
				<!-- Image -->
				<div class="slide-item-bg" style="background-image:url(<?php echo base_url().'dp/images/sliders/slide1.jpg' ?>);">
				</div>
				<!--/ Image -->

				<!-- Gradient overlay -->
				<div class="kl-slide-overlay" style="background:rgba(32,55,152,0.4); background: -moz-linear-gradient(left, rgba(32,55,152,0.4) 0%, rgba(17,93,131,0.25) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(32,55,152,0.4)), color-stop(100%,rgba(17,93,131,0.25))); background: -webkit-linear-gradient(left, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); background: -o-linear-gradient(left, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); background: -ms-linear-gradient(left, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); background: linear-gradient(to right, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); ">
				</div>
				<!--/ Gradient overlay -->

				<!-- Captions container -->
				<div class="container kl-iosslide-caption kl-ioscaption--style5 fromleft klios-alignleft kl-caption-posv-middle">
					<!-- Captions animateme wrapper -->
					<div class="animateme" data-when="span" data-from="0" data-to="0.75" data-opacity="0.1" data-easing="linear">
						<!-- Main Big Title -->
						<h3 class="main_title has_titlebig "><span>Dana Cair 1 Juta s/d 10 Milyar.<br>
						Bunga mulai 0,8% /bulan<br>
						Proses 1 hari<br>
						Tanpa BI checking</span></h3>
						<!--/ Main Big Title -->

						<!-- Big Title -->
						<h4 class="title_big">PELAJARI PRODUK PINJAMAN TERBAIK</h4>
						<!--/ Big Title -->

						<!-- Link more buttons -->
						<div class="more">
							<!-- Button full color style -->
							<a class="btn btn-fullcolor " href="#" target="_self">DENGAN JAMINAN</a>
							<!--/ Button full color style -->

							<!-- Button full lined style -->
							<a class="btn btn-lined " href="#" target="_self">TANPA JAMINAN</a>
							<!--/ Button full lined style -->
						</div>
						<!--/ Link more buttons -->

						<!-- Small Title -->
						<h4 class="title_small">DAFTAR DAN DAPATKAN PENAWARAN TERBAIK DARI KAMI.</h4>
						<!--/ Small Title -->
					</div>
					<!--/ Captions animateme wrapper -->
				</div>
				<!--/ Captions container -->
			</div>
			<!--/ Slide 3 -->
		</div>
		<!--/ Slides -->

		<!-- Navigation Controls - Prev -->
		<div class="kl-iosslider-prev">
			<!-- Arrow -->
			<span class="thin-arrows ta__prev"></span>
			<!--/ Arrow -->

			<!-- Label - prev -->
			<div class="btn-label">
				PREV
			</div>
			<!--/ Label - prev -->
		</div>
		<!--/ Navigation Controls - Prev -->

		<!-- Navigation Controls - Next -->
		<div class="kl-iosslider-next">
			<!-- Arrow -->
			<span class="thin-arrows ta__next"></span>
			<!--/ Arrow -->

			<!-- Label - next -->
			<div class="btn-label">
				NEXT
			</div>
			<!--/ Label - next -->
		</div>
		<!--/ Navigation Controls - Prev -->
	</div>
	<!--/ iOS Slider wrapper with animateme scroll efect -->

	<!-- Bullets -->
	<div class="kl-ios-selectors-block bullets2">
		<div class="selectors">
			<!-- Item #1 -->
			<div class="item iosslider__bull-item first">
			</div>
			<!--/ Item #1 -->

			<!-- Item #2 -->
			<div class="item iosslider__bull-item">
			</div>
			<!--/ Item #2 -->

			<!-- Item #3 -->
			<div class="item iosslider__bull-item">
			</div>
			<!--/ Item #3 -->

			<!-- Item #4 -->
			<div class="item iosslider__bull-item">
			</div>
			<!--/ Item #4 -->

			<!-- Item #5 -->
			<div class="item iosslider__bull-item">
			</div>
			<!--/ Item #5 -->
		</div>
		<!--/ .selectors -->
	</div>
	<!--/ Bullets -->

	<div class="scrollbarContainer">
	</div>

	<!-- Bottom mask style 2 -->
	<div class="kl-bottommask kl-bottommask--shadow_ud">
	</div>
	<!--/ Bottom mask style 2 -->
</div>
<!--/ Slideshow - iOS Slider element with animateme scroll efect, custom height and bottom mask style 2 -->

<!-- Action Box - Style 3 section with custom top padding and white background color -->
<section class="hg_section bg-white ptop-0">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="action_box style3" data-arrowpos="center" style="margin-top:-25px;">
					<div class="action_box_inner">
						<div class="action_box_content">
							<div class="ac-content-text">
								<!-- Title -->
								<h4 class="text"><span class="fw-thin">Kami Merupakan <span class="fw-semibold">FINANCIAL E-COMMERCE</span> Resmi</span></h4>
								<!--/ Title -->

								<!-- Sub-Title -->
								<h5 class="ac-subtitle">dari 5 Perusahaan Pembiayaan terpilih dan terpercaya</h5>
								<!--/ Sub-Title -->
							</div>

							<!-- Call to Action buttons -->
							<div class="ac-buttons">
								<a class="btn btn-lined ac-btn" href="#footer" target="_self">TIPS KEUANGAN GRATIS</a>
							</div>
							<!--/ Call to Action buttons -->
						</div>
						<!--/ action_box_content -->
					</div>
					<!--/ action_box_inner -->
				</div>
				<!--/ action_box style3 -->
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Action Box - Style 3 section with custom top padding and white background color -->

<!-- Partners/Logo carousel section -->
<section class="hg_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<!-- Partners carousel default style element -->
				<div class="partners_carousel clearfix">
					<div class="row">
						<div class="col-sm-2">
							<!-- Title -->
							<h4 class="title"><span>OUR PARTNERS VERSION</span></h4>
							<!--/ Title -->
						</div>
						<!--/ col-sm-2 -->

						<div class="col-sm-10">
							<div class="clients-carousel">
								<ul class="partners_carousel fixclear partners_carousel_trigger">
									<!-- Item #1 -->
									<li>
										<a href="https://www.ifsa.or.id/" target="_blank">
											<img src="<?php echo base_url()?>dp/images/misc-logo/logo_appi.png" alt="" title="" />
										</a>
									</li>
									<!--/ Item #1 -->

									<!-- Item #2 -->
									<li>
										<a href="http://www.infobanknews.com/" target="_blank">
											<img src="<?php echo base_url()?>dp/images/misc-logo/logo_infobank.png" alt="" title="" />
										</a>
									</li>
									<!--/ Item #2 -->

									<!-- Item #3 -->
									<li>
										<a href="http://www.investor.co.id/" target="_blank">
											<img src="<?php echo base_url()?>dp/images/misc-logo/logo_investor.png" alt="" title="" />
										</a>
									</li>
									<!--/ Item #3 -->

									<!-- Item #4 -->
									<li>
										<a href="https://www.ojk.go.id/" target="_blank">
											<img src="<?php echo base_url()?>dp/images/misc-logo/logo_ojk.png" alt="" title="" />
										</a>
									</li>
									<!--/ Item #4 -->
								</ul>
							</div>
							<!--/ .clients-carousel -->
						</div>
						<!--/ col-sm-10 -->
					</div>
					<!--/ row -->
				</div>
				<!--/ Partners carousel default style element -->
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Partners/Logo carousel section -->
		

<!-- Title - Style 1 section with custom top padding -->
<section class="hg_section bg-white ptop-65">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<!-- Title element -->
				<div class="kl-title-block clearfix text-center tbk-symbol-- tbk-icon-pos--after-title">
					<!-- Title with montserrat font, custom font size and line height, bold style and light gray color -->
					<h3 class="tbk__title montserrat fs-44 lh-44 fw-bold light-gray3">OK, JADI KENAPA HARUS PILIH DANAPEDIA?</h3>
					<!--/ Title with montserrat font, custom font size and line height, bold style and light gray color -->

					<!-- Sub-Title with custom font size an very thin style -->
					<h4 class="tbk__subtitle fs-18 fw-vthin">Yah, Banyak kelebihan yang terdapat di Danapedia, tapi yang paling terpenting adalah..</h4>
					<!--/ Sub-Title with custom font size an very thin style -->
				</div>
				<!--/ Title element -->
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Title - Style 1 section with custom top padding -->

<!-- Icon Box - Left Floated Style section -->
<section class="hg_section bg-white">
	<div class="full_width">
		<div class="row">
			<div class="col-sm-offset-1 col-md-10 col-sm-10">
				<div class="row gutter-md">
					<div class="col-md-3 col-sm-3">
						<!-- Icon box float left -->
						<div class="kl-iconbox kl-iconbox--fleft text-left">
							<div class="kl-iconbox__inner">
								<!-- Icon -->
								<div class="kl-iconbox__icon-wrapper">
									<img class="kl-iconbox__icon" src="<?php echo base_url()?>dp/images/set-03-01.svg" alt="Stunning Page Builder">
								</div>
								<!--/ Icon -->

								<!-- /.kl-iconbox__icon-wrapper -->
								<div class="kl-iconbox__content-wrapper">
									<!-- Title -->
									<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
										<h3 class="kl-iconbox__title fs-22 lh-30 fw-normal gray2">FOKUS</h3>
									</div>
									<!--/ Title -->

									<!-- Description -->
									<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
										<p class="kl-iconbox__desc fs-14 gray">
											Hanya <strong>DANA TUNAI</strong>. Lebih Lengkap. Lebih Expert. Lebih Terpercaya.
										</p>
									</div>
									<!--/ Description -->
								</div>
								<!-- /.kl-iconbox__content-wrapper -->
							</div>
							<!--/ kl-iconbox__inner -->
						</div>
						<!--/ Icon box float left -->
					</div>
					<!--/ col-md-3 col-sm-3 -->

					<div class="col-md-3 col-sm-3">
						<!-- Icon box float left -->
						<div class="kl-iconbox kl-iconbox--fleft text-left">
							<div class="kl-iconbox__inner">
								<!-- Icon -->
								<div class="kl-iconbox__icon-wrapper">
									<img class="kl-iconbox__icon" src="<?php echo base_url()?>dp/images/set-03-02.svg" alt="Iconic Awarded Design">
								</div>
								<!--/ Icon -->

								<!-- /.kl-iconbox__icon-wrapper -->
								<div class="kl-iconbox__content-wrapper">
									<!-- Title -->
									<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
										<h3 class="kl-iconbox__title fs-22 lh-30 fw-normal gray2">EKSKLUSIF</h3>
									</div>
									<!--/ Title -->

									<!-- Description -->
									<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
										<p class="kl-iconbox__desc fs-14 gray">
											Kami seleksi 5 Perusahaan Pembiayaan dan Bank terbaik dengan cabang di seluruh provinsi.
										</p>
									</div>
									<!--/ Description -->
								</div>
								<!-- /.kl-iconbox__content-wrapper -->
							</div>
							<!--/ kl-iconbox__inner -->
						</div>
						<!-- Icon box float left -->
					</div>
					<!--/ col-md-3 col-sm-3 -->

					<div class="col-md-3 col-sm-3">
						<!-- Icon box float left -->
						<div class="kl-iconbox kl-iconbox--fleft text-left">
							<div class="kl-iconbox__inner">
								<!-- Icon -->
								<div class="kl-iconbox__icon-wrapper">
									<img class="kl-iconbox__icon" src="<?php echo base_url()?>dp/images/set-03-03.svg" alt="Featurewise Complete">
								</div>
								<!--/ Icon -->

								<!-- /.kl-iconbox__icon-wrapper -->
								<div class="kl-iconbox__content-wrapper">
									<!-- Title -->
									<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
										<h3 class="kl-iconbox__title fs-22 lh-30 fw-normal gray2">Cepat &amp; Mudah</h3>
									</div>
									<!--/ Title -->

									<!-- Description -->
									<div class="kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
										<p class="kl-iconbox__desc fs-14 gray">
											Pengajuan Anda cair mulai dari <strong>1</strong> hari kerja.
										</p>
									</div>
									<!--/ Description -->
								</div>
								<!-- /.kl-iconbox__content-wrapper -->
							</div>
							<!--/ kl-iconbox__inner -->
						</div>
						<!--/ Icon box float left -->
					</div>
					<!--/ col-md-3 col-sm-3 -->

					<div class="col-md-3 col-sm-3">
						<!-- Icon box float left -->
						<div class="kl-iconbox kl-iconbox--fleft text-left">
							<div class="kl-iconbox__inner">
								<!-- Icon -->
								<div class="kl-iconbox__icon-wrapper ">
									<img class="kl-iconbox__icon" src="<?php echo base_url()?>dp/images/set-03-04.svg" alt="Mature project">
								</div>
								<!--/ Icon -->

								<!-- /.kl-iconbox__icon-wrapper -->
								<div class="kl-iconbox__content-wrapper">
									<!-- Title -->
									<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
										<h3 class="kl-iconbox__title fs-22 lh-30 fw-normal gray2">Promo Menarik</h3>
									</div>
									<!--/ Title -->

									<!-- Description -->
									<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
										<p class="kl-iconbox__desc fs-14 gray">
											Dapatkan promo menarik yang hanya Anda dapatkan di sini.
										</p>
									</div>
									<!--/ Description -->
								</div>
								<!-- /.kl-iconbox__content-wrapper -->
							</div>
							<!--/ kl-iconbox__inner -->
						</div>
					</div>
					<!--/ col-md-3 col-sm-3 -->
				</div>
				<!--/ row gutter-md -->
			</div>
			<!--/ col-sm-offset-1 col-md-10 col-sm-10 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ full_width -->
</section>
<!--/ Icon Box - Left Floated Style section -->

<section class="hg_section pbottom-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- Services box element modern style -->
                <div class="services_box services_box--modern sb--hasicon">
                    <!-- Service box wrapper -->
                    <div class="services_box__inner clearfix">
                        <!-- Icon content -->
                        <div class="services_box__icon">
                            <!-- Icon wrapper -->
                            <div class="services_box__icon-inner">
                                <!-- Icon = .icon-process3  -->
                                <span class="services_box__fonticon icon-process3"></span>
                            </div>
                            <!--/ Icon wrapper -->
                        </div>
                        <!--/ Icon content -->

                        <!-- Content -->
                        <div class="services_box__content">
                            <!-- Title -->
                            <h4 class="services_box__title" style="padding-bottom: 40px; font-weight: bolder; font-size: 20px;">Overview Produk</h4>
                            <!--/ Title -->

                            <div class="col-md-6 col-sm-6">
                            	<!-- List wrapper -->
                            	<div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <!-- List -->
                                <ul class="services_box__list">
                                    <li><span class="services_box__list-text" style="font-weight: bold; font-size: 18px;">Dana Cair mulai 1 juta s/d 10 Milyar</span></li>
                                    <li><span class="services_box__list-text" style="font-weight: bold; font-size: 18px;">Bunga mulai 0.8% per bulan</span></li>
                                    <li><span class="services_box__list-text" style="font-weight: bold; font-size: 18px;">Proses mulai dari 1 hari</span></li>
                                    <li><span class="services_box__list-text" style="font-weight: bold; font-size: 18px;">TANPA BI CHECKING</span></li>
                                    <li><span class="services_box__list-text" style="font-weight: bold; font-size: 18px;">Tanpa Potongan</span></li>
                                    <li><span class="services_box__list-text" style="font-weight: bold; font-size: 18px;">Bonus Asuransi Kendaraan &amp; Asuransi Jiwa</span></li>
                                </ul>
                                <!--/ List -->
                            </div>
                            <!--/ List wrapper -->
                            </div>

                            <div class="col-md-6 col-sm-6">
                            	<!-- List wrapper -->
                            	<div class="services_box__list-wrapper">
	                                <span class="services_box__list-bg"></span>
	                                <!-- List -->
	                                <ul class="services_box__list">
	                                	<li><span class="services_box__list-text" style="font-weight: bold; font-size: 18px;">Bisa take over kredit dari Bank/Leasing lain</span></li>
	                                    <li><span class="services_box__list-text" style="font-weight: bold; font-size: 18px;">Tenor 6 bulan s/d 4 tahun</span></li>
	                                    <li><span class="services_box__list-text" style="font-weight: bold; font-size: 18px;">BPKB terjamin aman</span></li>
	                                    <li><span class="services_box__list-text" style="font-weight: bold; font-size: 18px;">Syarat &amp; Data Fleksibel</span></li>
	                                    <li><span class="services_box__list-text" style="font-weight: bold; font-size: 18px;">Terima Plat Daerah</span></li>
	                                </ul>
	                                <!--/ List -->
	                            </div>
	                            <!--/ List wrapper -->
                            </div>
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Service box wrapper -->
                </div>
                <!--/ Services box element modern style -->
            </div>
            <!--/ col-md-6 col-sm-6 -->
        </div>
    </div>
    <!--/ container -->
</section>



<!-- PINJAMAN DENGAN JAMINAN SECTION -->
<section class="hg_section hg_section--relative ptop-80 pbottom-80">
	<!-- Background -->
	<div class="kl-bg-source">
		<!-- Gradient overlay -->
		<div class="kl-bg-source__overlay" style="background:rgba(205,33,34,1); background: -moz-linear-gradient(left, rgba(205,33,34,1) 0%, rgba(245,72,76,1) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(205,33,34,1)), color-stop(100%,rgba(245,72,76,1))); background: -webkit-linear-gradient(left, rgba(205,33,34,1) 0%,rgba(245,72,76,1) 100%); background: -o-linear-gradient(left, rgba(205,33,34,1) 0%,rgba(245,72,76,1) 100%); background: -ms-linear-gradient(left, rgba(205,33,34,1) 0%,rgba(245,72,76,1) 100%); background: linear-gradient(135deg,rgb(54,3,132),rgb(218,11,104)); ">
		</div>
		<!--/ Gradient overlay -->
	</div>
	<!--/ Background -->

	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<!-- Title element with bottom line style -->
				<div class="kl-title-block clearfix text-center tbk-symbol--line tbk-icon-pos--after-title">
					<!-- Title with montserrat font, white color and bold style -->
					<h3 class="tbk__title white montserrat fw-bold fs-32">PINJAMAN DENGAN JAMINAN BPKB Mobil, Motor, SHM</h3>
					<!-- Title bottom symbol -->
					<div class="tbk__symbol ">
						<span></span>
					</div>
					<!--/ Title bottom symbol -->
					<h4 class="tbk__subtitle white montserrat fs-16">Mari temukan penawaran menarik di bawah ini dan ajukan pembiayaan dengan rasa nyaman bahwa Anda sudah memilih yang Terbaik.</h4>
					<!--/ Title with montserrat font, white color and bold style -->
				</div>
				<!--/ Title element with bottom line style -->
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ PINJAMAN DENGAN JAMINAN SECTION -->

<!-- Partner Pinjaman dengan Jaminan -->
<section class="hg_section--relative">
	<!-- Background -->
	<div class="kl-bg-source">
		<!-- Background image -->
		<div class="kl-bg-source__bgimage" >
		</div>
		<!--/ Background image -->

		<!-- Background overlay -->
		<div class="kl-bg-source__overlay" >
		</div>
		<!--/ Background overlay -->
	</div>
	<!--/ Background -->


	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<!-- Partners carousel default style element -->
				<div class="partners_carousel clearfix">
					<div class="row">
						<div class="col-sm-12">
							<div class="clients-carousel">
								<ul class="partners_carousel fixclear partners_carousel_trigger">
									<!-- Item #1 -->
									<li>
										<a href="<?php echo site_url('lihat-penawaran-bfi-finance') ;?>" target="_blank">
											<img src="<?php echo base_url()?>dp/images/logo-leasing/logo_bfi.png" alt="" title="" />
										</a>
									</li>
									<!--/ Item #1 -->

									<!-- Item #2 -->
									<li>
										<a href="<?php echo site_url('lihat-penawaran-mnc-finance') ;?>" target="_blank">
											<img src="<?php echo base_url()?>dp/images/logo-leasing/logo_mnc.png" alt="" title="" />
										</a>
									</li>
									<!--/ Item #2 -->

									<!-- Item #3 -->
									<li>
										<a href="<?php echo site_url('lihat-penawaran-olimpindo') ;?>" target="_blank">
											<img src="<?php echo base_url()?>dp/images/logo-leasing/logo_olympindo.png" alt="" title="" />
										</a>
									</li>
									<!--/ Item #3 -->

									<!-- Item #4 -->
									<li>
										<a href="<?php echo site_url('lihat-penawaran-radana') ;?>" target="_blank">
											<img src="<?php echo base_url()?>dp/images/logo-leasing/logo_radana.png" alt="" title="" />
										</a>
									</li>
									<!--/ Item #4 -->
									
									<!-- Item #5 -->
									<li>
										<a href="<?php echo site_url('lihat-penawaran-sms-finance') ;?>" target="_blank">
											<img src="<?php echo base_url()?>dp/images/logo-leasing/logo_sms.png" alt="" title="" />
										</a>
									</li>
									<!--/ Item #5 -->
								</ul>
							</div>
							<!--/ .clients-carousel -->
						</div>
						<!--/ col-sm-10 -->
					</div>
					<!--/ row -->
				</div>
				<!--/ Partners carousel default style element -->
				
				
				<!-- Info box v3 element-->
				<div class="infobox3" style="background-image:url(); padding-top:0px; padding-bottom: 0px; background-repeat:no-repeat; background-size:cover; background-position:center center; background-attachment:scroll;">
					<div class="row">
						<!-- Info box v3 content - light text -->
						<div class="ib-content infobox3--light col-sm-12 col-lg-12">
							<!-- Info box Button #1 lined style -->
							<div class="ib-button ib-button-1">
								<a href="<?php echo site_url('ajukan-sekarang-jaminan') ;?>" target="_blank" class="ib-button-1 btn btn-fullcolor">AJUKAN SEKARANG</a>
							</div>
							<!--/ Info box Button #1 lined style -->
							
							<!-- Info box Button #2 full color style -->
							<div class="ib-button ib-button-2">
								<a href="<?php echo site_url('bandingkan-jaminan') ;?>" target="_blank" class="ib-button2 btn btn-fullcolor">LIHAT PERBANDINGAN</a>
							</div>
							<!--/ Info box Button #2 full color style -->
						</div>
						<!--/ Info box v3 content - light text -->
					</div>
					<!--/ row -->
				</div>
				<!--/ Info box v3 element-->
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!-- / Partner Pinjaman dengan Jaminan -->

<?php $this->load->view('pages/pengajuancepat');?>

<!-- Steps box section -->
<section class="hg_section ">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<!-- Process steps element #1 style 2 -->
				<div class="process_steps process_steps--style2 kl-bgc-light">
					<!-- Introduction step -->
					<div class="process_steps__step process_steps__intro process_steps__height">
						<!-- Intro wrapper -->
						<div class="process_steps__intro-wrp">
							<!-- Title -->
							<h3 class="process_steps__intro-title">our working process<strong>in 3 steps</strong></h3>
							<!--/ Title -->

							<!-- Description -->
							<p class="process_steps__intro-desc">
								Pengajuan Pinjaman Anda akan langsung di proses oleh Tim kami pada hari yang sama.
							</p>
							<!-- Description -->

							
						</div>
						<!--/ Intro wrapper -->
					</div>
					<!--/ Introduction step -->

					<!-- Steps container -->
					<div class="process_steps__container">
						<!-- Steps wrapper -->
						<div class="process_steps__inner process_steps__height">
							<!-- Step #1 -->
							<div class="process_steps__step">
								<!-- Image/Icon -->
								<div class="process_steps__step-icon process_steps__step-typeimg">
									<img src="<?php echo base_url()?>dp/images/process2.svg" class="process_steps__step-icon-src" alt="" title="" />
								</div>
								<!--/ Image/Icon -->

								<!-- Title -->
								<h3 class="process_steps__step-title">Isi Form Online</h3>
								<!--/ Title -->

								<!-- Description -->
								<p class="process_steps__step-desc">
									Isi data pengajuan Anda dengan lengkap hanya 1 menit.
								</p>
								<!--/ Description -->
							</div>
							<!--/ Step #1 -->

							<!-- Step #2 -->
							<div class="process_steps__step">
								<!-- Image/Icon -->
								<div class="process_steps__step-icon process_steps__step-typeimg">
									<img src="<?php echo base_url()?>dp/images/process1.svg" class="process_steps__step-icon-src" alt="" title="" />
								</div>
								<!--/ Image/Icon -->

								<!-- Title -->
								<h3 class="process_steps__step-title">Bandingkan</h3>
								<!--/ Title -->

								<!-- Description -->
								<p class="process_steps__step-desc">
									Kami akan kirimkan rangkuman penawaran dari 5 perusahaan pembiayaan diatas.
								</p>
								<!--/ Description -->
							</div>
							<!--/ Step #2 -->

							<!-- Step #3 -->
							<div class="process_steps__step">
								<!-- Image/Icon -->
								<div class="process_steps__step-icon process_steps__step-typeimg">
									<img src="<?php echo base_url()?>dp/images/process3.svg" alt="" class="process_steps__step-icon-src">
								</div>
								<!--/ Image/Icon -->

								<!-- Title -->
								<h3 class="process_steps__step-title">Pilih</h3>
								<!--/ Title -->

								<!-- Description -->
								<p class="process_steps__step-desc">
									Tentukan Perusahaan Pembiayaan pilihan Anda.
								</p>
								<!--/ Description -->
							</div>
							<!--/ Step #3 -->
						</div>
						<!--/ Steps wrapper -->
					</div>
					<!--/ Steps container -->

					<div class="clearfix">
					</div>
				</div>
				<!--/ Process steps element #1 style 2 -->
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Steps box section -->

<!-- Statistics section -->
<section class="hg_section hg_section--relative ptop-80 pbottom-80">
	<!-- Background -->
	<div class="kl-bg-source">
		<!-- Background image -->
		<div class="kl-bg-source__bgimage" style="background-image:url(<?php echo base_url()?>dp/images/man-791049_1280.jpg); background-repeat:no-repeat; background-attachment:scroll; background-position-x:center; background-position-y:center; background-size:cover;">
		</div>
		<!--/ Background image -->

		<!-- Color overlay -->
		<div class="kl-bg-source__overlay" style="background-color:rgba(0,0,0,0.85)">
		</div>
		<!--/ Color overlay -->
	</div>
	<!--/ Background -->

	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<!-- Title element -->
				<div class="kl-title-block clearfix tbk--text-light text-center tbk-symbol--line tbk--colored tbk-icon-pos--after-title">
					<!-- Title -->
					<h3 class="tbk__title ">DANAPEDIA STATISTICS</h3>
					<!--/ Title -->

					<!-- Title bottom symbol -->
					<div class="tbk__symbol ">
						<span></span>
					</div>
					<!--/ Title bottom symbol -->
				</div>
				<!--/ Title element -->

				<!-- Statistics element - normal placement & light box style -->
				<div class="statistic-box__container statistic-box--stl-style2 statistic-box--light">
					<!-- Statistic box #1 -->
					<div class="statistic-box">
						<!-- Icon wrapper -->
						<div class="statistic-box__icon-holder">
							<!-- Icon = .icon-noun_65754 -->
							<span class="statistic-box__icon icon-noun_65754"></span>
						</div>
						<!-- Icon wrapper -->

						<!-- Line -->
						<div class="statistic-box__line">
						</div>
						<!--/ Line -->

						<!-- Details -->
						<div class="statistic-box__details">
							<!-- Title -->
							<h4 class="statistic-box__title">250+</h4>
							<!--/ Title -->

							<!-- Content -->
							<div class="statistic-box__content">
								KANTOR CABANG SELURUH INDONESIA
							</div>
							<!--/ Content -->
						</div>
						<!--/ Details -->
					</div>
					<!--/ Statistic box #1 -->

					<!-- Statistic box #2 -->
					<div class="statistic-box">
						<!-- Icon wrapper -->
						<div class="statistic-box__icon-holder">
							<!-- Icon = .icon-noun_61152 -->
							<span class="statistic-box__icon icon-noun_61152"></span>
						</div>
						<!-- Icon wrapper -->

						<!-- Line -->
						<div class="statistic-box__line">
						</div>
						<!--/ Line -->

						<!-- Details -->
						<div class="statistic-box__details">
							<!-- Title -->
							<h4 class="statistic-box__title">500 Ribu+</h4>
							<!--/ Title -->

							<!-- Content -->
							<div class="statistic-box__content">
								NASABAH AKTIF
							</div>
							<!--/ Content -->
						</div>
						<!--/ Details -->
					</div>
					<!--/ Statistic box #2 -->

					<!-- Statistic box #3 -->
					<div class="statistic-box">
						<!-- Icon wrapper -->
						<div class="statistic-box__icon-holder">
							<!-- Icon = .icon-gi-ico-10 -->
							<span class="statistic-box__icon icon-gi-ico-10"></span>
						</div>
						<!-- Icon wrapper -->

						<!-- Line -->
						<div class="statistic-box__line">
						</div>
						<!--/ Line -->

						<!-- Details -->
						<div class="statistic-box__details">
							<!-- Title -->
							<h4 class="statistic-box__title">15 Triliyun+</h4>
							<!--/ Title -->

							<!-- Content -->
							<div class="statistic-box__content">
								PEMBIAYAAN TERSALURKAN
							</div>
							<!--/ Content -->
						</div>
						<!--/ Details -->
					</div>
					<!--/ Statistic box #3 -->

					<!-- Statistic box #4 -->
					<div class="statistic-box">
						<!-- Icon wrapper -->
						<div class="statistic-box__icon-holder">
							<!-- Icon = .icon-noun_167805 -->
							<span class="statistic-box__icon icon-noun_167805"></span>
						</div>
						<!-- Icon wrapper -->

						<!-- Line -->
						<div class="statistic-box__line">
						</div>
						<!--/ Line -->

						<!-- Details -->
						<div class="statistic-box__details">
							<!-- Title -->
							<h4 class="statistic-box__title">180++</h4>
							<!--/ Title -->

							<!-- Content -->
							<div class="statistic-box__content">
								REVIEWS AND RATINGS
							</div>
							<!--/ Content -->
						</div>
						<!--/ Details -->
					</div>
					<!--/ Statistic box #4 -->
				</div>
				<!--/ Statistics element - normal placement & light box style -->
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Statistics section -->

<!-- PINJAMAN TANPA JAMINAN SECTION -->
<section class="hg_section hg_section--relative ptop-80 pbottom-80">
	<!-- Background -->
	<div class="kl-bg-source">
		<!-- Gradient overlay -->
		<div class="kl-bg-source__overlay" style="background:rgba(205,33,34,1); background: -moz-linear-gradient(left, rgba(205,33,34,1) 0%, rgba(245,72,76,1) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(205,33,34,1)), color-stop(100%,rgba(245,72,76,1))); background: -webkit-linear-gradient(left, rgba(205,33,34,1) 0%,rgba(245,72,76,1) 100%); background: -o-linear-gradient(left, rgba(205,33,34,1) 0%,rgba(245,72,76,1) 100%); background: -ms-linear-gradient(left, rgba(205,33,34,1) 0%,rgba(245,72,76,1) 100%); background: linear-gradient(135deg,rgb(54,3,132),rgb(218,11,104)); ">
		</div>
		<!--/ Gradient overlay -->
	</div>
	<!--/ Background -->

	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<!-- Title element with bottom line style -->
				<div class="kl-title-block clearfix text-center tbk-symbol--line tbk-icon-pos--after-title">
					<!-- Title with montserrat font, white color and bold style -->
					<h2 class="tbk__title white montserrat fw-bold fs-32">PINJAMAN TANPA JAMINAN</h2>
					<!-- Title bottom symbol -->
					<div class="tbk__symbol ">
						<span></span>
					</div>
					<!--/ Title bottom symbol -->
					<h4 class="tbk__subtitle white montserrat fs-16">Mari temukan penawaran menarik di bawah ini dan ajukan pembiayaan dengan rasa nyaman bahwa Anda sudah memilih yang Terbaik.</h4>
					<!--/ Title with montserrat font, white color and bold style -->
				</div>
				<!--/ Title element with bottom line style -->
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ PINJAMAN TANPA JAMINAN SECTION -->

<!-- Partner Pinjaman TANPA Jaminan -->
<section class="hg_section--relative">
	<!-- Background -->
	<div class="kl-bg-source">
		<!-- Background image -->
		<div class="kl-bg-source__bgimage" >
		</div>
		<!--/ Background image -->

		<!-- Background overlay -->
		<div class="kl-bg-source__overlay" >
		</div>
		<!--/ Background overlay -->
	</div>
	<!--/ Background -->


	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<!-- Partners carousel default style element -->
				<div class="partners_carousel clearfix">
					<div class="row">
						<div class="col-sm-12">
							<div class="clients-carousel">
								<ul class="partners_carousel fixclear partners_carousel_trigger">
									<!-- Item #1 -->
									<li>
										<a href="<?php echo site_url('lihat-penawaran-cimb');?>" target="_blank">
											<img src="<?php echo base_url()?>dp/images/logo-leasing/logo_cimb.png" alt="" title="" />
										</a>
									</li>
									<!--/ Item #1 -->

									<!-- Item #2 -->
									<li>
										<a href="<?php echo site_url('lihat-penawaran-dbs');?>" target="_blank">
											<img src="<?php echo base_url()?>dp/images/logo-leasing/logo_dbs.png" alt="" title="" />
										</a>
									</li>
									<!--/ Item #2 -->

									<!-- Item #3 -->
									<li>
										<a href="<?php echo site_url('lihat-penawaran-permata-kta');?>" target="_blank">
											<img src="<?php echo base_url()?>dp/images/logo-leasing/logo_permata.png" alt="" title="" />
										</a>
									</li>
									<!--/ Item #3 -->

									<!-- Item #4 -->
									<li>
										<a href="<?php echo site_url('lihat-penawaran-qnb');?>" target="_blank">
											<img src="<?php echo base_url()?>dp/images/logo-leasing/logo_qnb.png" alt="" title="" />
										</a>
									</li>
									<!--/ Item #4 -->
									
									<!-- Item #5 -->
									<li>
										<a href="<?php echo site_url('lihat-penawaran-sc');?>" target="_blank">
											<img src="<?php echo base_url()?>dp/images/logo-leasing/logo_stancart.png" alt="" title="" />
										</a>
									</li>
									<!--/ Item #5 -->
								</ul>
							</div>
							<!--/ .clients-carousel -->
						</div>
						<!--/ col-sm-10 -->
					</div>
					<!--/ row -->
				</div>
				<!--/ Partners carousel default style element -->
				
				<!-- Info box v3 element-->
				<div class="infobox3" style="background-image:url(); padding-top:0px; padding-bottom: 0px; background-repeat:no-repeat; background-size:cover; background-position:center center; background-attachment:scroll;">
					<div class="row">
						<!-- Info box v3 content - light text -->
						<div class="ib-content infobox3--light col-sm-12 col-lg-12">
							<!-- Info box Button #1 lined style -->
							<div class="ib-button ib-button-1">
								<a href="<?php echo site_url('ajukan-sekarang-tanpa-jaminan');?>" target="_blank" class="ib-button-1 btn btn-fullcolor">AJUKAN SEKARANG</a>
							</div>
							<!--/ Info box Button #1 lined style -->
							
							<!-- Info box Button #2 full color style -->
							<div class="ib-button ib-button-2">
								<a href="<?php echo site_url('bandingkan-tanpa-jaminan') ;?>" target="_blank" class="ib-button2 btn btn-fullcolor">LIHAT PERBANDINGAN</a>
							</div>
							<!--/ Info box Button #2 full color style -->
						</div>
						<!--/ Info box v3 content - light text -->
					</div>
					<!--/ row -->
				</div>
				<!--/ Info box v3 element-->
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!-- / Partner Pinjaman TANPA Jaminan -->

<?php $this->load->view('pages/pengajuancepat');?>

<!--/Partner and Testimonials -->
<section class="hg_section hg_section--relative ptop-65 pbottom-20" style="background-color: #222222;">
	<!-- Background -->
	<div class="kl-bg-source">
		<!-- Gloss overlay -->
		<div class="kl-bg-source__overlay-gloss"></div>
		<!--/ Gloss overlay -->
	</div>
	<!--/ Background -->

	<!-- Content -->
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<!-- Title element with bottom line style -->
				<div class="kl-title-block clearfix text-center tbk-symbol--line tbk-icon-pos--after-title">
					<!-- Title with montserrat font, white color and bold style -->
					<h3 class="tbk__title white montserrat fw-bold">TESTIMONI</h3>
					<!--/ Title with montserrat font, white color and bold style -->

					<!-- Title bottom symbol -->
					<div class="tbk__symbol ">
						<span></span>
					</div>
					<!--/ Title bottom symbol -->
				</div>
				<!--/ Title element with bottom line style -->

				<!-- Testimonials & Partners element - light text style -->
				<div class="testimonials-partners testimonials-partners--light">
					<!-- Testimonials  element-->
					<div class="ts-pt-testimonials clearfix">
                                            <?php
                                            $testimonies = getTestimonies();
                                            foreach ($testimonies as $testimony){
                                            ?>
						<!-- Item - size 2 and normal style with custom margin top -->
						<div class="ts-pt-testimonials__item ts-pt-testimonials__item--size-<?php echo rand(1,2);?> ts-pt-testimonials__item--normal" style="margin-top:20px; ">
							<!-- Testimonial text -->
							<div class="ts-pt-testimonials__text">
                                                            <?php echo $testimony['testimony_desc'];?>
                                                        </div>
							<!--/ Testimonial text -->

							<!-- Testimonial info -->
							<div class="ts-pt-testimonials__infos ts-pt-testimonials__infos--">
								<!-- User image -->
								<div class="ts-pt-testimonials__img" style="background-image:url('<?php echo base_url()?>/uploads/testimony/<?php echo $testimony['testimony_photo'];?>');" title="JIMMY FERRARA">
								</div>
								<!--/ User image -->


								<!-- Name -->
								<h4 class="ts-pt-testimonials__name"><?php echo strtoupper($testimony['testimony_name']);?></h4>
								<!--/ Name -->

								<!-- Position -->
								<div class="ts-pt-testimonials__position">
									<?php echo strtoupper($testimony['testimony_jobtitle']);?> 
                                                                        <?php echo strtoupper($testimony['testimony_company']);?>
								</div>
								<!--/ Position -->

								<!-- Review stars - 5 stars -->
								<div class="ts-pt-testimonials__stars ts-pt-testimonials__stars--5">
									<span class="glyphicon glyphicon-star"></span>
									<span class="glyphicon glyphicon-star"></span>
									<span class="glyphicon glyphicon-star"></span>
									<span class="glyphicon glyphicon-star"></span>
								</div>
								<!--/ Review stars - 5 stars -->
							</div>
							<!--/ Testimonial info - .ts-pt-testimonials__infos -->
						</div>
						<!--/ Item - size 2 and normal style with custom margin top - .ts-pt-testimonials__item -->
                                            
                                            <?php   
                                            }
                                            ?>
                                                
					</div>
					<!--/ Testimonials element - .ts-pt-testimonials-->

					
				</div>
				<!--/ Testimonials & Partners element - light text style -->
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ Content .container -->
</section>

<!-- Latest Posts - Accordion Style section -->
<section class="hg_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<!-- Latest posts accordion style -->
				<div class="latest_posts acc-style">
					<!-- Title -->
					<h3 class="m_title">OUR LATEST STORIES</h3>
					<!--/ Title -->

					<!-- View all posts button -->
                                        <a href="<?php echo site_url('blog');?>" class="viewall">VIEW ALL -</a>
					<!--/ View all posts button -->

					<!-- CSS3 Accordion -->
					<div class="css3accordion">
						<ul>
                                                    <?php
                                                    $blog = getBlog();
                                                    $idx = 0;
                                                    foreach ($blog as $post) {
                                                        $blogimage = $post['image'];
                                                        if($post['image'] == ""){
                                                            $blogimage = 'default.jpg';
                                                        }
                                                    ?>
                                                        <!-- Post -->
							<li <?php  if($idx == 2) echo 'class="last"';?>>
								<!-- Post wrapper -->
								<div class="inner-acc" style="width: 570px;">
									<!-- Post link wrapper -->
									<a href="<?php echo site_url("blog/read/".$post['slug'])?>" target="_blank" class="thumb hoverBorder plus">
										<!-- Border wrapper -->
										<span class="hoverBorderWrapper">
													<!-- Image -->
                                                                                                        <img src="uploads/blogs/<?php echo $blogimage;?>" alt="" title="" />
											<!--/ Image -->

											<!-- Hover border/shadow -->
													<span class="theHoverBorder"></span>
											<!--/ Hover border/shadow -->
												</span>
										<!--/ Border wrapper -->
									</a>
									<!-- Post link wrapper -->

									<!-- Post content -->
									<div class="content">
										<!-- Details & tags -->
										<em><?php echo dateFormat($post['created'],1);?></em>
										<!--/ Details & tags -->

										<!-- Title with link -->
										<h5 class="m_title"><a href="blog-post.html"><?php echo $post['title'];?></a></h5>
										<!--/ Title with link -->

										<!-- Content text -->
										<div class="text">
                                                                                    <?php 
                                                                                    $content = strip_tags($post['content']);
                                                                                    
                                                                                    echo substr($content,0,300);
                                                                                    ?>...
                                                                                </div>
										<!--/ Content text -->

										<!-- Read more button -->
                                                                                <a href="<?php echo site_url("blog/read/".$post['slug'])?>">READ MORE +</a>
										<!--/ Read more button -->
									</div>
									<!--/ Post content -->
								</div>
								<!--/ Post wrapper -->
							</li>
							<!--/ Post -->

                                                    <?php
                                                    $idx++;
                                                    }
                                                    ?>
							
						</ul>
					</div>
					<!--/ CSS3 Accordion -->
				</div>
				<!--/ Latest posts accordion style -->
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ hg_section_size container -->
</section>
<!--/ Latest Posts - Accordion Style section -->


<!-- Payment Point section -->
<section class="hg_section bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<!--/ Image Box style 4 - left symbol style -->
				<div class="box image-boxes imgboxes_style4 kl-title_style_left">
					<!-- Image Box anchor link -->
					<a class="imgboxes4_link imgboxes-wrapper" href="#" target="_blank">
						<!-- Image -->
						<img src="<?php echo base_url()?>dp/images/banner_payment.jpg" alt="RESCUE SUPPORT" title="RESCUE SUPPORT" class="img-responsive" />
						<!--/ Image -->
					</a>
					<!--/ Image Box anchor link -->
				</div>
				<!--/ Image Box style 4 - left symbol style -->
			</div>
			<!--/ col-md-4 col-sm-4  -->

		</div>
		<!--/ row -->
	</div>
	<!--/ hg_section_size container -->
</section>
<!--/ Payment Point section -->


