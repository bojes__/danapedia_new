<style>

@media screen and (max-width: 768px) {
	.kl-header-bg{
	 background:none !important;
	}
}
</style>
<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="header_clearfix_1"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
            	
                <!-- Info Box v2 style 2 element - light theme -->
                <div class="ib2-style2 ib2-text-color-light-theme ib2-custom" style="background-color:#fff;">
                    <div class="ib2-inner">
                    
                    
                        <!-- Content -->
                        <div class="ib2-content" >

                            <div  class="row"  >
                                <div class="col-sm-3" style=" position:relative; height:200px;">
                                    <img class="img_v_align" src="<?php echo base_url()?>dp/images/logo-leasing/logo_cimb.png" alt="" title="" />
                                </div>
                                <div class="col-sm-9">
                                	<div class="row">
                                	<div class="col-sm-12">
                                    	<h2 class="ib2-content--title">PT Bank CIMB Niaga Tbk</h2>
                                        <br>
                                    </div>
                                    </div>
                                    
                                    <div class="row">
                                    <div class="col-sm-3 fs-16">
                                    	<strong>Kantor Pusat</strong>
                                    </div>
                                    <div class="col-sm-9 fs-16">
                                    	<span>Graha Niaga/ Niaga Tower. Jalan Jendral Sudirman Kav 58 Jakarta Selatan 12190</span>
                                    </div>
                                    </div>
                                    
                                    <div class="row">
                                    <div class="col-sm-3 fs-16">
                                    	<strong>Berdiri Sejak</strong>
                                    </div>
                                    <div class="col-sm-9 fs-16">
                                    	<span>&nbsp;1863&nbsp;</span>
                                    </div>
                                    </div>
                                    
                                    <div class="row">
                                    <div class="col-sm-3 fs-16">
                                    	<strong>Jumlah Produk</strong>
                                    </div>
                                    <div class="col-sm-9 fs-16">
                                    	<span>&nbsp;-&nbsp;</span>
                                    </div>
                                    </div>
                                    
                                    <div class="row">
                                    <div class="col-sm-3 fs-16">
                                    	<strong>Jumlah Cabang (di Indonesia)</strong>
                                    </div>
                                    <div class="col-sm-9 fs-16">
                                    	<span>27 cabang</span>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Info box v2 style 2 wrapper - .ib2-inner -->
                </div>
                <!--/ Info Box v2 style 2 element - light theme -->
            </div>
        </div>
    </div>
</section>

<section class="hg_section bg-white ptop-65">
	<div class="container">
    	<div class="row">
        	<div class="col-md-6">
            	<h4 class=" tcolor f_bold">STRUKTUR PEMBIAYAAN</h4>
              <table class="table_content" >
                <tbody>
                	<tr>
                    	<td>Plafon Pinjaman</td>
                        <td>Rp. 4.000.000,- sd Rp. 200.000.000,-</td>
                    </tr>
                    <tr>
                    	<td>Tenor</td>
                        <td>Hingga 48 bulan</td>
                    </tr>
                    <tr>
                    	<td>Suku Bunga</td>
                        <td>1.49% (flat per bulan)</td>
                    </tr>
                    <tr>
                    	<td>Lama Proses</td>
                        <td>1 hari</td>
                    </tr>
                </tbody>
              </table>
              <br><br><br>
            </div>
        </div>
        
        <div class="row">
        	<div class="col-md-10">
            	<h4 class=" tcolor f_bold">KRITERIA DEBITUR</h4>
              <table class="table_content" >
              	<thead>
                	<tr class="f_bold black">
                    	<td class="black bg_color_1">Kriteria Umum</td>
                        <td class="black bg_color_3">Karyawan</td>
                        <td class="black bg_color_3">Wiraswasta</td>
                        <td class="black bg_color_3">Profesional</td>
                    </tr>
                </thead>
                <tbody>
                	<tr >
                    	<td class="f_bold black" >WNI usia minimal 21 tahun dan maksimal 55 tahun</td>
                        <td class="tx_center">V</td>
                        <td class="tx_center">V</td>
                        <td class="tx_center">V</td>
                    </tr>
                    <tr >
                    	<td class="f_bold black" >Penghasilan kotor minimal perbulan</td>
                        <td colspan="3" class="tx_center">tidak ada syarat minimal</td>
                    </tr>
                    <tr >
                    	<td class="f_bold black" >Minimal usaha atau bekerja</td>
                        <td colspan="3" class="tx_center"></td>
                    </tr>
                    <tr >
                    	<td class="f_bold black" >Masa aktif kartu kredit di bank lain</td>
                        <td colspan="3" class="tx_center"></td>
                    </tr>
                    <tr >
                    	<td class="f_bold black" >Domisili</td>
                        <td colspan="3" class="tx_center"></td>
                    </tr>
                    <tr >
                    	<td class="f_bold black" >Wajib buka rekening CIMB Niaga</td>
                        <td colspan="3" class="tx_center">
                        	Ya
                        </td>
                    </tr>
                </tbody>
              </table>
              <br><br>

            </div>
        </div>
        
        
        <div class="row">
        	<div class="col-md-6">
            	<h4 class=" tcolor f_bold">BIAYA - BIAYA YANG MUNCUL</h4>
                  <table class="table_content" >
                    <tbody>
                        <tr >
                            <td class="f_bold black" >Biaya Administrasi**</td>
                            <td class="tx_center">tidak ada</td>
                        </tr>
                        <tr >
                            <td class="f_bold black" >Biaya Provisi</td>
                            <td class="tx_center">tidak ada</td>
                        </tr>
                        <tr >
                            <td class="f_bold black" >Denda Keterlambatan</td>
                            <td class="tx_center">5% per hari dari cicilan tertunggak</td>
                        </tr>
                        <tr >
                            <td class="f_bold black" >Biaya Pelunasa Dipercepat</td>
                            <td class="tx_center">5% dari sisa pokok pinjaman</td>
                        </tr>
                    </tbody>
                  </table>
                  <br><br>
            </div>
            
            <div class="col-md-6">
            	<h4 class=" tcolor f_bold">Pembayaran Angsuran</h4>
                  <table class="table_content" >
                  	<thead>
                    	<tr >
                            <td class="f_bold black bg_color_1" >Tempat Pembayaran</td>
                            <td class="tx_center bg_color_3">Biaya Transaksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr >
                            <td class="f_bold black" >Jaringan ATM Bersama</td>
                            <td class="tx_center">sesuai kebijakan masing - masing bank</td>
                        </tr>
                        <tr >
                            <td class="f_bold black" >Transfer/Kliring*</td>
                            <td class="tx_center">sesuai kebijakan masing - masing bank</td>
                        </tr>
                        <tr >
                            <td class="f_bold black" >Auto debet dari Rekening CIMB Niaga</td>
                            <td class="tx_center">0</td>
                        </tr>
                    </tbody>
                  </table>
                  <br><br>
            </div>
        </div>
         
        
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <!-- Services box element modern style -->
                <div class="services_box services_box--modern sb--hasicon">
                    <!-- Service box wrapper -->
                    <div class="services_box__inner clearfix">
                        <!-- Icon content -->
                        <div class="services_box__icon">
                            <!-- Icon wrapper -->
                            <div class="services_box__icon-inner">
                                <!-- Icon = .icon-process3  -->
                                <span class="services_box__fonticon icon-process3"></span>
                            </div>
                            <!--/ Icon wrapper -->
                        </div>
                        <!--/ Icon content -->

                        <!-- Content -->
                        <div class="services_box__content">
                            <!-- Title -->
                            <h4 class="services_box__title" style="padding-bottom: 40px;">Keunggulan</h4>
                            <!--/ Title -->

                            <!-- List wrapper -->
                            <div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <!-- List -->
                                <ul class="services_box__list">
                                    <li><span class="services_box__list-text">Tennor Hingga 4 tahun.</span></li>
                                    <li><span class="services_box__list-text">Pinjaman Hingga 400 juta</span></li>
                                    <li><span class="services_box__list-text">Persyaratan Lebih Simpel dan Proses Lebih Cepat 1 hari.</span></li>
                                    <li><span class="services_box__list-text">Bunga Ringan (1.49% /bulan)</span></li>
                                    <li><span class="services_box__list-text">Resiko Lebih Kecil (Karena Tanpa Agunan)</span></li>
                                    <li><span class="services_box__list-text">Data Bisa Dijemput Oleh Tim Marketing</span></li>
                                    <li><span class="services_box__list-text">Ada Banyak Promo Dan Crash Program</span></li>
                                </ul>
                                <!--/ List -->
                            </div>
                            <!--/ List wrapper -->
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Service box wrapper -->
                </div>
                <!--/ Services box element modern style -->
            </div>
            <!--/ col-md-6 col-sm-6 -->


        </div>
        
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <!-- Services box element modern style -->
                <div class="services_box services_box--modern sb--hasicon">
                    <!-- Service box wrapper -->
                    <div class="services_box__inner clearfix">
                        <!-- Icon content -->
                        <div class="services_box__icon">
                            <!-- Icon wrapper -->
                            <div class="services_box__icon-inner">
                                <!-- Icon = .icon-process3  -->
                                <span class="services_box__fonticon icon-process3"></span>
                            </div>
                            <!--/ Icon wrapper -->
                        </div>
                        <!--/ Icon content -->

                        <!-- Content -->
                        <div class="services_box__content">
                            <!-- Title -->
                            <h4 class="services_box__title" style="padding-bottom: 40px;">Dokumen Persyaratan</h4>
                            <!--/ Title -->

                            <!-- List wrapper -->
                            <div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <!-- List -->
                                <ul class="services_box__list">
                                    <li><span class="services_box__list-text">Fotocopy KTP</span></li>
                                    <li><span class="services_box__list-text">Slip Gaji (Hanya untuk karyawan)</span></li>
                                    <li><span class="services_box__list-text">NPWP Jika Pinjaman Lebih Dari Rp. 50.000.000,-</span></li>
                                    <li><span class="services_box__list-text">Fotocopy Kartu Kredit (Kecuali Karyawan &amp; Nasabah CIMB Niaga)</span></li>
                                    <li><span class="services_box__list-text">Resiko Lebih Kecil (Karena Tanpa Agunan)</span></li>
                                    <li><span class="services_box__list-text">Data Bisa Dijemput Oleh Tim Marketing</span></li>
                                    <li><span class="services_box__list-text">Ada Banyak Promo Dan Crash Program</span></li>
                                </ul>
                                <!--/ List -->
                            </div>
                            <!--/ List wrapper -->
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Service box wrapper -->
                </div>
                <!--/ Services box element modern style -->
            </div>
            <!--/ col-md-6 col-sm-6 -->
            
            <div class="col-md-6 col-sm-6">
            <h4 class="services_box__title" style="padding-bottom: 40px;">Lama Proses</h4>
            <strong>1 Hari Kerja</strong>
            </div>

        </div>
        
        
	</div>
	<!--/ container -->
</section>

<?php $this->load->view('pages/pengajuancepattanpajaminan');?>