<section class="hg_section pbottom-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- Info Box v2 style 2 element - light theme -->
                <div class="ib2-style2 ib2-text-color-light-theme ib2-custom" style="background-color:#fff;">
                    <div class="ib2-inner">
                        <!-- Title -->
                        <h4 class="ib2-info-message" style="font-size: 20px">PENGAJUAN CEPAT, SMS SEKARANG</h4>
                        <!--/ Title -->

                        <!-- Content -->
                        <div class="ib2-content">

                            <div class="row">
                                <div class="col-sm-2">
                                    <!-- User image -->
                                    <div class="ts-pt-testimonials__img" style="background-image:url('<?php echo base_url()?>dp/images/t2.png');" title="Customer Service">
                                    </div>
                                    <!--/ User image -->
                                </div>
                                <div class="col-sm-10">
                                    <!-- Content title -->
                                    <h2 class="ib2-content--title">Nama_Merek, Tipe, Tahun Mobil/Motor_Nilai Pinjaman_Kota.</h2>
                                    <!--/ Content title -->
                                    <!-- Content text -->
                                    <h5 class="ib2-content--text">Contoh: Doni_Toyota Avanza G 1.3 MT 2012_100 juta_Jakarta.</h5>
                                    <!--/ Content text -->

                                    <!-- Content title -->
                                    <h4 class="black" style="font-weight: bold">KIRIM KE: 081272776204.</h4>
                                    <h5 >maka tim kami akan segera memberikan simulasi pinjaman terbaik dari beberapa perusahaan pembiayaan untuk Anda.</h5>
                                    <!--/ Content title -->
                                </div>
                            </div>
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Info box v2 style 2 wrapper - .ib2-inner -->
                </div>
                <!--/ Info Box v2 style 2 element - light theme -->
            </div>
            <!--/ col-md-12 col-sm-12 -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>