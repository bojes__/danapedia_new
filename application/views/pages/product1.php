<!-- Page Sub-Header -->
<div id="page_header" class="page-subheader site-subheader-cst">
    <div class="bgback"></div>

    <!-- Animated Sparkles -->
    <div class="th-sparkles"></div>
    <!--/ Animated Sparkles -->

    <!-- Background -->
    <div class="kl-bg-source">
        <!-- Gradient overlay -->
        <div class="kl-bg-source__overlay" style="background:rgba(36,36,36,1); background: -moz-linear-gradient(left, rgba(36,36,36,1) 0%, rgba(107,107,107,1) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(36,36,36,1)), color-stop(100%,rgba(107,107,107,1))); background: -webkit-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: -o-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: -ms-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: linear-gradient(135deg,rgb(54,3,132),rgb(218,11,104)); ">
        </div>
        <!--/ Gradient overlay -->
    </div>
    <!--/ Background -->

    <!-- Sub-Header content wrapper -->
    <div class="ph-content-wrap">
        <div class="ph-content-v-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <!-- Breadcrumbs -->
                        <ul class="breadcrumbs fixclear">
                            <li><a href="<?php echo site_url('') ?>">Products</a></li>
                            <li class="active"><?php echo $pageTitle ?></li>
                        </ul>
                        <!-- Breadcrumbs -->

                        <!-- Current date -->
                        <span id="current-date" class="subheader-currentdate"><?php echo date("M d, Y") ?></span>
                        <!--/ Current date -->

                        <div class="clearfix"></div>
                    </div>
                    <!--/ col-sm-6 -->
                    <div class="col-sm-6">
                        <!-- Sub-header titles -->
                        <div class="subheader-titles">
                            <h2 class="subheader-maintitle"><?php echo $pageTitle ?></h2>
                            <h4 class="subheader-subtitle"><?php echo $pageNote ?></h4>
                        </div>
                        <!--/ Sub-header titles -->
                    </div>
                    <!--/ col-sm-6 -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ .ph-content-v-center -->
    </div>
    <!--/ Sub-Header content wrapper -->

    <!-- Bottom mask style 3 -->
    <div class="kl-bottommask kl-bottommask--mask3">
        <svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
                    <feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
                    <feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
                    <feMerge>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                        <feMergeNode in="shadowMatrixInner1"></feMergeNode>
                    </feMerge>
                </filter>
            </defs>
            <path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5"></path>
        </svg>
        <i class="glyphicon glyphicon-chevron-down"></i>
    </div>
    <!--/ Bottom mask style 3 -->
</div>
<!--/ Page sub-header -->

<section class="hg_section pbottom-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- Title element -->
                <div class="kl-title-block clearfix text-left tbk-symbol--  tbk-icon-pos--after-title">
                    <!-- Title with montserrat font and bold style -->
                    <h3 class="tbk__title montserrat fw-bold">DESKRIPSI</h3>

                    <!-- Sub-title with custom font size, line height and very thin style -->
                    <h4 class="tbk__subtitle fs-18 lh-32 fw-vthin">
                        Perusahaan pembiayaan (kreditur) akan memberikan pinjaman dana kepada Anda (debitur) dengan syarat ada aset yang Anda jaminkan. Hal ini dilakukan untuk menciptakan rasa aman dan trust
                        di kedua belah pihak (antara kreditur dan debitur).  Contoh aset yang bisa Anda jaminkan adalah BPKB Motor, BPKB mobil maupun Sertifikat Hak Milik Rumah/Ruko. Nilai atau harga aset yang Anda jaminkan
                        akan menentukan plafon pinjaman maksimal yang bisa Anda dapatkan. Semakin mahal harga motor/mobil/rumah yang BPKB atau sertifikatnya Anda jaminkan, maka semakin besar plafon pinjamannya.
                        Tentunya yang Anda jaminkan hanya BPKB/ Sertifikat nya, dimana Anda masih bisa menggunakan atau memanfaatkan fisik Kendaraan/ rumah tersebut.
                    </h4>
                </div>
                <!--/ Title element -->
            </div>
            <!--/ col-md-12 col-sm-12 -->

            <div class="col-md-12 col-sm-12">
                <!-- Services box element modern style -->
                <div class="services_box services_box--modern sb--hasicon">
                    <!-- Service box wrapper -->
                    <div class="services_box__inner clearfix">
                        <!-- Icon content -->
                        <div class="services_box__icon">
                            <!-- Icon wrapper -->
                            <div class="services_box__icon-inner">
                                <!-- Icon = .icon-process3  -->
                                <span class="services_box__fonticon icon-process3"></span>
                            </div>
                            <!--/ Icon wrapper -->
                        </div>
                        <!--/ Icon content -->

                        <!-- Content -->
                        <div class="services_box__content">
                            <!-- Title -->
                            <h4 class="services_box__title">Manfaat Pinjaman Dengan Agunan</h4>
                            <!--/ Title -->

                            <!-- Description -->
                            <div class="services_box__desc">
                                <p>
                                    Pinjaman dana dengan agunan dapat dimanfaatkan untuk berbagai kebutuhan baik yang bersifat produktif maupun konsumtif. Dalam kurun waktu yang singkat maupun jangka panjang.
                                </p>
                            </div>
                            <!--/ Description -->

                            <!-- List wrapper -->
                            <div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <!-- List -->
                                <ul class="services_box__list">
                                    <li><span class="services_box__list-text">Modal usaha/kerja</span></li>
                                    <li><span class="services_box__list-text">Renovasi Bangunan</span></li>
                                    <li><span class="services_box__list-text">Pelunasan Tagihan</span></li>
                                    <li><span class="services_box__list-text">Biaya Pendidikan</span></li>
                                    <li><span class="services_box__list-text">Biaya Kesehatan</span></li>
                                    <li><span class="services_box__list-text">Biaya Pernikahan</span></li>
                                    <li><span class="services_box__list-text">Wisata</span></li>
                                    <li><span class="services_box__list-text">dan aneka kebutuhan lain</span></li>
                                </ul>
                                <!--/ List -->
                            </div>
                            <!--/ List wrapper -->
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Service box wrapper -->
                </div>
                <!--/ Services box element modern style -->
            </div>
            <!--/ col-md-4 col-sm-4 -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>

<section class="hg_section pbottom-0">
	<div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class=""><strong>Ketentuan Umum</strong></h4>
                </div>
            </div>
            <div class="row">

                <div class="col-md-6 col-sm-12">


                
                <table class="table_content" >
                    <thead>
                        <tr class="tx_center">
                            <td class="bg_color_1 black f_bold fs-12">Parameter</td>
                            <td class="bg_color_3 black f_bold fs-12">Sub Parameter</td>
                            <td class="bg_color_3 black f_bold fs-12" colspan="2">Ketentuan</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $where = array(
                            array(
                                    'field' => 'tipe',
                                    'value' => 'Dengan Jaminan' 
                            ),
                            array(
                                    'field' => 'parameter',
                                    'value' => 'Jaminan BPKB' 
                            )
                        );

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td class="f_bold" rowspan="2">
                            Jaminan
                            </td>
                            <td>BPKB</td>
                            <td>
                                <?php echo $rs[0]['ketentuan']?>
                            </td>
                            <td>
                                <?php echo $rs[0]['deskripsi']?>
                            </td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Jaminan Sertifikat';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td>Sertifikat</td>
                            <td>
                                <?php echo $rs[0]['ketentuan']?>
                            </td>
                            <td>
                                <?php echo $rs[0]['deskripsi']?>
                            </td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Usia Aset Motor';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td class="f_bold" rowspan="3">
                            Usia Aset
                            </td>
                            <td>Motor</td>
                            <td>
                                <?php echo $rs[0]['ketentuan']?>
                            </td>
                            <td>
                                <?php echo $rs[0]['deskripsi']?>
                            </td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Usia Aset Mobil';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td>Mobil</td>
                            <td>
                                <?php echo $rs[0]['ketentuan']?>
                            </td>
                            <td>
                                <?php echo $rs[0]['deskripsi']?>
                            </td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Usia Aset Bangunan';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td>Bangunan</td>
                            <td>
                                <?php echo $rs[0]['ketentuan']?>
                            </td>
                            <td>
                                <?php echo $rs[0]['deskripsi']?>
                            </td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Usia Pemohon';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td class="f_bold" >
                            Usia Pemohon
                            </td>
                            <td></td>
                            <td>
                                <?php echo $rs[0]['ketentuan']?>
                            </td>
                            <td>
                                <?php echo $rs[0]['deskripsi']?>
                            </td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Tenor Min';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td class="f_bold" rowspan="2">
                            Tenor
                            </td>
                            <td>Min</td>
                            <td>
                                <?php echo $rs[0]['ketentuan']?>
                            </td>
                            <td>
                                <?php echo $rs[0]['deskripsi']?>
                            </td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Tenor Max';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            
                            <td>Max</td>
                            <td>
                                <?php echo $rs[0]['ketentuan']?>
                            </td>
                            <td>
                                <?php echo $rs[0]['deskripsi']?>
                            </td>
                        </tr>
                        
                        
                    </tbody>
                  </table>
                  <br>
                  <br> 
                </div>
                
                <div class="col-md-6 col-sm-12">


                
                <table class="table_content" >
                    <thead>
                        <tr class="tx_center">
                            <td class="bg_color_1 black f_bold fs-12">Parameter</td>
                            <td class="bg_color_3 black f_bold fs-12">Sub Parameter</td>
                            <td class="bg_color_3 black f_bold fs-12" colspan="2">Ketentuan</td>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php
                        $where[1]['value'] = 'Plafon Min';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td class="f_bold" rowspan="2">
                            Plafon
                            </td>
                            <td>Min</td>
                            <td>
                                <?php echo $rs[0]['ketentuan']?>
                            </td>
                            <td>
                                <?php echo $rs[0]['deskripsi']?>
                            </td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Plafon Max';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td>Max</td>
                            <td>
                                <?php echo $rs[0]['ketentuan']?>
                            </td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Bunga';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td class="f_bold" >
                                Bunga
                            </td>
                            <td></td>
                            <td>
                                <?php echo $rs[0]['ketentuan']?>
                            </td>
                            <td>
                                <?php echo $rs[0]['deskripsi']?>
                            </td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Lama Proses';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td class="f_bold" >
                                Lama Proses
                            </td>
                            <td>
                                min<br>
                                max
                            </td>
                            <td>
                                <?php echo $rs[0]['ketentuan']?>
                            </td>
                            <td>
                                <?php echo $rs[0]['deskripsi']?>
                            </td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Asuransi Motor Mobil';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td class="f_bold" rowspan="4" >
                                Asuransi
                            </td>
                            <td>
                                Motor<br>
                            </td>
                            <td rowspan="2">
                                <?php echo $rs[0]['ketentuan']?><br><br>
                            </td>
                            <td rowspan="4">
                                <?php echo $rs[0]['deskripsi']?>
                            </td>
                            
                        </tr>
                        
                        <tr >
                            <td>
                                Mobil
                            </td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Asuransi Bangunan';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td>
                                Bangunan
                            </td>
                            <td>
                                <?php echo $rs[0]['ketentuan']?>
                            </td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Asuransi Debitur';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td>
                                Debitur<br>
                            </td>
                            <td>
                                <?php echo $rs[0]['ketentuan']?>
                            </td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Wilayah';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td class="f_bold" >
                                Wilayah
                            </td>
                            <td></td>
                            <td>
                                <?php echo $rs[0]['ketentuan']?>
                            </td>
                            <td></td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Biaya - biaya BPKB Motor';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td class="f_bold" rowspan="3" >
                                Biaya - biaya
                            </td>
                            <td>
                                BPKB Motor
                            </td>
                            <td></td>
                            <td><?php echo $rs[0]['ketentuan']?></td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Biaya - biaya BPKB Mobil/Truk';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td>
                                BPKB Mobil/Truk
                            </td>
                            <td></td>
                            <td><?php echo $rs[0]['ketentuan']?></td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Biaya - biaya SHM';

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td>
                                SHM
                            </td>
                            <td></td>
                            <td>
                                <?php echo $rs[0]['ketentuan']?>
                            </td>
                        </tr>
                        
                    </tbody>
                  </table>
                  <br>
                  <br> 
                </div>

            </div>
         
        
	</div>
	<!--/ container -->
</section>



<section class="hg_section pbottom-0">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <!-- Services box element modern style -->
                <div class="services_box services_box--modern sb--hasicon">
                    <!-- Service box wrapper -->
                    <div class="services_box__inner clearfix">
                        <!-- Icon content -->
                        <div class="services_box__icon">
                            <!-- Icon wrapper -->
                            <div class="services_box__icon-inner">
                                <!-- Icon = .icon-process3  -->
                                <span class="services_box__fonticon icon-process3"></span>
                            </div>
                            <!--/ Icon wrapper -->
                        </div>
                        <!--/ Icon content -->

                        <!-- Content -->
                        <div class="services_box__content">
                            <!-- Title -->
                            <h4 class="services_box__title" style="padding-bottom: 40px;">Keunggulan</h4>
                            <!--/ Title -->

                            <!-- List wrapper -->
                            <div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <!-- List -->
                                <ul class="services_box__list">
                                    <li><span class="services_box__list-text">Proses lebih cepat dibandingkan pengajuan ke Bank.</span></li>
                                    <li><span class="services_box__list-text">Bunga ringan.</span></li>
                                    <li><span class="services_box__list-text">Tanpa BI Checking.</span></li>
                                    <li><span class="services_box__list-text">Aset diasuransikan (Asuransi kendaraan, Asuransi Jiwa dan Asuransi Kebakaran).</span></li>
                                    <li><span class="services_box__list-text">Nilai pinjaman tak dibatasi sesuai nilai aset Anda.</span></li>
                                    <li><span class="services_box__list-text">BPKB/ SHM tidak harus atas nama sendiri.</span></li>
                                    <li><span class="services_box__list-text">Bisa over kredit (alih kontrak), meski BPKB/ SHM masih dijaminkan di tempat lain.</span></li>
                                    <li><span class="services_box__list-text">STNK dengan pajak mati bisa tetap dibiayai.</span></li>
                                    <li><span class="services_box__list-text">Persyaratan simple dan fleksibel.</span></li>
                                    <li><span class="services_box__list-text">Berlaku untuk seluruh wilayah Indonesia.</span></li>
                                    <li><span class="services_box__list-text">Pembayaran angsuran di payment point: Alfamart, Indomaret, ATM, Kantor Pos.</span></li>
                                </ul>
                                <!--/ List -->
                            </div>
                            <!--/ List wrapper -->
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Service box wrapper -->
                </div>
                <!--/ Services box element modern style -->
            </div>
            <!--/ col-md-6 col-sm-6 -->

            <div class="col-md-6 col-sm-6">
                <!-- Services box element modern style -->
                <div class="services_box services_box--modern sb--hasicon">
                    <!-- Service box wrapper -->
                    <div class="services_box__inner clearfix">
                        <!-- Icon content -->
                        <div class="services_box__icon">
                            <!-- Icon wrapper -->
                            <div class="services_box__icon-inner">
                                <!-- Icon = .icon-process3  -->
                                <span class="services_box__fonticon icon-process3"></span>
                            </div>
                            <!--/ Icon wrapper -->
                        </div>
                        <!--/ Icon content -->

                        <!-- Content -->
                        <div class="services_box__content">
                            <!-- Title -->
                            <h4 class="services_box__title" style="padding-bottom: 40px;">Dokumen Persyaratan</h4>
                            <!--/ Title -->

                            <!-- List wrapper -->
                            <div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <!-- List -->
                                <ul class="services_box__list">
                                    <li><span class="services_box__list-text">Identitas diri (KTP & KK)</span></li>
                                    <li><span class="services_box__list-text">Bukti kepemilikan/sewa rumah atau kontrakan (Rekening listrik/air/Token PLN)</span></li>
                                    <li><span class="services_box__list-text">Bukti keuangan (Slip gaji /Buku Tabungan/ Bon usaha)</span></li>
                                    <li><span class="services_box__list-text">Copy STNK (untuk jaminan BPKB)</span></li>
                                    <li><span class="services_box__list-text">BPKB / Sertifikat asli (kecuali masih dijaminkan di tempat lain)</span></li>
                                    <li><span class="services_box__list-text">Khusus untuk jaminan SHM, siapkan juga copy IMB, PBB , NPWP</span></li>
                                </ul>
                                <!--/ List -->
                            </div>
                            <!--/ List wrapper -->
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Service box wrapper -->
                </div>
                <!--/ Services box element modern style -->
            </div>
            <!--/ col-md-6 col-sm-6 -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>

<section class="hg_section bg-white ptop-80 pbottom-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- Title element -->
                <div class="kl-title-block clearfix text-center tbk-symbol--line tbk--colored tbk-icon-pos--after-title">
                    <!-- Title -->
                    <h3 class="tbk__title ">ALUR PROSES PENGAJUAN PINJAMAN DENGAN JAMINAN</h3>
                    <!--/ Title -->

                    <!-- Title bottom symbol -->
                    <div class="tbk__symbol ">
                        <span></span>
                    </div>
                    <!--/ Title bottom symbol -->
                </div>
                <!--/ Title element -->

                <!-- Steps boxes element #4 - numbers style -->
                <div class="step-boxes-4">
                    <!-- Step #1 -->
                    <div class="process_box4">
                        <!-- Title -->
                        <h4 class="stp_title">Isi Form Online</h4>
                        <!--/ Title -->

                        <!-- Bubble wrapper -->
                        <div class="pb__line">
                            <!-- Number/Icon -->
                            <div class="number">
                                <!-- Number -->
                                <span>1</span>
                            </div>
                        </div>
                        <!--/ Bubble wrapper -->

                        <!-- Content -->
                        <div class="content">
                            <!-- Description -->
                            <p>
                                Silahkan isi Form Online yang ada di website ini.
                            </p>
                            <!--/ Description -->
                        </div>
                        <!--/ Content -->

                        <div class="clear">
                        </div>
                    </div>
                    <!--/ Step #1 -->

                    <!-- Step #2 -->
                    <div class="process_box4">
                        <!-- Title -->
                        <h4 class="stp_title">Deal</h4>
                        <!--/ Title -->

                        <!-- Bubble wrapper -->
                        <div class="pb__line">
                            <!-- Number/Icon -->
                            <div class="number">
                                <!-- Number -->
                                <span>2</span>
                            </div>
                        </div>
                        <!--/ Bubble wrapper -->

                        <!-- Content -->
                        <div class="content">
                            <!-- Description -->
                            <p>
                                Deal Nilai Pinjaman, Angsuran, Tenor sesuai dengan kebutuhan dan kemampuan Anda.
                            </p>
                            <!--/ Description -->
                        </div>
                        <!--/ Content -->

                        <div class="clear">
                        </div>
                    </div>
                    <!--/ Step #2 -->

                    <!-- Step #3 last -->
                    <div class="process_box4 last">
                        <!-- Title -->
                        <h4 class="stp_title">Persyaratan</h4>
                        <!--/ Title -->

                        <!-- Bubble wrapper -->
                        <div class="pb__line">
                            <!-- Number/Icon -->
                            <div class="number">
                                <!-- Number -->
                                <span>3</span>
                            </div>
                        </div>
                        <!--/ Bubble wrapper -->

                        <!-- Content -->
                        <div class="content">
                            <!-- Description -->
                            <p>
                                Siapkan Persyaratan yang dibutuhkan untuk proses.
                            </p>
                            <!--/ Description -->
                        </div>
                        <!--/ Content -->

                        <div class="clear">
                        </div>
                    </div>
                    <!--/ Step #3 last -->
                    <!-- Step #1 -->
                    <div class="process_box4">
                        <!-- Title -->
                        <h4 class="stp_title">Jemput Data</h4>
                        <!--/ Title -->

                        <!-- Bubble wrapper -->
                        <div class="pb__line">
                            <!-- Number/Icon -->
                            <div class="number">
                                <!-- Number -->
                                <span>4</span>
                            </div>
                        </div>
                        <!--/ Bubble wrapper -->

                        <!-- Content -->
                        <div class="content">
                            <!-- Description -->
                            <p>
                                Tim kami akan  menjemput data Anda &amp; Tanda tangan kontrak.
                            </p>
                            <!--/ Description -->
                        </div>
                        <!--/ Content -->

                        <div class="clear">
                        </div>
                    </div>
                    <!--/ Step #1 -->

                    <!-- Step #2 -->
                    <div class="process_box4">
                        <!-- Title -->
                        <h4 class="stp_title">Analisa Kredit</h4>
                        <!--/ Title -->

                        <!-- Bubble wrapper -->
                        <div class="pb__line">
                            <!-- Number/Icon -->
                            <div class="number">
                                <!-- Number -->
                                <span>5</span>
                            </div>
                        </div>
                        <!--/ Bubble wrapper -->

                        <!-- Content -->
                        <div class="content">
                            <!-- Description -->
                            <p>
                                Selanjutnya jika data yang anda berikan sudah lengkap, maka akan kami analisa terlebih dahulu.
                            </p>
                            <!--/ Description -->
                        </div>
                        <!--/ Content -->

                        <div class="clear">
                        </div>
                    </div>
                    <!--/ Step #2 -->

                    <!-- Step #2 -->
                    <div class="process_box4">
                        <!-- Title -->
                        <h4 class="stp_title">Approval</h4>
                        <!--/ Title -->

                        <!-- Bubble wrapper -->
                        <div class="pb__line">
                            <!-- Number/Icon -->
                            <div class="number">
                                <!-- Number -->
                                <span>6</span>
                            </div>
                        </div>
                        <!--/ Bubble wrapper -->

                        <!-- Content -->
                        <div class="content">
                            <!-- Description -->
                            <p>
                                Jika telah melewati proses Analisa Kredit, maka data anda sudah disetujui.
                            </p>
                            <!--/ Description -->
                        </div>
                        <!--/ Content -->

                        <div class="clear">
                        </div>
                    </div>
                    <!--/ Step #2 -->

                    <!-- Step #3 last -->
                    <div class="process_box4 last">
                        <!-- Title -->
                        <h4 class="stp_title">Dana Cair</h4>
                        <!--/ Title -->

                        <!-- Bubble wrapper -->
                        <div class="pb__line">
                            <!-- Number/Icon -->
                            <div class="number">
                                <!-- Number -->
                                <span>7</span>
                            </div>
                        </div>
                        <!--/ Bubble wrapper -->

                        <!-- Content -->
                        <div class="content">
                            <!-- Description -->
                            <p>
                                dan Anda dapat langsung menerima dana yang diajukan.
                            </p>
                            <!--/ Description -->
                        </div>
                        <!--/ Content -->

                        <div class="clear">
                        </div>
                    </div>
                    <!--/ Step #3 last -->
                </div>
                <!--/ Steps boxes element #4 - numbers style -->
            </div>
            <!--/ col-md-12 col-sm-12 -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>