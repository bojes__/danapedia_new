<!-- Page Sub-Header -->
<div id="page_header" class="page-subheader site-subheader-cst">
    <div class="bgback"></div>

    <!-- Animated Sparkles -->
    <div class="th-sparkles"></div>
    <!--/ Animated Sparkles -->

    <!-- Background -->
    <div class="kl-bg-source">
        <!-- Gradient overlay -->
        <div class="kl-bg-source__overlay" style="background:rgba(36,36,36,1); background: -moz-linear-gradient(left, rgba(36,36,36,1) 0%, rgba(107,107,107,1) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(36,36,36,1)), color-stop(100%,rgba(107,107,107,1))); background: -webkit-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: -o-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: -ms-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: linear-gradient(135deg,rgb(54,3,132),rgb(218,11,104)); ">
        </div>
        <!--/ Gradient overlay -->
    </div>
    <!--/ Background -->
    
    <!-- Sub-Header content wrapper -->
    <div class="ph-content-wrap">
        <div class="ph-content-v-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <!-- Breadcrumbs -->
                        <ul class="breadcrumbs fixclear">
                            <li><a href="<?php echo site_url('') ?>">Products</a></li>
                            <li class="active"><?php echo $pageTitle ?></li>
                        </ul>
                        <!-- Breadcrumbs -->

                        <!-- Current date -->
                        <span id="current-date" class="subheader-currentdate"><?php echo date("M d, Y") ?></span>
                        <!--/ Current date -->

                        <div class="clearfix"></div>
                    </div>
                    <!--/ col-sm-6 -->
                    <div class="col-sm-6">
                        <!-- Sub-header titles -->
                        <div class="subheader-titles">
                            <h2 class="subheader-maintitle"><?php echo $pageTitle ?></h2>
                            <h4 class="subheader-subtitle"><?php echo $pageNote ?></h4>
                        </div>
                        <!--/ Sub-header titles -->
                    </div>
                    <!--/ col-sm-6 -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ .ph-content-v-center -->
    </div>
    <!--/ Sub-Header content wrapper -->

    <!-- Bottom mask style 3 -->
    <div class="kl-bottommask kl-bottommask--mask3">
        <svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
                    <feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
                    <feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
                    <feMerge>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                        <feMergeNode in="shadowMatrixInner1"></feMergeNode>
                    </feMerge>
                </filter>
            </defs>
            <path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5"></path>
        </svg>
        <i class="glyphicon glyphicon-chevron-down"></i>
    </div>
    <!--/ Bottom mask style 3 -->
</div>
<!--/ Page sub-header -->

<section class="hg_section pbottom-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- Title element -->
                <div class="kl-title-block clearfix text-left tbk-symbol--  tbk-icon-pos--after-title">
                    <!-- Title with montserrat font and bold style -->
                    <h3 class="tbk__title montserrat fw-bold">DESKRIPSI</h3>

                    <!-- Sub-title with custom font size, line height and very thin style -->
                    <h4 class="tbk__subtitle fs-18 lh-32 fw-vthin">
                        Pinjaman/ kredit tanpa agunan merupakan produk keuangan yang dikeluarkan oleh Bank berupa pinjaman dana tunai dengan nominal tertentu sesuai dengan kapasitas keuangan debitur/pemohon.
                        Keunggulan dari produk ini adalah bahwa debitur/pemohon tidak perlu menyiapkan jaminan/agunan apapun. Bank akan menilai kelayakan debitur berdasarkan kapasitas (kemampuan bayar) dan
                        karakter debitur. Kapasitas dilihat dari penghasilan dan pengeluaran bulanan, limit atau tagihan kartu kredit yang ada. Sedangkan karakter dilihat dari  riwayat pembayaran kartu kredit debitur.
                    </h4>
                </div>
                <!--/ Title element -->
            </div>
            <!--/ col-md-12 col-sm-12 -->

            <div class="col-md-12 col-sm-12">
                <!-- Services box element modern style -->
                <div class="services_box services_box--modern sb--hasicon">
                    <!-- Service box wrapper -->
                    <div class="services_box__inner clearfix">
                        <!-- Icon content -->
                        <div class="services_box__icon">
                            <!-- Icon wrapper -->
                            <div class="services_box__icon-inner">
                                <!-- Icon = .icon-process3  -->
                                <span class="services_box__fonticon icon-process3"></span>
                            </div>
                            <!--/ Icon wrapper -->
                        </div>
                        <!--/ Icon content -->

                        <!-- Content -->
                        <div class="services_box__content">
                            <!-- Title -->
                            <h4 class="services_box__title">Manfaat Pinjaman Tanpa Agunan</h4>
                            <!--/ Title -->

                            <!-- Description -->
                            <div class="services_box__desc">
                                <p>
                                    Pinjaman dana tanpa agunan dapat dimanfaatkan untuk berbagai kebutuhan baik yang bersifat produktif maupun konsumtif. Dalam kurun waktu yang singkat maupun jangka panjang.
                                </p>
                            </div>
                            <!--/ Description -->

                            <!-- List wrapper -->
                            <div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <!-- List -->
                                <ul class="services_box__list">
                                    <li><span class="services_box__list-text">Modal usaha/kerja</span></li>
                                    <li><span class="services_box__list-text">Renovasi Bangunan</span></li>
                                    <li><span class="services_box__list-text">Pelunasan Tagihan</span></li>
                                    <li><span class="services_box__list-text">Biaya Pendidikan</span></li>
                                    <li><span class="services_box__list-text">Biaya Kesehatan</span></li>
                                    <li><span class="services_box__list-text">Biaya Pernikahan</span></li>
                                    <li><span class="services_box__list-text">Wisata</span></li>
                                    <li><span class="services_box__list-text">dan aneka kebutuhan lain</span></li>
                                </ul>
                                <!--/ List -->
                            </div>
                            <!--/ List wrapper -->
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Service box wrapper -->
                </div>
                <!--/ Services box element modern style -->
            </div>
            <!--/ col-md-4 col-sm-4 -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>


<section class="hg_section pbottom-0">
	<div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class=""><strong>Ketentuan Umum</strong></h4>
                </div>
            </div>
            <div class="row">

                <div class="col-md-6 col-sm-12">


                
                <table class="table_content" >
                    <thead>
                        <tr class="tx_center">
                            <td class="bg_color_1 black f_bold fs-12">Indikator</td>
                            <td class="bg_color_3 black f_bold fs-12" colspan="2">Ketentuan</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $where = array(
                            array(
                                    'field' => 'tipe',
                                    'value' => 'Tanpa Jaminan' 
                            ),
                            array(
                                    'field' => 'parameter',
                                    'value' => 'Cover Area' 
                            )
                        );

                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td>
                                Cover Area
                            </td>
                            <td><?php echo $rs[0]['ketentuan'];?></td>
                            <td><?php echo $rs[0]['deskripsi'];?></td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Usia Pemohon';
                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td>
                                Usia Pemohon
                            </td>
                            <td><?php echo $rs[0]['ketentuan'];?></td>
                            <td><?php echo $rs[0]['deskripsi'];?></td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Profil Pemohon';
                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td>
                                Profil Pemohon
                            </td>
                            <td><?php echo $rs[0]['ketentuan'];?></td>
                            <td><?php echo $rs[0]['deskripsi'];?></td>
                        </tr>
                         
                        <?php
                        $where[1]['value'] = 'Syarat Penghasilan';
                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td>
                                Syarat Penghasilan
                            </td>
                            <td><?php echo $rs[0]['ketentuan'];?></td>
                            <td><?php echo $rs[0]['deskripsi'];?></td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Kepemilikan Kartu Kredit';
                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td>
                                Kepemilikan Kartu Kredit
                            </td>
                            <td><?php echo $rs[0]['ketentuan'];?></td>
                            <td><?php echo $rs[0]['deskripsi'];?></td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Limit Kartu Kredit';
                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td>
                                Limit Kartu Kredit
                            </td>
                            <td><?php echo $rs[0]['ketentuan'];?></td>
                            <td><?php echo $rs[0]['deskripsi'];?></td>
                        </tr>
                        
                    </tbody>
                  </table>
                  <br>
                  <br> 
                </div>
                
                <div class="col-md-6 col-sm-12">

                <table class="table_content" >
                    <thead>
                        <tr class="tx_center">
                            <td class="bg_color_1 black f_bold fs-12">Indikator</td>
                            <td class="bg_color_3 black f_bold fs-12" colspan="2">Ketentuan</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $where[1]['value'] = 'Plafon Pinjaman';
                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td>
                                Plafon Pinjaman
                            </td>
                            <td><?php echo $rs[0]['ketentuan'];?></td>
                            <td><?php echo $rs[0]['deskripsi'];?></td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Tenor Pinjaman';
                        $rs = getKetentuan($where);
                        ?>
                        <tr >
                            <td>
                                Tenor Pinjaman
                            </td>
                            <td><?php echo $rs[0]['ketentuan'];?></td>
                            <td><?php echo $rs[0]['deskripsi'];?></td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Bunga';
                        $rs = getKetentuan($where);
                        ?>
                        <tr>
                            <td>
                                Bunga
                            </td>
                            <td><?php echo $rs[0]['ketentuan'];?></td>
                            <td><?php echo $rs[0]['deskripsi'];?></td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Lama Proses';
                        $rs = getKetentuan($where);
                        ?>
                        <tr>
                            <td>
                                Lama Proses
                            </td>
                            <td><?php echo $rs[0]['ketentuan'];?></td>
                            <td><?php echo $rs[0]['deskripsi'];?></td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'Pembayaran Angsuran';
                        $rs = getKetentuan($where);
                        ?>
                        <tr>
                            <td>
                                Pembayaran Angsuran
                            </td>
                            <td><?php echo $rs[0]['ketentuan'];?></td>
                            <td><?php echo $rs[0]['deskripsi'];?></td>
                        </tr>
                        
                        <?php
                        $where[1]['value'] = 'BI Checking';
                        $rs = getKetentuan($where);
                        ?>
                        <tr>
                            <td>
                                BI Checking
                            </td>
                            <td><?php echo $rs[0]['ketentuan'];?></td>
                            <td><?php echo $rs[0]['deskripsi'];?></td>
                        </tr>
                    </tbody>
                  </table>
                  <br>
                  <br> 
                </div>

            </div>
         
        
	</div>
	<!--/ container -->
</section>



<section class="hg_section pbottom-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- Services box element modern style -->
                <div class="services_box services_box--modern sb--hasicon">
                    <!-- Service box wrapper -->
                    <div class="services_box__inner clearfix">
                        <!-- Icon content -->
                        <div class="services_box__icon">
                            <!-- Icon wrapper -->
                            <div class="services_box__icon-inner">
                                <!-- Icon = .icon-process3  -->
                                <span class="services_box__fonticon icon-process3"></span>
                            </div>
                            <!--/ Icon wrapper -->
                        </div>
                        <!--/ Icon content -->

                        <!-- Content -->
                        <div class="services_box__content">
                            <!-- Title -->
                            <h4 class="services_box__title" style="padding-bottom: 40px;">Biaya -biaya yang muncul</h4>
                            <!--/ Title -->

                            <!-- List wrapper -->
                            <div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <!-- List -->
                                <ul class="services_box__list">
                                    <li>
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <span class="services_box__list-text">Biaya provisi</span>
                                            </div>
                                            <div class="col-md-1 col-sm-1">
                                                <span class="services_box__list-text">:</span>
                                            </div>
                                            <div class="col-md-8 col-sm-8">
                                                <span class="services_box__list-text">Dibayar di depan melalui pemotongan pencairan dana</span>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <span class="services_box__list-text">Biaya Admin</span>
                                            </div>
                                            <div class="col-md-1 col-sm-1">
                                                <span class="services_box__list-text">:</span>
                                            </div>
                                            <div class="col-md-8 col-sm-8">
                                                <span class="services_box__list-text"></span>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <span class="services_box__list-text">Denda keterlambatan</span>
                                            </div>
                                            <div class="col-md-1 col-sm-1">
                                                <span class="services_box__list-text">:</span>
                                            </div>
                                            <div class="col-md-8 col-sm-8">
                                                <span class="services_box__list-text">Biaya yang muncul apabila debitur terlambat membayar angsuran melewati jatuh tempo setiap bulannya. Denda keterlambatan dihitung harian.</span>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <span class="services_box__list-text">Biaya Tahunan</span>
                                            </div>
                                            <div class="col-md-1 col-sm-1">
                                                <span class="services_box__list-text">:</span>
                                            </div>
                                            <div class="col-md-8 col-sm-8">
                                                <span class="services_box__list-text">Pada tahun pertama biasanya dihitung berdasarkan prosentase tertentu terhadap nilai pinjaman, kemudian untuk tahun selanjutnya nominalnya fixed/tetap.</span>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3">
                                                <span class="services_box__list-text">Penalty pelunasan dipercepat</span>
                                            </div>
                                            <div class="col-md-1 col-sm-1">
                                                <span class="services_box__list-text">:</span>
                                            </div>
                                            <div class="col-md-8 col-sm-8">
                                                <span class="services_box__list-text">Biaya yang muncul jika debitur melunasi seluruh pinjaman lebih cepat dari tenor yang disepakati sebelumnya. Misal : Kesepakatan awal 3 tahun, namun di tahun ke-1, debitur melunasi seuruh angsuran yang tersisa.</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <!--/ List -->
                            </div>
                            <!--/ List wrapper -->
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Service box wrapper -->
                </div>
                <!--/ Services box element modern style -->
            </div>
            <!--/ col-md-12 col-sm-12 -->

            <div class="col-md-12 col-sm-12">
                <!-- Services box element modern style -->
                <div class="services_box services_box--modern sb--hasicon">
                    <!-- Service box wrapper -->
                    <div class="services_box__inner clearfix">
                        <!-- Icon content -->
                        <div class="services_box__icon">
                            <!-- Icon wrapper -->
                            <div class="services_box__icon-inner">
                                <!-- Icon = .icon-process3  -->
                                <span class="services_box__fonticon icon-process3"></span>
                            </div>
                            <!--/ Icon wrapper -->
                        </div>
                        <!--/ Icon content -->

                        <!-- Content -->
                        <div class="services_box__content">
                            <!-- Title -->
                            <h4 class="services_box__title" style="padding-bottom: 40px;">Keunggulan</h4>
                            <!--/ Title -->

                            <!-- List wrapper -->
                            <div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <!-- List -->
                                <ul class="services_box__list">
                                    <li><span class="services_box__list-text">Persyaratan lebih simple dan Proses lebih cepat.</span></li>
                                    <li><span class="services_box__list-text">Bunga ringan (untuk pinjaman besar).</span></li>
                                    <li><span class="services_box__list-text">Resiko lebih kecil (karena tanpa agunan).</span></li>
                                    <li><span class="services_box__list-text">Data bisa dijemput oleh tim marketing.</span></li>
                                    <li><span class="services_box__list-text">Ada banyak promo dan crash program.</span></li>
                                </ul>
                                <!--/ List -->
                            </div>
                            <!--/ List wrapper -->
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Service box wrapper -->
                </div>
                <!--/ Services box element modern style -->
            </div>
            <!--/ col-md-12 col-sm-12 -->

            <div class="col-md-12 col-sm-12">
                <!-- Services box element modern style -->
                <div class="services_box services_box--modern sb--hasicon">
                    <!-- Service box wrapper -->
                    <div class="services_box__inner clearfix">
                        <!-- Icon content -->
                        <div class="services_box__icon">
                            <!-- Icon wrapper -->
                            <div class="services_box__icon-inner">
                                <!-- Icon = .icon-process3  -->
                                <span class="services_box__fonticon icon-process3"></span>
                            </div>
                            <!--/ Icon wrapper -->
                        </div>
                        <!--/ Icon content -->

                        <!-- Content -->
                        <div class="services_box__content">
                            <!-- Title -->
                            <h4 class="services_box__title" style="padding-bottom: 40px;">Dokumen Persyaratan</h4>
                            <!--/ Title -->

                            <!-- List wrapper -->
                            <div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <!-- List -->
                                <ul class="services_box__list">
                                    <li><span class="services_box__list-text">Identitas diri (KTP).</span></li>
                                    <li><span class="services_box__list-text">Copy Kartu Kredit.</span></li>
                                    <li><span class="services_box__list-text">Copy cover buku tabungan pribadi.</span></li>
                                    <li><span class="services_box__list-text">Materai 1 lembar.</span></li>
                                    <li><span class="services_box__list-text">NPWP (hanya untuk pengajuan di atas 50 juta).</span></li>
                                </ul>
                                <!--/ List -->
                            </div>
                            <!--/ List wrapper -->
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Service box wrapper -->
                </div>
                <!--/ Services box element modern style -->
            </div>
            <!--/ col-md-12 col-sm-12 -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>

<section class="hg_section bg-white ptop-80 pbottom-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- Title element -->
                <div class="kl-title-block clearfix text-center tbk-symbol--line tbk--colored tbk-icon-pos--after-title">
                    <!-- Title -->
                    <h3 class="tbk__title ">ALUR PROSES PENGAJUAN PINJAMAN DENGAN JAMINAN</h3>
                    <!--/ Title -->

                    <!-- Title bottom symbol -->
                    <div class="tbk__symbol ">
                        <span></span>
                    </div>
                    <!--/ Title bottom symbol -->
                </div>
                <!--/ Title element -->

                <!-- Steps boxes element #4 - numbers style -->
                <div class="step-boxes-4">
                    <!-- Step #1 -->
                    <div class="process_box4">
                        <!-- Title -->
                        <h4 class="stp_title">Isi Form Online</h4>
                        <!--/ Title -->

                        <!-- Bubble wrapper -->
                        <div class="pb__line">
                            <!-- Number/Icon -->
                            <div class="number">
                                <!-- Number -->
                                <span>1</span>
                            </div>
                        </div>
                        <!--/ Bubble wrapper -->

                        <!-- Content -->
                        <div class="content">
                            <!-- Description -->
                            <p>
                                Silahkan isi Form Online yang ada di website ini.
                            </p>
                            <!--/ Description -->
                        </div>
                        <!--/ Content -->

                        <div class="clear">
                        </div>
                    </div>
                    <!--/ Step #1 -->

                    <!-- Step #2 -->
                    <div class="process_box4">
                        <!-- Title -->
                        <h4 class="stp_title">Deal</h4>
                        <!--/ Title -->

                        <!-- Bubble wrapper -->
                        <div class="pb__line">
                            <!-- Number/Icon -->
                            <div class="number">
                                <!-- Number -->
                                <span>2</span>
                            </div>
                        </div>
                        <!--/ Bubble wrapper -->

                        <!-- Content -->
                        <div class="content">
                            <!-- Description -->
                            <p>
                                Deal Nilai Pinjaman, Angsuran, Tenor sesuai dengan kebutuhan dan kemampuan Anda.
                            </p>
                            <!--/ Description -->
                        </div>
                        <!--/ Content -->

                        <div class="clear">
                        </div>
                    </div>
                    <!--/ Step #2 -->

                    <!-- Step #3 last -->
                    <div class="process_box4 last">
                        <!-- Title -->
                        <h4 class="stp_title">Persyaratan</h4>
                        <!--/ Title -->

                        <!-- Bubble wrapper -->
                        <div class="pb__line">
                            <!-- Number/Icon -->
                            <div class="number">
                                <!-- Number -->
                                <span>3</span>
                            </div>
                        </div>
                        <!--/ Bubble wrapper -->

                        <!-- Content -->
                        <div class="content">
                            <!-- Description -->
                            <p>
                                Siapkan Persyaratan yang dibutuhkan untuk proses.
                            </p>
                            <!--/ Description -->
                        </div>
                        <!--/ Content -->

                        <div class="clear">
                        </div>
                    </div>
                    <!--/ Step #3 last -->
                    <!-- Step #1 -->
                    <div class="process_box4">
                        <!-- Title -->
                        <h4 class="stp_title">Jemput Data</h4>
                        <!--/ Title -->

                        <!-- Bubble wrapper -->
                        <div class="pb__line">
                            <!-- Number/Icon -->
                            <div class="number">
                                <!-- Number -->
                                <span>4</span>
                            </div>
                        </div>
                        <!--/ Bubble wrapper -->

                        <!-- Content -->
                        <div class="content">
                            <!-- Description -->
                            <p>
                                Tim kami akan  menjemput data Anda &amp; Tanda tangan kontrak.
                            </p>
                            <!--/ Description -->
                        </div>
                        <!--/ Content -->

                        <div class="clear">
                        </div>
                    </div>
                    <!--/ Step #1 -->

                    <!-- Step #2 -->
                    <div class="process_box4">
                        <!-- Title -->
                        <h4 class="stp_title">Analisa Kredit</h4>
                        <!--/ Title -->

                        <!-- Bubble wrapper -->
                        <div class="pb__line">
                            <!-- Number/Icon -->
                            <div class="number">
                                <!-- Number -->
                                <span>5</span>
                            </div>
                        </div>
                        <!--/ Bubble wrapper -->

                        <!-- Content -->
                        <div class="content">
                            <!-- Description -->
                            <p>
                                Selanjutnya jika data yang anda berikan sudah lengkap, maka akan kami analisa terlebih dahulu.
                            </p>
                            <!--/ Description -->
                        </div>
                        <!--/ Content -->

                        <div class="clear">
                        </div>
                    </div>
                    <!--/ Step #2 -->

                    <!-- Step #2 -->
                    <div class="process_box4">
                        <!-- Title -->
                        <h4 class="stp_title">Approval</h4>
                        <!--/ Title -->

                        <!-- Bubble wrapper -->
                        <div class="pb__line">
                            <!-- Number/Icon -->
                            <div class="number">
                                <!-- Number -->
                                <span>6</span>
                            </div>
                        </div>
                        <!--/ Bubble wrapper -->

                        <!-- Content -->
                        <div class="content">
                            <!-- Description -->
                            <p>
                                Jika telah melewati proses Analisa Kredit, maka data anda sudah disetujui.
                            </p>
                            <!--/ Description -->
                        </div>
                        <!--/ Content -->

                        <div class="clear">
                        </div>
                    </div>
                    <!--/ Step #2 -->

                    <!-- Step #3 last -->
                    <div class="process_box4 last">
                        <!-- Title -->
                        <h4 class="stp_title">Dana Cair</h4>
                        <!--/ Title -->

                        <!-- Bubble wrapper -->
                        <div class="pb__line">
                            <!-- Number/Icon -->
                            <div class="number">
                                <!-- Number -->
                                <span>7</span>
                            </div>
                        </div>
                        <!--/ Bubble wrapper -->

                        <!-- Content -->
                        <div class="content">
                            <!-- Description -->
                            <p>
                                dan Anda dapat langsung menerima dana yang diajukan.
                            </p>
                            <!--/ Description -->
                        </div>
                        <!--/ Content -->

                        <div class="clear">
                        </div>
                    </div>
                    <!--/ Step #3 last -->
                </div>
                <!--/ Steps boxes element #4 - numbers style -->
            </div>
            <!--/ col-md-12 col-sm-12 -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>