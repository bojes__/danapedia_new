<!-- Page Sub-Header -->
<div id="page_header" class="page-subheader site-subheader-cst" style="">
    <div class="bgback"></div>

    <!-- Animated Sparkles -->
    <div class="th-sparkles"></div>
    <!--/ Animated Sparkles -->

    <!-- Background -->
    <div class="kl-bg-source">
        <!-- Gradient overlay -->
        <div class="kl-bg-source__overlay" style="background:rgba(36,36,36,1); background: -moz-linear-gradient(left, rgba(36,36,36,1) 0%, rgba(107,107,107,1) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(36,36,36,1)), color-stop(100%,rgba(107,107,107,1))); background: -webkit-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: -o-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: -ms-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: linear-gradient(135deg,rgb(54,3,132),rgb(218,11,104)); ">
        </div>
        <!--/ Gradient overlay -->
    </div>
    <!--/ Background -->
    <div class="col-sm-12" id="subheader_title" >
        <div class="subheader-titles" id="pagetitle">
            <h2 class="subheader-maintitle" style="margin:auto;"><?php echo $pageTitle ?></h2>
            <h4 class="subheader-maintitle" style="margin:auto;">isi form online dibawah ini maka tim kami akan segera memberikan simulasi pinjaman terbaik dari beberapa perusahaan pembiayaan untuk Anda.</h4>
        </div>
        <!--/ Sub-header titles -->
    </div>


    <!-- Bottom mask style 3 -->
    <div class="kl-bottommask kl-bottommask--mask3">
        <svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
                    <feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
                    <feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
                    <feMerge>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                        <feMergeNode in="shadowMatrixInner1"></feMergeNode>
                    </feMerge>
                </filter>
            </defs>
            <path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5"></path>
        </svg>
        <i class="glyphicon glyphicon-chevron-down"></i>
    </div>
    <!--/ Bottom mask style 3 -->
</div>
<!--/ Page sub-header -->

<section class="hg_section pbottom-0">
    <div class="container" >
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div style="text-align:center; font-size:16px;">
                    <br>
                        <span style="font-weight:bolder;" class="tx_color_1">ISI FORM ONLINE </span> 
                        &nbsp;&nbsp;
                        <span>
                            <input type="radio" readonly="readonly" checked="checked" name="jaminan" value="1"> Pinjaman Dengan Jaminan BPKB/SKHM 
                        </span>
                        &nbsp;&nbsp; 
                        <span>
                            <input type="radio" readonly="readonly" name="jaminan" value="1"> Pinjaman Tanpa Jaminan 
                        </span>
                        <br>
                            <br>
                                <br>
                </div>
            </div>
            <br><br>

        </div>

        <div class="row">
            <div class="col-md-8 col-sm-12">

                <form id="formonline" class="form-horizontal" parsley-validate='true' novalidate='true' method="post" action="<?php echo site_url('pagepost/pinjamandenganjaminan'); ?>" >

                    <div class="form-group">
                        <label for="tx_nama" class="col-sm-4 control-label">Nama</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="tx_nama" id="tx_nama" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tx_alamat" class="col-sm-4 control-label">Alamat</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="tx_alamat" id="tx_alamat">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tx_hp" class="col-sm-4 control-label">HP</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="tx_hp" id="tx_hp">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tx_wa" class="col-sm-4 control-label">WA</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="tx_wa" id="tx_wa">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tx_bbm" class="col-sm-4 control-label">BBM</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="tx_bbm" id="tx_bbm">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tx_fb" class="col-sm-4 control-label">Facebook</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="tx_fb" id="tx_fb">
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Penghasilan Perbulan</label>
                        <div class="col-sm-8">
                            <label class="col-xs-6">
                                <input type="radio" value="&lt; 20jt" name="rad_penghasilan_perbulan"> &lt; 20jt
                            </label>
                            <label class="col-xs-6">
                                <input type="radio" value="20jt - 50jt" name="rad_penghasilan_perbulan">  20jt - 50jt
                            </label>
                            <label class="col-xs-6">
                                <input type="radio" value="50jt - 100jt" name="rad_penghasilan_perbulan">  50jt - 100jt
                            </label>
                            <label class="col-xs-6">
                                <input type="radio" value="&gt; 100jt" name="rad_penghasilan_perbulan"> &gt; 100jt
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tx_fb" class="col-sm-4 control-label">Kepemilikan Rumah</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="ddl_kepemilikan_rumah">
                                <option value="">--Pilih--</option>
                                <option value="Milik Sendiri">Milik Sendiri</option>
                                <option value="Keluarga">Keluarga</option>
                                <option value="Sewa">Sewa</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Nilai Pinjaman</label>
                        <div class="col-sm-8">
                            <label class="col-xs-6">
                                <input type="radio" value="Maximal" name="rad_nilai_pinjaman"> Pinjaman Maksimal
                            </label>
                            <label class="col-xs-6">
                                <input type="radio" value="nominal" id="nom_nilai_pinjaman" name="rad_nilai_pinjaman">  Isi Nominal
                            </label>
                            <div id="nilai_pinjaman" class="col-sm-12 hide">
                                <input type="number" class="form-control" name="tx_nilai_pinjaman" >
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Tenor yang diinginkan</label>
                        <div class="col-sm-8">
                            <label class="col-xs-4">
                                <input type="radio" value="1" name="rad_tenor"> 1 Tahun
                            </label>
                            <label class="col-xs-4">
                                <input type="radio" value="2" name="rad_tenor"> 2 Tahun
                            </label>
                            <label class="col-xs-4">
                                <input type="radio" value="3" name="rad_tenor"> 3 Tahun
                            </label>
                            <label class="col-xs-4">
                                <input type="radio" value="4" name="rad_tenor"> 4 Tahun
                            </label>
                            <label class="col-xs-4">
                                <input type="radio" value="5" name="rad_tenor"> 5 Tahun
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tx_fb" class="col-sm-4 control-label">Jenis agunan</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="ddl_jenis_agunan">
                                <option value="">--Pilih--</option>
                                <option value="BPKB Mobil">BPKB Mobil</option>
                                <option value="BPKB Pick up">BPKB Pick up</option>
                                <option value="BPKB Truck">BPKB Truck</option>
                                <option value="SHM">SHM</option>
                                <option value="SHGB">SHGB</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tx_fb" class="col-sm-4 control-label">Merek</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="tx_merek" id="tx_merek">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tx_fb" class="col-sm-4 control-label">Tipe</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="tx_tipe" id="tx_tipe">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tx_fb" class="col-sm-4 control-label">Tahun</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="tx_tahun" id="tx_tahun">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tx_fb" class="col-sm-4 control-label">Plat nomor</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="tx_plat_no" id="tx_plat_no">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tx_fb" class="col-sm-4 control-label">Kondisi pajak</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="ddl_kondisi_pajak">
                                <option value="">--Pilih--</option>
                                <option value="Hidup">Hidup</option>
                                <option value="Mati">Mati</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tx_fb" class="col-sm-4 control-label">BPKB atas nama</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="ddl_bpkb_atas_nama">
                                <option value="">--Pilih--</option>
                                <option value="Sendiri">Sendiri</option>
                                <option value="Pasangan">Pasangan</option>
                                <option value="Keluarga">Keluarga</option>
                                <option value="Orang Lain">Orang Lain</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tx_fb" class="col-sm-4 control-label">Posisi BPKB</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="ddl_posisi_bpkb">
                                <option value="">--Pilih--</option>
                                <option value="Ditangan">Ditangan</option>
                                <option value="Masih di leasing atau bank">Masih di leasing atau bank</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tx_fb" class="col-sm-4 control-label">Kebutuhan prioritas</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="ddl_kebutuhan_prioritas">
                                <option value="">--Pilih--</option>
                                <option value="Dana Cair Paling Besar">Dana Cair Paling Besar</option>
                                <option value="Bunga Paling Ringan">Bunga Paling Ringan</option>
                                <option value="Proses Paling Cepat">Proses Paling Cepat</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Multi finance yang dipilih</label>
                        <div class="col-sm-8">
                            <label class="col-xs-12">
                                <input type="checkbox" value="Radana Finance" name="ck_multifinance[]"> Radana Finance
                            </label>
                            <label class="col-xs-12">
                                <input type="checkbox" value="BFI Finance" name="ck_multifinance[]"> BFI Finance
                            </label>
                            <label class="col-xs-12">
                                <input type="checkbox" value="SMS Finance" name="ck_multifinance[]"> SMS Finance
                            </label>
                            <label class="col-xs-12">
                                <input type="checkbox" value="MNC Finance" name="ck_multifinance[]"> MNC Finance
                            </label>
                            <label class="col-xs-12">
                                <input type="checkbox" value="Olympindo Multifinance" name="ck_multifinance[]"> Olympindo Multifinance
                            </label>
                            <label class="col-xs-12">
                                <input type="checkbox" value="Bandingkan Semua" name="ck_multifinance[]"> Bandingkan Semua
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <br>
                                <br>
                                    <button type="submit" class="ib-button-1 btn btn-fullcolor">
                                        <span id="bx_loading" class="hide">
                                            <img src="<?php echo base_url()?>dp/images/loading.gif" class="v_align_m">
                                                <span id="loading_status">Mohon tunggu...</span>
                                        </span>
                                        
                                        <span id="tx_btn">KIRIM</span>
                                    </button>
                                    <br><br><br>
                                                <br><br><br>
                                    </div>
                                    </div>
                                    </form>
            </div>

            <div class="col-md-4 col-sm-12">
                <div style="height:500px; width:100%; background-color:#F8DBFF"></div>
            </div>
        </div>

</div>
</section>

<?php $this->load->view('pages/pengajuancepatdenganjaminan');?>

<section id="cover_loading" class="hide" style="z-index:99999999;  position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; background-color: rgba(0,0,0,0.1);">
    
</section>
    
<script >
    function showLoading(statusMsg){
        jQuery("#cover_loading").removeClass("hide").addClass("show");
        jQuery("#loading_status").text(statusMsg);
        jQuery("#tx_btn").removeClass("show").addClass("hide");
        jQuery("#bx_loading").removeClass("hide").addClass("show");
        
    }
    
    function hideLoading(statusMsg){
        jQuery("#cover_loading").removeClass("show").addClass("hide");
        jQuery("#loading_status").text(statusMsg);
        
        setTimeout(function(){
            jQuery("#bx_loading").removeClass("show").addClass("hide");
            jQuery("#tx_btn").removeClass("hide").addClass("show");
        }, 3000);
        
    }
    
    jQuery("input[name='rad_nilai_pinjaman']").change(function(){
        var rad_nilai_pinjaman = jQuery(this).val();
        if(rad_nilai_pinjaman == 'nominal'){
            jQuery('#nilai_pinjaman').removeClass("hide").addClass("show");
        }else{
            jQuery('#nilai_pinjaman').removeClass("show").addClass("hide");
        }
    });

    jQuery("#formonline").submit(function (event) {
        event.preventDefault();
        
        showLoading("Mohon tunggu...");
        var form = jQuery(this),
                url = form.attr('action');

        var str = jQuery("#formonline").serialize();

        var posting = jQuery.post(url, jQuery(this).serialize());

        posting.done(function (data) {
            var obj = JSON.parse(data);
            hideLoading(obj.msg);
            if(obj.isSucces == true){
                window.location.href = '<?php echo site_url('ajukan-sekarang-jaminan');?>'; 
            }
        });
    });
</script>