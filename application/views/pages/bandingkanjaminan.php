<?php
$where = array(
    array(
            'field' => 'jaminan',
            'value' => 'BPKB Mobil' 
    ),
    array(
            'field' => 'multifinance',
            'value' => 'RADANA' 
    )
);

$radana = getPinjamanJaminan($where);

$where[1]['value'] = 'BFI Finance';
$bfi = getPinjamanJaminan($where);

$where[1]['value'] = 'SMS Finance';
$sms = getPinjamanJaminan($where);

$where[1]['value'] = 'MNC Finance';
$mnc = getPinjamanJaminan($where);

$where[1]['value'] = 'Olympindo';
$olympindo = getPinjamanJaminan($where);

$where = array(
    array(
            'field' => 'jaminan',
            'value' => 'BPKB Motor' 
    ),
    array(
            'field' => 'multifinance',
            'value' => 'RADANA' 
    )
);

$radana_motor = getPinjamanJaminan($where);

$where[1]['value'] = 'BFI Finance';
$bfi_motor = getPinjamanJaminan($where);

$where[1]['value'] = 'WOM Finance';
$wom_motor = getPinjamanJaminan($where);


?>
<!-- Page Sub-Header -->
<div id="page_header" class="page-subheader site-subheader-cst" style="">
    <div class="bgback"></div>

    <!-- Animated Sparkles -->
    <div class="th-sparkles"></div>
    <!--/ Animated Sparkles -->

    <!-- Background -->
    <div class="kl-bg-source">
        <!-- Gradient overlay -->
        <div class="kl-bg-source__overlay" style="background:rgba(36,36,36,1); background: -moz-linear-gradient(left, rgba(36,36,36,1) 0%, rgba(107,107,107,1) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(36,36,36,1)), color-stop(100%,rgba(107,107,107,1))); background: -webkit-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: -o-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: -ms-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: linear-gradient(135deg,rgb(54,3,132),rgb(218,11,104)); ">
        </div>
        <!--/ Gradient overlay -->
    </div>
    <!--/ Background -->
    <div class="col-sm-12" id="subheader_title" >
        <div class="subheader-titles" id="pagetitle">
                <h2 class="subheader-maintitle" style="margin:auto;"><?php echo $pageTitle ?></h2>
        </div>
        <!--/ Sub-header titles -->
    </div>
    
    
    <!-- Bottom mask style 3 -->
    <div class="kl-bottommask kl-bottommask--mask3">
        <svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
                    <feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
                    <feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
                    <feMerge>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                        <feMergeNode in="shadowMatrixInner1"></feMergeNode>
                    </feMerge>
                </filter>
            </defs>
            <path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5"></path>
        </svg>
        <i class="glyphicon glyphicon-chevron-down"></i>
    </div>
    <!--/ Bottom mask style 3 -->
</div>
<!--/ Page sub-header -->


<style>
</style>

<section class="hg_section pbottom-0">
    <div class="container">
        <div class="row">
        
            <div class="col-md-12 col-sm-12">
            <span>
            	Berikut ini perbandingan beberapa perusahaan pembiayaan pilihan tim Dana Ekspress dengan berbagai keunggulan masing - masing
            </span>
            <br><br>	
            
            <h4 class=" tcolor"><strong>BPKB MOBIL</strong></h4>
            <table class="table_content" style="max-width:1000px;">
                <thead>
                    <tr class="f_bold tx_center bg_color_white">
                      <td class=" black">Indikator</td>
                      <td  class=" tx_center">
                      		<img  src="<?php echo base_url()?>dp/images/logo-leasing/logo_bfi.png" alt="" title="" />
                      </td>
                      <td  class=" tx_center">
                      		<img   src="<?php echo base_url()?>dp/images/logo-leasing/logo_radana.png" alt="" title="" />
                      </td>
                      <td  class=" tx_center">
                      		<img style="width:105px;" src="<?php echo base_url()?>dp/images/logo-leasing/logo_sms.png" alt="" title="" />
                      </td>
                      <td  class=" tx_center">
                      		<img style="width:105px;" src="<?php echo base_url()?>dp/images/logo-leasing/logo_mnc.png" alt="" title="" />
                      </td>
                      <td  class=" tx_center">
                      		<img   src="<?php echo base_url()?>dp/images/logo-leasing/logo_olympindo.png" alt="" title="" />
                      </td>
                	</tr>
                </thead>
                <tbody>
                	<tr class="f_bold tx_center">
                      <td class="bg_color_1"></td>
                      <td  class="bg_color_3 tx_center"></td>
                      <td  class="bg_color_3 tx_center"></td>
                      <td  class="bg_color_3 tx_center"></td>
                      <td  class="bg_color_3 tx_center"></td>
                      <td  class="bg_color_3 tx_center"></td>
                	</tr>
                	<tr >
                      <td class="f_bold">
                      	Tahun Kendaraan
                      </td>
                      <td><?php echo $bfi[0]['tahunKendaraan']?></td>
                      <td><?php echo $radana[0]['tahunKendaraan']?></td>
                      <td><?php echo $sms[0]['tahunKendaraan']?></td>
                      <td><?php echo $mnc[0]['tahunKendaraan']?></td>
                      <td><?php echo $olympindo[0]['tahunKendaraan']?></td>
                	</tr>
                    <tr >
                      <td class="f_bold">
                      	Dana Cair
                      </td>
                      <td><?php echo $bfi[0]['danaCair']?></td>
                      <td><?php echo $radana[0]['danaCair']?></td>
                      <td><?php echo $sms[0]['danaCair']?></td>
                      <td><?php echo $mnc[0]['danaCair']?></td>
                      <td><?php echo $olympindo[0]['danaCair']?></td>
                	</tr>
                    <tr >
                      <td class="f_bold">
                      	Bunga
                      </td>
                      <td><?php echo $bfi[0]['bunga']?></td>
                      <td><?php echo $radana[0]['bunga']?></td>
                      <td><?php echo $sms[0]['bunga']?></td>
                      <td><?php echo $mnc[0]['bunga']?></td>
                      <td><?php echo $olympindo[0]['bunga']?></td>
                	</tr>
                    <tr >
                      <td class="f_bold">
                      	Tenor
                      </td>
                      <td><?php echo $bfi[0]['tenor']?></td>
                      <td><?php echo $radana[0]['tenor']?></td>
                      <td><?php echo $sms[0]['tenor']?></td>
                      <td><?php echo $mnc[0]['tenor']?></td>
                      <td><?php echo $olympindo[0]['tenor']?></td>
                	</tr>
                    <tr >
                      <td class="f_bold">
                      	Biaya/Potongan
                      </td>
                      <td><?php echo $bfi[0]['biayaPotongan']?></td>
                      <td><?php echo $radana[0]['biayaPotongan']?></td>
                      <td><?php echo $sms[0]['biayaPotongan']?></td>
                      <td><?php echo $mnc[0]['biayaPotongan']?></td>
                      <td><?php echo $olympindo[0]['biayaPotongan']?></td>
                	</tr>
                    <tr >
                      <td class="f_bold">
                      	Lama Proses
                      </td>
                      <td><?php echo $bfi[0]['lamaProses']?></td>
                      <td><?php echo $radana[0]['lamaProses']?></td>
                      <td><?php echo $sms[0]['lamaProses']?></td>
                      <td><?php echo $mnc[0]['lamaProses']?></td>
                      <td><?php echo $olympindo[0]['lamaProses']?></td>
                    </tr>
                </tbody>
              </table>
              <br>
            </div>
            
            <div class="col-md-12 col-sm-12">
            <h4 class=" tcolor"><strong>BPKB MOTOR</strong></h4>
                <table class="table_content" style="max-width:600px;">
                    <thead>
                        <tr class="f_bold tx_center bg_color_white">
                          <td class="black">Indikator</td>
                          <td  class=" tx_center">
                                <img  style="margin:0px 30px;" src="<?php echo base_url()?>dp/images/logo-leasing/logo_bfi.png" alt="" title="" />
                          </td>
                          <td  class=" tx_center">
                                <img   src="<?php echo base_url()?>dp/images/logo-leasing/logo_radana.png" alt="" title="" />
                          </td>
                          <td  class=" tx_center">
                                <img style="width:105px;" src="<?php echo base_url()?>dp/images/logo-leasing/wom-finance.jpg" alt="" title="WOM Finance" />
                          </td>                        
                        </tr>
                    </thead>
                    <tbody>
                    	<tr >
                          <td class="bg_color_1"></td>
                          <td  class="bg_color_3 "></td>
                          <td  class="bg_color_3 "></td>
                          <td  class="bg_color_3 "></td>                        
                        </tr>
                        <td class="f_bold">
                      	Tahun Kendaraan
                      </td>
                      <td><?php echo $bfi_motor[0]['tahunKendaraan']?></td>
                      <td><?php echo $radana_motor[0]['tahunKendaraan']?></td>
                      <td><?php echo $wom_motor[0]['tahunKendaraan']?></td>
                	</tr>
                    <tr >
                      <td class="f_bold">
                      	Dana Cair
                      </td>
                      <td><?php echo $bfi_motor[0]['danaCair']?></td>
                      <td><?php echo $radana_motor[0]['danaCair']?></td>
                      <td><?php echo $wom_motor[0]['danaCair']?></td>
                	</tr>
                    <tr >
                      <td class="f_bold">
                      	Bunga
                      </td>
                      <td><?php echo $bfi_motor[0]['bunga']?></td>
                      <td><?php echo $radana_motor[0]['bunga']?></td>
                      <td><?php echo $wom_motor[0]['bunga']?></td>
                	</tr>
                    <tr >
                      <td class="f_bold">
                      	Tenor
                      </td>
                      <td><?php echo $bfi_motor[0]['tenor']?></td>
                      <td><?php echo $radana_motor[0]['tenor']?></td>
                      <td><?php echo $wom_motor[0]['tenor']?></td>
                	</tr>
                    <tr >
                      <td class="f_bold">
                      	Biaya/Potongan
                      </td>
                      <td><?php echo $bfi_motor[0]['biayaPotongan']?></td>
                      <td><?php echo $radana_motor[0]['biayaPotongan']?></td>
                      <td><?php echo $wom_motor[0]['biayaPotongan']?></td>
                	</tr>
                    <tr >
                      <td class="f_bold">
                      	Lama Proses
                      </td>
                      <td><?php echo $bfi_motor[0]['lamaProses']?></td>
                      <td><?php echo $radana_motor[0]['lamaProses']?></td>
                      <td><?php echo $wom_motor[0]['lamaProses']?></td>
                    </tr>
                    </tbody>
                  </table>
              <br>
            </div>
            
            <?php   
              $where = array(   
                  array(    
                          'field' => 'multifinance',    
                          'value' => 'RADANA FINANCE'     
                  )   
              );    
              $radana = getSHM($where);   
              $where[0]['value'] = 'BFI FINANCE';   
              $bfi = getSHM($where);    
              $where[0]['value'] = 'MNC FINANCE';   
              $mnc = getSHM($where);    
            ?>

            <div class="col-md-12 col-sm-12">
              <h4 class=" tcolor"><strong>SERTIFIKAT HAK MILIK (SHM)</strong></h4>
                <table class="table_content" style="max-width:800px;">
                    <thead>
                        <tr class="f_bold tx_center black">
                          <td class="bg_color_1">Indikator</td>
                          <td  class="bg_color_3 ">
                              RADANA FINANCE
                          </td>
                          <td  class="bg_color_3 ">
                              MNC FINANCE
                          </td>
                          <td  class="bg_color_3 ">
                              BFI FINANCE
                          </td>                     
                        </tr>
                    </thead>
                    <tbody>
                      <tr>
                          <td >Dana Cair</td>
                          <td  class=" tx_center">
                              <?php echo $radana[0]['danaCair'];?>
                          </td>
                          <td  class=" tx_center">
                            <?php echo $mnc[0]['danaCair'];?>
                          </td>
                          <td  class=" tx_center">
                            <?php echo $bfi[0]['danaCair'];?>
                          </td>                      
                        </tr>
                        <tr>
                          <td >Bunga Flat Per Bulan</td>
                          <td  class=" tx_center">
                              <?php echo $radana[0]['bungaFlatPerBulan'];?>
                          </td>
                          <td  class=" tx_center">
                            <?php echo $mnc[0]['bungaFlatPerBulan'];?>
                          </td>
                          <td  class=" tx_center">
                            <?php echo $bfi[0]['bungaFlatPerBulan'];?>
                          </td>                       
                        </tr>
                        <tr>
                          <td >Tenor</td>
                          <td  class=" tx_center">
                              <?php echo $radana[0]['tenor'];?>
                          </td>
                          <td  class=" tx_center">
                            <?php echo $mnc[0]['tenor'];?>
                          </td>
                          <td  class=" tx_center">
                            <?php echo $bfi[0]['tenor'];?>
                          </td>                   
                        </tr>
                        <tr>
                          <td class="v_align_m">Biaya - Biaya</td>
                          <td  class=" tx_center">
                              <?php echo $radana[0]['biaya'];?>
                          </td>
                          <td  class=" tx_center">
                            <?php echo $mnc[0]['biaya'];?>
                          </td>
                          <td  class=" tx_center">
                            <?php echo $bfi[0]['biaya'];?>
                          </td>                     
                        </tr>
                        <tr>
                          <td class="v_align_m">Take Over</td>
                          <td  class=" tx_center">
                              <?php echo $radana[0]['takeOver'];?>
                          </td>
                          <td  class=" tx_center">
                            <?php echo $mnc[0]['takeOver'];?>
                          </td>
                          <td  class=" tx_center">
                            <?php echo $bfi[0]['takeOver'];?>
                          </td>                     
                        </tr>
                        
                    </tbody>
                  </table>
              <br>
                  <br>
            </div>
            
      </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>
