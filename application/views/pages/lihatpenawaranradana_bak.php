<?php
$where = array(
    array(
            'field' => 'jaminan',
            'value' => 'BPKB Mobil' 
    ),
    array(
            'field' => 'multifinance',
            'value' => 'RADANA' 
    )
);

$radana = getPinjamanJaminan($where);


$where = array(
    array(
            'field' => 'jaminan',
            'value' => 'BPKB Motor' 
    ),
    array(
            'field' => 'multifinance',
            'value' => 'RADANA' 
    )
);

$radana_motor = getPinjamanJaminan($where);

?>
<style>

    @media screen and (max-width: 768px) {
        .kl-header-bg{
            background:none !important;
        }
    }
</style>
<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="header_clearfix_1"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">

                <!-- Info Box v2 style 2 element - light theme -->
                <div class="ib2-style2 ib2-text-color-light-theme ib2-custom" style="background-color:#fff;">
                    <div class="ib2-inner">


                        <!-- Content -->
                        <div class="ib2-content" >

                            <div  class="row"  >
                                <div class="col-sm-3" style=" position:relative; height:200px;">
                                    <img class="img_v_align" style="width: auto; min-height: 110px;" src="<?php echo base_url() ?>dp/images/logo-leasing/logo_radana.png" alt="" title="" />
                                </div>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h2 class="ib2-content--title">PT. RADANA BHASKARA FINANCE, Tbk</h2>
                                            <br>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12 fs-14">
                                            (dulu bernama PT,  HD Finance Indonesia) Tiara Marga Trgakindo Group
                                        </div>
                                        <div class="col-sm-3 fs-16">
                                            <strong>Kantor Pusat</strong>
                                        </div>
                                        <div class="col-sm-9 fs-16">
                                            <span>The Bluegreen Boutique Office 5th floor</span><br>
                                            <span>Jl Lingkar Luar Barat Kav. 88</span><br>
                                            <span>Jakarta 11620</span><br>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3 fs-16">
                                            <strong>Berdiri Sejak</strong>
                                        </div>
                                        <div class="col-sm-9 fs-16">
                                            <span> 1972</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3 fs-16">
                                            <strong>Produk</strong>
                                        </div>
                                        <div class="col-sm-9 fs-16">
                                            <span>Pembiayaan multiguna sepeda motor, mobil, dan rumah</span><br>
                                            <span>Pembiayaan pembelian sepeda motor baru</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3 fs-16">
                                            <strong>Jumlah Cabang</strong>
                                        </div>
                                        <div class="col-sm-9 fs-16">
                                            <span>49 cabang (per desember 2015)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Info box v2 style 2 wrapper - .ib2-inner -->
                </div>
                <!--/ Info Box v2 style 2 element - light theme -->
            </div>
        </div>
    </div>
</section>

<section class="hg_section bg-white ptop-65">
    <div class="container">

        <div class="row">

            <div class="col-md-7 col-sm-12">


                <h4 class=" tcolor"><strong>BPKB MOBIL</strong></h4>
                <table class="table_content" style="max-width:1000px;">

                    <tbody>
                        <tr >
                            <td class="f_bold">
                                Jenis Mobil
                            </td>
                            <td>Mobil Penumpang dan Mobil Komersial (Pick UP, Truk, Tronton, BUS)</td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Merek
                            </td>
                            <td>Semua merek mobil</td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Tahun Kendaraan
                            </td>
                            <td><?php echo $radana[0]['tahunKendaraan']?></td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Dana Cair
                            </td>
                            <td><?php echo $radana[0]['danaCair']?></td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Bunga
                            </td>
                            <td><?php echo $radana[0]['bunga']?></td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Tenor
                            </td>
                            <td><?php echo $radana[0]['tenor']?></td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Biaya/Potongan
                            </td>
                            <td><?php echo $radana[0]['biayaPotongan']?></td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Lama Proses
                            </td>
                            <td><?php echo $radana[0]['lamaProses']?></td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Asuransi
                            </td>
                            <td>Asuransi kendaraan (TLO / All Risk) Asuransi Jiwa</td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <br>
            </div>

        </div>

        <div class="row">

            <div class="col-md-7 col-sm-12">


                <h4 class=" tcolor"><strong>BPKB MOTOR</strong></h4>
                <table class="table_content" style="max-width:1000px;">

                    <tbody>
                        <tr >
                            <td class="f_bold">
                                Jenis Motor
                            </td>
                            <td>Bebek, Sport, Matic</td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Merek
                            </td>
                            <td>Honda, Yamaha, Suzuki, Kawasaki, Bajaj</td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Tahun Kendaraan
                            </td>
                            <td><?php echo $radana_motor[0]['tahunKendaraan']?></td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Dana Cair
                            </td>
                            <td><?php echo $radana_motor[0]['danaCair']?></td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Bunga
                            </td>
                            <td><?php echo $radana_motor[0]['bunga']?></td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Tenor
                            </td>
                            <td><?php echo $radana_motor[0]['tenor']?></td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Biaya/Potongan
                            </td>
                            <td><?php echo $radana_motor[0]['biayaPotongan']?></td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Lama Proses
                            </td>
                            <td><?php echo $radana_motor[0]['lamaProses']?></td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Asuransi
                            </td>
                            <td>Asuransi kendaraan (TLO / All Risk) Asuransi Jiwa</td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <br>
            </div>

        </div>    

        <div class="row">

            <div class="col-md-7 col-sm-12">


                <h4 class=" tcolor"><strong>SERTIFIKAT HAK MILIK (SHM) / HAK GUNA BANGUNAN (SHGB)</strong></h4>
                <table class="table_content">

                    <tbody>
                        <tr >
                            <td class="f_bold">
                                Jenis Aset
                            </td>
                            <td>Rumah Huni/Kosong/Kontrakan, Ruko/Rukan, Apartment, Gudang, Pabrik </td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Merek
                            </td>
                            <td>Honda, Yamaha, Suzuki, Kawasaki, Bajaj</td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Tahun Kendaraan
                            </td>
                            <td>2007 - 2016</td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Dana Cair
                            </td>
                            <td>1 juta - 50 juta (tergantung harga second kendaraan)</td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Bunga
                            </td>
                            <td>Request table angsuran (tergantung usia dan jenis kendaraan)</td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Tenor
                            </td>
                            <td>12 bulan, 18 bulan, 24 bulan</td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Biaya/Potongan
                            </td>
                            <td>tidak ada</td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Lama Proses
                            </td>
                            <td>1 hari</td>
                        </tr>
                        <tr >
                            <td class="f_bold">
                                Asuransi
                            </td>
                            <td>Asuransi kendaraan (TLO / All Risk) Asuransi Jiwa</td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <br>
            </div>

        </div> 
        
        
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <!-- Services box element modern style -->
                <div class="services_box services_box--modern sb--hasicon">
                    <!-- Service box wrapper -->
                    <div class="services_box__inner clearfix">
                        <!-- Icon content -->
                        <div class="services_box__icon">
                            <!-- Icon wrapper -->
                            <div class="services_box__icon-inner">
                                <!-- Icon = .icon-process3  -->
                                <span class="services_box__fonticon icon-process3"></span>
                            </div>
                            <!--/ Icon wrapper -->
                        </div>
                        <!--/ Icon content -->

                        <!-- Content -->
                        <div class="services_box__content">
                            <!-- Title -->
                            <h4 class="services_box__title" style="padding-bottom: 40px;">Keunggulan</h4>
                            <!--/ Title -->

                            <!-- List wrapper -->
                            <div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <!-- List -->
                                <ul class="services_box__list">
                                    <li><span class="services_box__list-text">Prose lebih cepat dibandingkan dengan pengajuan ke Bank</span></li>
                                    <li><span class="services_box__list-text">Bunga ringan</span></li>
                                    <li><span class="services_box__list-text">Tanpa BI Checking</span></li>
                                    <li><span class="services_box__list-text">Aset diasuransikan( Asuransi Kendaraan, Asuransi Jiwa dan Asuransi Kebakaran)</span></li>
                                    <li><span class="services_box__list-text">Nilai pinjaman tak dibatasi sesuai dei dengan nilai aset Anda</span></li>
                                    <li><span class="services_box__list-text">BPKB/SHM tidak harus nama sendiri</span></li>
                                    <li><span class="services_box__list-text">Bisa overcredit (alih kontrak), meski BPKB/SHM masih dijaminkan di tempat lain</span></li>
                                    <li><span class="services_box__list-text">Persyaratan simple dan fleksibel</span></li>
                                    <li><span class="services_box__list-text">Berlaku untuk seluruh wilayah Indonesia</span></li>
                                    <li><span class="services_box__list-text">Pembayaran angsuran di payment point: Alfamart,Indomaret,ATM, Kantor Pos</span></li>
                                </ul>
                                <!--/ List -->
                            </div>
                            <!--/ List wrapper -->
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Service box wrapper -->
                </div>
                <!--/ Services box element modern style -->
            </div>
            <!--/ col-md-6 col-sm-6 -->

            <div class="col-md-6 col-sm-6">
                <!-- Services box element modern style -->
                <div class="services_box services_box--modern sb--hasicon">
                    <!-- Service box wrapper -->
                    <div class="services_box__inner clearfix">
                        <!-- Icon content -->
                        <div class="services_box__icon">
                            <!-- Icon wrapper -->
                            <div class="services_box__icon-inner">
                                <!-- Icon = .icon-process3  -->
                                <span class="services_box__fonticon icon-process3"></span>
                            </div>
                            <!--/ Icon wrapper -->
                        </div>
                        <!--/ Icon content -->

                        <!-- Content -->
                        <div class="services_box__content">
                            <!-- Title -->
                            <h4 class="services_box__title" style="padding-bottom: 40px;">Dokumen Persyaratan</h4>
                            <!--/ Title -->

                            <!-- List wrapper -->
                            <div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <!-- List -->
                                <ul class="services_box__list">
                                    <li><span class="services_box__list-text">Identitas diri (KTP &amp; KK)</span></li>
                                    <li><span class="services_box__list-text">Bukti kepemilikan/sewa rumah atau kontrakan (Rekening listrik,air,Token PLN)</span></li>
                                    <li><span class="services_box__list-text">Bukti keuangan (slip gaji/Buku Tabungan/Bon Usaha)</span></li>
                                    <li><span class="services_box__list-text">Copy STNK (untuk jaminan BPKB)</span></li>
                                    <li><span class="services_box__list-text">BPKB/Sertifikat Asli (kecuali masih dijaminkan ditempat lain)</span></li>
                                </ul>
                                <!--/ List -->
                            </div>
                            <!--/ List wrapper -->
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Service box wrapper -->
                </div>
                <!--/ Services box element modern style -->
            </div>
            <!--/ col-md-6 col-sm-6 -->

        </div>


    </div>
    <!--/ container -->
</section>

<?php $this->load->view('pages/pengajuancepatdenganjaminan');?>