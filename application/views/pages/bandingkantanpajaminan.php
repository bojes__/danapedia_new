<!-- Page Sub-Header -->
<div id="page_header" class="page-subheader site-subheader-cst" style="">
    <div class="bgback"></div>

    <!-- Animated Sparkles -->
    <div class="th-sparkles"></div>
    <!--/ Animated Sparkles -->

    <!-- Background -->
    <div class="kl-bg-source">
        <!-- Gradient overlay -->
        <div class="kl-bg-source__overlay" style="background:rgba(36,36,36,1); background: -moz-linear-gradient(left, rgba(36,36,36,1) 0%, rgba(107,107,107,1) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(36,36,36,1)), color-stop(100%,rgba(107,107,107,1))); background: -webkit-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: -o-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: -ms-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: linear-gradient(135deg,rgb(54,3,132),rgb(218,11,104)); ">
        </div>
        <!--/ Gradient overlay -->
    </div>
    <!--/ Background -->
    <div class="col-sm-12" id="subheader_title" >
        <div class="subheader-titles" id="pagetitle">
                <h2 class="subheader-maintitle" style="margin:auto;"><?php echo $pageTitle ?></h2>
        </div>
        <!--/ Sub-header titles -->
    </div>
    
    
    <!-- Bottom mask style 3 -->
    <div class="kl-bottommask kl-bottommask--mask3">
        <svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
                    <feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
                    <feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
                    <feMerge>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                        <feMergeNode in="shadowMatrixInner1"></feMergeNode>
                    </feMerge>
                </filter>
            </defs>
            <path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5"></path>
        </svg>
        <i class="glyphicon glyphicon-chevron-down"></i>
    </div>
    <!--/ Bottom mask style 3 -->
</div>
<!--/ Page sub-header -->


<style>
</style>

<section class="hg_section pbottom-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
              <table class="table_content" >
                <thead>
                	<tr class="bg_color_white">
                      <td class="" >Parameter</td>
                      <td  class=" tx_center">
                      	<img style="width:120px;" src="<?php echo base_url()?>dp/images/logo-leasing/logo_stancart.png" alt="" title="" />
                      </td>
                      <td  class=" tx_center">
                      	<img style="width:150px;" src="<?php echo base_url()?>dp/images/logo-leasing/logo_cimb.png" alt="" title="" />
                      </td>
                      <td  class=" tx_center">
                      	<img style="width:120px;" src="<?php echo base_url()?>dp/images/logo-leasing/logo_dbs.png" alt="" title="" />
                      </td>
                      <td  class=" tx_center">
                      	<img style="width:120px;" src="<?php echo base_url()?>dp/images/logo-leasing/logo_qnb.png" alt="" title="" />
                      </td>
                      <td  class=" tx_center">
                      	<img style="width:120px;" src="<?php echo base_url()?>dp/images/logo-leasing/logo_permata.png" alt="" title="" />
                      </td>
                	</tr>
                </thead>
                <tbody>
                	<tr class="f_bold">
                      <td class="bg_color_1">Parameter</td>
                      <td  class="bg_color_3 tx_center">
                      		KTA Standard Chartered
                      </td>
                      <td  class="bg_color_3 tx_center">
                      		X-TRA DANA CIMB Niaga
                      </td>
                      <td  class="bg_color_3 tx_center">
                      		DANA BANTUAN SAHABAT<br>
                            (DBS)
                      </td>
                      <td  class="bg_color_3 tx_center">
                      		QNB PERSONAL LOAN
                      </td>
                      <td  class="bg_color_3 tx_center">
                      		PERMATA KTA SPEED
                      </td>
                	</tr>
                    <tr >
                      <td >BI Checking</td>
                      <td  class=" tx_center">
                      		ada
                      </td>
                      <td  class=" tx_center">
                      		ada
                      </td>
                      <td  class=" tx_center">
                      		ada
                      </td>
                      <td  class=" tx_center">
                      		ada
                      </td>
                      <td  class=" tx_center">
                      		ada
                      </td>
                	</tr>
                    <tr >
                      <td >Platfon Maksimal</td>
                      <td  class=" tx_center">
                      		200 juta
                      </td>
                      <td  class=" tx_center">
                      		200 juta
                      </td>
                      <td  class=" tx_center">
                      		200 juta
                      </td>
                      <td  class=" tx_center">
                      		250 juta
                      </td>
                      <td  class=" tx_center">
                      		
                      </td>
                	</tr>
                	<tr >
                      <td >Tenor Maks</td>
                      <td  class=" tx_center">
                      		60 bulan
                      </td>
                      <td  class=" tx_center">
                      		48 bulan
                      </td>
                      <td  class=" tx_center">
                      		36 bulan
                      </td>
                      <td  class=" tx_center">
                      		72 bulan
                      </td>
                      <td  class=" tx_center">
                      		60 bulan
                      </td>
                	</tr>
                    <tr >
                      <td >Lama Proses</td>
                      <td  class=" tx_center">
                      		3 hari
                      </td>
                      <td  class=" tx_center">
                      		1 hari
                      </td>
                      <td  class=" tx_center">
                      		 
                      </td>
                      <td  class=" tx_center">
                      		 
                      </td>
                      <td  class=" tx_center">
                      		1 hari
                      </td>
                	</tr>
                    <tr >
                      <td >Area Cover</td>
                      <td  class=" tx_center">
                      		Jabodetabek, Bandung, Surabaya<br>
                            Solo, Yogyakarta, Bali
                      </td>
                      <td  class=" tx_center">
                      </td>
                      <td  class=" tx_center">
                      Jabodetabek, Bandung, Surabaya
                      </td>
                      <td  class=" tx_center">
                      Jabodetabek, Bandung, Surabaya
                      </td>
                      <td  class=" tx_center">
                      </td>
                	</tr>
                    <tr >
                      <td >Bunga</td>
                      <td  class=" tx_center">
                      		1.49 % s/d 1.89 %
                      </td>
                      <td  class=" tx_center">
                      		1.49 %
                      </td>
                      <td  class=" tx_center">
                      		0.99 % s/d 1.49%
                      </td>
                      <td  class=" tx_center">
                      		1.70 %
                      </td>
                      <td  class=" tx_center">
                      		1.19 % s/d 1.79 %
                      </td>
                	</tr>
                    <tr >
                      <td >Masa Aktif Kartu Kredit</td>
                      <td  class=" tx_center">
                      		1 tahun
                      </td>
                      <td  class=" tx_center">
                      </td>
                      <td  class=" tx_center">
                      		1 tahun sd 2 tahun
                      </td>
                      <td  class=" tx_center">
                      		1 tahun
                      </td>
                      <td  class=" tx_center">
                      		1 tahun sd 2 tahun
                      </td>
                	</tr>
                    <tr >
                      <td >Limit Kartu Kredit</td>
                      <td  class=" tx_center">
                      		8 juta
                      </td>
                      <td  class=" tx_center">
                      		
                      </td>
                      <td  class=" tx_center">
                      		7 juta sd 7 juta
                      </td>
                      <td  class=" tx_center">
                      		8 juta
                      </td>
                      <td  class=" tx_center">
                      		6 juta sd 7 juta
                      </td>
                	</tr>
                    <tr >
                      <td >Biaya Admin</td>
                      <td  class=" tx_center">
                      		tidak ada
                      </td>
                      <td  class=" tx_center">
                      		tidak ada
                      </td>
                      <td  class=" tx_center">
                      		399 ribu
                      </td>
                      <td  class=" tx_center">
                      		tidak ada
                      </td>
                      <td  class=" tx_center">
                      		tidak ada
                      </td>
                	</tr>
                    <tr >
                      <td >Biaya Provisi</td>
                      <td  class=" tx_center">
                      		3.5% dari pinjaman (tahun ke - 1) dan Rp. 50 ribu/tahun (tahun ke -2 dst)
                      </td>
                      <td  class=" tx_center">
                      		tidak ada
                      </td>
                      <td  class=" tx_center">
                      		tidak ada
                      </td>
                      <td  class=" tx_center">
                      		2.5 % dari pinjaman yang disetujui atau minimal sebesar Rp. 100 ribu
                      </td>
                      <td  class=" tx_center">
                      		1.5 % atau Min Rp. 150 ribu
                      </td>
                	</tr>
                    <tr >
                      <td >Denda Keterlambatan</td>
                      <td  class=" tx_center">
                      		Rp. 250.000,- atau 6 % dari cicilan tertunggak
                      </td>
                      <td  class=" tx_center">
                      		5 %  per hari dari cicilan tertunggak
                      </td>
                      <td  class=" tx_center">
                      		Rp. 250.000,- per keterlambatan
                      </td>
                      <td  class=" tx_center">
                      		4 % per hari dari cicilan tertunggak
                      </td>
                      <td  class=" tx_center">
                      		5 % dari cicilan atau minimum Rp. 50.000,-
                      </td>
                	</tr>
                    <tr >
                      <td >Pelunasan Dipercepat</td>
                      <td  class=" tx_center">
                      		6 % dari sisah pokok pinjaman
                      </td>
                      <td  class=" tx_center">
                      		5 % dari sisah pokok pinjaman
                      </td>
                      <td  class=" tx_center">
                      		8 % dari sisah pinjaman dan biaya lainnya
                      </td>
                      <td  class=" tx_center">
                      		5 % dari sisah pokok pinjaman
                      </td>
                      <td  class=" tx_center">
                      		5 % dari sisah pokok pinjaman
                      </td>
                	</tr>
                    <tr >
                      <td >Biaya Transfer Pencairan</td>
                      <td  class=" tx_center">
                      		30 ribu
                      </td>
                      <td  class=" tx_center">
                      		tidak ada
                      </td>
                      <td  class=" tx_center">
                      		30 ribu
                      </td>
                      <td  class=" tx_center">
                      		tidak ada
                      </td>
                      <td  class=" tx_center">
                      		tidak ada
                      </td>
                	</tr>
                    <tr >
                      <td >Biaya Tahunan</td>
                      <td  class=" tx_center">
                      		tidak ada
                      </td>
                      <td  class=" tx_center">
                      		tidak ada
                      </td>
                      <td  class=" tx_center">
                      		1.75 % dari jumlah pinjaman dan Rp. 65 ribu per tahun untuk tahun ke-2 dst
                      </td>
                      <td  class=" tx_center">
                      		tidak ada
                      </td>
                      <td  class=" tx_center">
                      		tidak ada
                      </td>
                	</tr>
                </tbody>
              </table>
              <br>
            </div>
            <div class="col-md-12 col-sm-12">
            <br>
            <br>
            <h4>Bandingkan juga PROMONYA...</h4>
              <table class="table_content" style="max-width:800px;">
                <thead>
                    <tr class="f_bold tx_center">
                      <td class="bg_color_1">Multifinance</td>
                      <td  class="bg_color_2 tx_center">
                      		Promo Title
                      </td>
                      <td  class="bg_color_3 tx_center">
                      		Description
                      </td>
                      <td  class="bg_color_3 tx_center">
                      		Masa Berlaku
                      </td>
                	</tr>
                </thead>
                <tbody class="f_bold">
                	<tr >
                      <td class="tx_center">
                      	<img style="width:105px;" src="<?php echo base_url()?>dp/images/logo-leasing/logo_stancart.png" alt="" title="" />
                      	<br>
                        KTA Standard Chartered	
                      </td>
                      <td  ></td>
                      <td  ></td>
                      <td  ></td>
                	</tr>
                    <tr >
                      <td class="tx_center">
                      
                      	<img style="width:160px;" src="<?php echo base_url()?>dp/images/logo-leasing/logo_cimb.png" alt="" title="" />
                      	<br>X-TRA DANA CIMB Niaga
                      </td>
                      <td  ></td>
                      <td  ></td>
                      <td  ></td>
                	</tr>
                    <tr >
                      <td class="tx_center">
                      	<img style="width:120px;" src="<?php echo base_url()?>dp/images/logo-leasing/logo_dbs.png" alt="" title="" />
                      	<br>DANA BANTUAN SAHABAT <br>
                        (DBS)
                      </td>
                      <td  ></td>
                      <td  ></td>
                      <td  ></td>
                	</tr>
                    <tr >
                      <td class="tx_center">
                      	<img style="width:110px;" src="<?php echo base_url()?>dp/images/logo-leasing/logo_qnb.png" alt="" title="" />
                      	<br>QNB PERSONAL LOAN
                      </td>
                      <td  ></td>
                      <td  ></td>
                      <td  ></td>
                	</tr>
                    <tr >
                      <td class="tx_center">
                      	<img style="width:170px;" src="<?php echo base_url()?>dp/images/logo-leasing/logo_permata.png" alt="" title="" />
                      	<br>PERMATA KTA SPEED
                      </td>
                      <td  ></td>
                      <td  ></td>
                      <td  ></td>
                	</tr>
                    
                </tbody>
              </table>
            </div>
            
      </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>
