<!-- Page Sub-Header -->
<div id="page_header" class="page-subheader site-subheader-cst" style="">
    <div class="bgback"></div>

    <!-- Animated Sparkles -->
    <div class="th-sparkles"></div>
    <!--/ Animated Sparkles -->

    <!-- Background -->
    <div class="kl-bg-source">
        <!-- Gradient overlay -->
        <div class="kl-bg-source__overlay" style="background:rgba(36,36,36,1); background: -moz-linear-gradient(left, rgba(36,36,36,1) 0%, rgba(107,107,107,1) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(36,36,36,1)), color-stop(100%,rgba(107,107,107,1))); background: -webkit-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: -o-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: -ms-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: linear-gradient(135deg,rgb(54,3,132),rgb(218,11,104)); ">
        </div>
        <!--/ Gradient overlay -->
    </div>
    <!--/ Background -->
    <div class="col-sm-12" id="subheader_title" >
        <div class="subheader-titles" id="pagetitle">
                <h2 class="subheader-maintitle" style="margin:auto;"><?php echo $pageTitle ?></h2>
        </div>
        <!--/ Sub-header titles -->
    </div>
    
    
    <!-- Bottom mask style 3 -->
    <div class="kl-bottommask kl-bottommask--mask3">
        <svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
                    <feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
                    <feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
                    <feMerge>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                        <feMergeNode in="shadowMatrixInner1"></feMergeNode>
                    </feMerge>
                </filter>
            </defs>
            <path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5"></path>
        </svg>
        <i class="glyphicon glyphicon-chevron-down"></i>
    </div>
    <!--/ Bottom mask style 3 -->
</div>
<!--/ Page sub-header -->

<section class="hg_section pbottom-0">
    <div class="container" style="max-width:1000px;">
        <div class="row">
            <div class="col-md-12 col-sm-12">
            	<div style="text-align:center; font-size:16px;">
                    <br>
                <span style="font-weight:bolder;" class="tx_color_1">ISI FORM ONLINE </span> 
                &nbsp;&nbsp;
                <span>
                    <input type="radio" readonly="readonly" name="jaminan" value="1"> Pinjaman Dengan Jaminan BPKB/SKHM 
                </span>
                &nbsp;&nbsp; 
                <span>
                    <input type="radio" readonly="readonly" checked="checked" name="jaminan" value="1"> Pinjaman Tanpa Jaminan 
                </span>
                <br><br>
                </div>
            </div>
            <br><br>

        </div>
        
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <form id="formonline" class="form-horizontal" parsley-validate='true' novalidate='true' method="post" action="<?php echo site_url('pagepost/pinjamantanpajaminan'); ?>" >
                  <div class="form-group">
                    <label for="tx_nama" class="col-sm-4 control-label">Nama</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="tx_nama" id="tx_nama" required >
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="tx_alamat" class="col-sm-4 control-label">Alamat</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="tx_alamat" required id="tx_alamat">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="tx_hp" class="col-sm-4 control-label">HP</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="tx_hp" required id="tx_hp">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="tx_wa" class="col-sm-4 control-label">WA</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="tx_wa" id="tx_wa">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="tx_bbm" class="col-sm-4 control-label">BBM</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="tx_bbm" id="tx_bbm">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="tx_fb" class="col-sm-4 control-label">Facebook</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="tx_fb" id="tx_fb">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label  class="col-sm-4 control-label">Penghasilan Perbulan</label>
                    <div class="col-sm-8">
                    	<label class="col-xs-6">
                            <input type="radio" name="rad_penghasilan_perbulan" value="&lt; 20jt" required> &lt; 20jt
                        </label>
                        <label class="col-xs-6">
                            <input type="radio" name="rad_penghasilan_perbulan" value="20jt - 50jt" required>  20jt - 50jt
                        </label>
                        <label class="col-xs-6">
                            <input type="radio" name="rad_penghasilan_perbulan" value="50jt - 100jt" required>  50jt - 100jt
                        </label>
                        <label class="col-xs-6">
                            <input type="radio" name="rad_penghasilan_perbulan" value="&gt; 100jt" required> &gt; 100jt
                        </label>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="tx_jumlah_cc" class="col-sm-4 control-label">Jumlah Kartu Kredit Aktif</label>
                    <div class="col-sm-8">
                        <input type="number" class="form-control"  name="tx_jumlah_cc" required id="tx_jumlah_cc">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label  class="col-sm-4 control-label">Usia Kartu Kredit</label>
                    <div class="col-sm-8">
                    	<label class="col-xs-6">
                          <input type="radio" value="&lt; 1 Tahun" name="rad_usia_cc" required> &lt; 1 Tahun
                        </label>
                        <label class="col-xs-6">
                          <input type="radio" value="&gt; 1 Tahun" name="rad_usia_cc" required> &gt; 1 Tahun
                        </label>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="tx_limit_cc" class="col-sm-4 control-label">Limit Kartu Kredit</label>
                    <div class="col-sm-8">
                      	<div class="input-group">
                          <input type="number" name="tx_limit_cc" id="tx_limit_cc" required class="form-control" aria-describedby="basic-addon2">
                          <span class="input-group-addon" id="basic-addon2"> juta</span>
                        </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="tx_pinjaman" class="col-sm-4 control-label">Pinjaman Yang Diinginkan</label>
                    <div class="col-sm-8">
                      	<div class="input-group">
                          <input required type="number" name="tx_pinjaman" id="tx_pinjaman" class="form-control" aria-describedby="basic-addon3">
                          <span class="input-group-addon" id="basic-addon3"> juta</span>
                        </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="tx_tenor" class="col-sm-4 control-label">Tenor yang diinginkan</label>
                    <div class="col-sm-4">
                      	<div class="input-group">
                          <input required type="number" name="tx_tenor" id="tx_tenor" class="form-control" aria-describedby="basic-addon4">
                          <span class="input-group-addon" id="basic-addon4"> tahun</span>
                        </div>
                    </div>
                  </div>
                       
                  
                  <div class="form-group">
                    <label  class="col-sm-4 control-label">Produk KTA yang dipilih</label>
                    <div class="col-sm-8">
                    	<label class="col-xs-12">
                          <input type="checkbox" value="Q Personal Loan" name="ck_kta[]"> Q Personal Loan
                        </label>
                        <label class="col-xs-12">
                          <input type="checkbox" value="KTA Mandiri" name="ck_kta[]"> KTA Mandiri
                        </label>
                        <label class="col-xs-12">
                          <input type="checkbox" value="Permata KTA" name="ck_kta[]"> Permata KTA
                        </label>
                        <label class="col-xs-12">
                          <input type="checkbox" value="CIMB Niaga Xtra Dana" name="ck_kta[]"> CIMB Niaga Xtra Dana
                        </label>
                        <label class="col-xs-12">
                          <input type="checkbox" value="KTA CBS" name="ck_kta[]"> KTA CBS
                        </label>
                        <label class="col-xs-12">
                          <input type="checkbox" value="Bandingkan Semua" name="ck_kta[]"> Bandingkan Semua
                        </label>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <br><br>
                        <button type="submit" class="ib-button-1 btn btn-fullcolor">
                            <span id="bx_loading" class="hide">
                                <img src="<?php echo base_url()?>dp/images/loading.gif" class="v_align_m">
                                    <span id="loading_status">Mohon tunggu...</span>
                            </span>

                            <span id="tx_btn">KIRIM</span>
                        </button>
                        <br><br><br><br><br><br>
                    </div>
                  </div>
                </form>
            </div>
        
        	<div class="col-md-4 col-sm-12">
            	<div style="height:500px; width:100%; background-color:#F8DBFF"></div>
            </div>
        </div>
     </div>
</section>


<?php $this->load->view('pages/pengajuancepattanpajaminan');?>

<section id="cover_loading" class="hide" style="z-index:99999999;  position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; background-color: rgba(0,0,0,0.1);">
    
</section>

<script >
    function showLoading(statusMsg){
        jQuery("#cover_loading").removeClass("hide").addClass("show");
        jQuery("#loading_status").text(statusMsg);
        jQuery("#tx_btn").removeClass("show").addClass("hide");
        jQuery("#bx_loading").removeClass("hide").addClass("show");
        
    }
    
    function hideLoading(statusMsg){
        jQuery("#cover_loading").removeClass("show").addClass("hide");
        jQuery("#loading_status").text(statusMsg);
        
        setTimeout(function(){
            jQuery("#bx_loading").removeClass("show").addClass("hide");
            jQuery("#tx_btn").removeClass("hide").addClass("show");
        }, 3000);
        
    }
    
    
    jQuery("input[name='rad_nilai_pinjaman']").change(function(){
        var rad_nilai_pinjaman = jQuery(this).val();
        if(rad_nilai_pinjaman == 'nominal'){
            jQuery('#nilai_pinjaman').removeClass("hide").addClass("show");
        }else{
            jQuery('#nilai_pinjaman').removeClass("show").addClass("hide");
        }
    });

    jQuery("#formonline").submit(function(event) {

        event.preventDefault();
        
        showLoading("Mohon tunggu...");
        
        var form = jQuery(this),
          url = form.attr( 'action' );
          
        var str = jQuery("#formonline" ).serialize();
    
        var posting = jQuery.post( url, jQuery(this).serialize() );

        posting.done(function( data ) {
            debugger;
            var obj = JSON.parse(data);
            hideLoading(obj.msg);
            if(obj.isSucces == true){
                window.location.href = '<?php echo site_url('ajukan-sekarang-tanpa-jaminan');?>'; 
            }
        });
    });
</script>