<?php
$where = [];
$rsCareer = getCareer($where);
?>
<!-- Page header + Bottom Mask style 3 -->
<div id="page_header" class="page-subheader site-subheader-cst uh_flat_dark_blue maskcontainer--mask3">
    <div class="bgback"></div>

    <!-- Animated Sparkles -->
    <div class="th-sparkles"></div>
    <!--/ Animated Sparkles -->

    <!-- Background -->
    <div class="kl-bg-source">
        <!-- Gradient overlay -->
        <div class="kl-bg-source__overlay" style="background:rgba(36,36,36,1); background: -moz-linear-gradient(left, rgba(36,36,36,1) 0%, rgba(107,107,107,1) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(36,36,36,1)), color-stop(100%,rgba(107,107,107,1))); background: -webkit-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: -o-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: -ms-linear-gradient(left, rgba(36,36,36,1) 0%,rgba(107,107,107,1) 100%); background: linear-gradient(135deg,rgb(54,3,132),rgb(218,11,104)); ">
        </div>
        <!--/ Gradient overlay -->
    </div>
    <!--/ Background -->

    <!-- Sub-Header content wrapper -->
    <div class="ph-content-wrap">
        <div class="ph-content-v-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <!-- Breadcrumbs -->
                        <ul class="breadcrumbs fixclear">
                            <li><a href="<?php echo site_url('') ?>">Home</a></li>
                            <li class="active"><?php echo $pageTitle ?></li>
                        </ul>
                        <!-- Breadcrumbs -->

                        <!-- Current date -->
                        <span id="current-date" class="subheader-currentdate"><?php echo date("M d, Y") ?></span>
                        <!--/ Current date -->
                        <div class="clearfix"></div>
                    </div>
                    <!--/ col-sm-6 -->
                    <div class="col-sm-6">
                        <!-- Sub-header titles -->
                        <div class="subheader-titles">
                            <h2 class="subheader-maintitle"><?php echo $pageTitle ?></h2>
                            <h4 class="subheader-subtitle"><?php echo $pageNote ?></h4>
                        </div>
                        <!--/ Sub-header titles -->
                    </div>
                    <!--/ col-sm-6 -->
                </div>
                <!-- end row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ .ph-content-v-center -->
    </div>
    <!-- Sub-Header content wrapper -->

    <!-- Bottom mask style 3 -->
    <div class="kl-bottommask kl-bottommask--mask3">
        <svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
                    <feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
                    <feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
                    <feMerge>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                        <feMergeNode in="shadowMatrixInner1"></feMergeNode>
                    </feMerge>
                </filter>
            </defs>
            <path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5"></path>
        </svg>
        <i class="glyphicon glyphicon-chevron-down"></i>
    </div>
    <!--/ Bottom mask style 3 -->
</div>
<!--/ Page header + Bottom Mask style 3 -->




<!-- Content page section -->
<section class="hg_section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- Page title & Subtitle-->
                <div class="kl-title-block clearfix tbk--left tbk-symbol--line tbk-icon-pos--after-title ptop-35 pbottom-65">
                    <h2 class="tbk__title fs-34 black montserrat"><strong>BERGABUNGLAH BERSAMA KAMI</strong></h2>
                    <div class="tbk__symbol">
                        <span></span>
                    </div>
                    <h4 class="tbk__subtitle fs-20 fw-thin">
                            Kami mengundang Anda, yang memiliki passion yang besar dalam bidang start-up untuk membangun karis prospektif bersama kami,
                             dan melakukan perkajaan - pekerjaan yang luarbiasa dari. Kami tawarkan milestone berharga untuk Anda dibeberapa posisi sebagai berikut 
                            
                    </h4>
                </div>
                <!--/ Page title & Subtitle-->
            </div>
            <!--/ col-md-12 col-sm-12 -->

                
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>
<!-- Content page section -->

<section class="bg_color_white">
    <div class="container">
        <?php
        foreach ($rsCareer as $item) {
        ?>
        <div class="row">
            <div class="col-md-12">
                <h4 class="tbk__title fs-18 black montserrat">
                    <strong><?php echo $item['title'];?></strong>
                </h4>    
            </div>
        </div> 
        <div class="row">
            <div class="col-md-1 col-sm-3">
                <strong>Deskripsi</strong>
            </div>
            <div class="col-md-6 col-sm-7">
                <?php echo $item['desc'];?>
                <br>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-1 col-sm-3">
                <strong>Kualifikasi</strong>
            </div>
            <div class="col-md-6 col-sm-7">
                <?php echo $item['skill_requirement'];?>
                <br>
            </div>
        </div>
        
        
        <div class="row">
            <div class="col-md-12">
                <br>
                <hr>
                <br>
            </div>
        </div>
        <?php
        }
        ?>
        
        
    </div>
    
</section>

<!-- Contact form & details section with custom paddings -->
<section id="contact" class="hg_section p-0 pbottom-80">
    <br><br><br>    
   
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9">
                <!-- Contact form -->
                <div class="contactForm">
                    <form id="formonline" class="contact_form row" method="post" action="<?php echo site_url('pagepost/careers'); ?>" >
                
                        <div class="cf_response"></div>
                        <p class="col-sm-6 kl-fancy-form">
                            <input type="text" name="tx_namadepan" id="cf_name" class="form-control" placeholder="Please enter your first name" value="" tabindex="1" maxlength="35" required>
                            <label class="control-label">FIRSTNAME</label>
                        </p>
                        <p class="col-sm-6 kl-fancy-form">
                            <input type="text" name="tx_namabelakang" id="cf_lastname" class="form-control" placeholder="Please enter your first last name" value="" tabindex="1" maxlength="35" required>
                            <label class="control-label">LASTNAME</label>
                        </p>
                        <p class="col-sm-12 kl-fancy-form">
                            <input type="email" name="tx_email" id="cf_email" class="form-control h5-email" placeholder="Please enter your email address" value="" tabindex="1" maxlength="35" required>
                            <label class="control-label">EMAIL</label>
                        </p>
                        <p class="col-sm-12 kl-fancy-form">
                            <input type="text" name="tx_telepon" id="cf_phone" class="form-control h5-phone" placeholder="Please enter phone number" value="" tabindex="1" maxlength="35" required>
                            <label class="control-label">PHONE</label>
                        </p>
                        <p class="col-sm-12 kl-fancy-form">
                            <input type="text" name="tx_subject" id="cf_subject" class="form-control" placeholder="Silahkan isi posisi yang ingin di-lamar" value="" tabindex="1" maxlength="35" required>
                            <label class="control-label">POSISI</label>
                        </p>
                        <p class="col-sm-12 kl-fancy-form">
                            <textarea name="tx_pesan" id="cf_message" class="form-control" cols="30" rows="10" placeholder="SIlahkan uraikan pendidikan dan pengalaman kerja Anda" tabindex="4" required></textarea>
                            <label class="control-label">PROFIL</label>
                        </p>

                        <!-- Google recaptcha required site-key -->
                        <div class="g-recaptcha" data-sitekey="xxxx"></div>

                        <p class="col-sm-12">
                            <span id="btnSubmit" class="ib-button-1 btn btn-fullcolor">
                                <span id="bx_loading" class="hide">
                                    <img src="<?php echo base_url()?>dp/images/loading.gif" class="v_align_m">
                                        <span id="loading_status">Mohon tunggu...</span>
                                </span>

                                <span id="tx_btn">KIRIM</span>
                            </span>
                        </p>

                    </form>
                </div>
                <!--/ Contact form -->
            </div>
            <!--/ col-md-9 col-sm-9 -->

            <div class="col-md-3 col-sm-3">
                <!-- Contact details -->
                <div class="text_box">
                    <h3 class="text_box-title text_box-title--style2">CONTACT INFO</h3>
                    <h4>Taman Topi Square, lt 1 Blok G no 6. Jln Kapten Muslihat - Paledang, Kota Bogor</h4>
                    <p>
                       (+62) 812 7277 6204
                    </p>
                    <p>
                        <a href="mailto:#">hello@danapedia.com</a><br>
                    </p>
                </div>
                <!--/ Contact details -->
            </div>
            <!--/ col-md-3 col-sm-3 -->
        </div>
        <!--/ .row -->
    </div>
    <!--/ .container -->
</section>
<!--/ Contact form & details section with custom paddings -->

<section id="cover_loading" class="hide" style="z-index:99999999;  position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; background-color: rgba(0,0,0,0.1);">
    
</section>

<script >
    function showLoading(statusMsg){
        jQuery("#cover_loading").removeClass("hide").addClass("show");
        jQuery("#loading_status").text(statusMsg);
        jQuery("#tx_btn").removeClass("show").addClass("hide");
        jQuery("#bx_loading").removeClass("hide").addClass("show");
        
    }
    
    function hideLoading(statusMsg){
        jQuery("#cover_loading").removeClass("show").addClass("hide");
        jQuery("#loading_status").text(statusMsg);
        
        setTimeout(function(){
            jQuery("#bx_loading").removeClass("show").addClass("hide");
            jQuery("#tx_btn").removeClass("hide").addClass("show");
        }, 3000);
        
    }
    
    
    jQuery("input[name='rad_nilai_pinjaman']").change(function(){
        var rad_nilai_pinjaman = jQuery(this).val();
        if(rad_nilai_pinjaman == 'nominal'){
            jQuery('#nilai_pinjaman').removeClass("hide").addClass("show");
        }else{
            jQuery('#nilai_pinjaman').removeClass("show").addClass("hide");
        }
    });

    jQuery("#formonline").submit(function(event) {

        event.preventDefault();
        
        showLoading("Mohon tunggu...");
        
        var form = jQuery(this),
          url = form.attr( 'action' );
          
        var str = jQuery("#formonline" ).serialize();
    
        var posting = jQuery.post( url, jQuery(this).serialize() );

        posting.done(function( data ) {
            debugger;
            var obj = JSON.parse(data);
            hideLoading(obj.msg);
            if(obj.isSucces == true){
                window.location.href = '<?php echo site_url('careers');?>'; 
            }
        });
    });
    
    jQuery("#btnSubmit").click(function(){
        showLoading("Mohon tunggu...");
        
        var form = jQuery("#formonline"),
          url = form.attr( 'action' );
          
        var str = jQuery("#formonline" ).serialize();
    
        var posting = jQuery.post( url, jQuery("#formonline").serialize() );

        posting.done(function( data ) {
            debugger;
            var obj = JSON.parse(data);
            hideLoading(obj.msg);
            if(obj.isSucces == true){
                window.location.href = '<?php echo site_url('careers');?>'; 
            }
        });
    });
</script>