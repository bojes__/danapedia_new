<section class="hg_section pbottom-80">
    <div class="container" style="max-width: 900px; margin: auto;">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- Info Box v2 style 2 element - light theme -->
                <div class="ib2-style2 ib2-text-color-light-theme ib2-custom" style="background-color:#fff;">
                    <div class="ib2-inner">
                        <!-- Title -->
                        <h2 class="ib2-info-message" style="font-size: 20px">PENGAJUAN CEPAT, SMS SEKARANG DENGAN FORMAT :</h2>
                        <!--/ Title -->

                        <!-- Content -->
                        <div class="ib2-content">

                            <div class="row">
                                
                                <div class="col-sm-12">
                                    <h3 class="ib2-content--title">a. Jaminan BPKB</h3>
                                    
                                    <!-- Content title -->
                                    <h2 class="ib2-content--title">Nama_Merek, Tipe, Tahun Mobil/Motor_Nilai Pinjaman_Alamat.</h2>
                                    <!--/ Content title -->
                                    <!-- Content text -->
                                    <h5 class="ib2-content--text">Contoh: Doni_Toyota Avanza G 1.3 MT 2012_100 juta_Jl Mawar No 43 A. Pejaten Jaksel.</h5>
                                    <!--/ Content text -->
                                    
                                    <hr>
                                    
                                    <h3 class="ib2-content--title">b. Jaminan Sertifikat</h3>
                                    <!-- Content title -->
                                    <h2 class="ib2-content--title">Nama_Nilai Pinjaman_Alamat Lengkap_Posisi Sertifikat.</h2>
                                    <!--/ Content title -->
                                    <!-- Content text -->
                                    <h5 class="ib2-content--text">Contoh: Doni_500 juta_Jl Mawar No 43 A. Pejaten Jaksel_Masih ada di Bank BRI</h5>
                                    <!--/ Content text -->
                                    
                                    <hr>
                                    
                                    <!-- Content title -->
                                    <h4 class="black" style="font-weight: bold">KIRIM KE: 081272776204.</h4>
                                    <h5 >maka tim kami akan segera memberikan simulasi pinjaman terbaik dari beberapa perusahaan pembiayaan untuk Anda.</h5>
                                    <!--/ Content title -->
                                </div>
                            </div>
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Info box v2 style 2 wrapper - .ib2-inner -->
                </div>
                <!--/ Info Box v2 style 2 element - light theme -->
            </div>
            <!--/ col-md-12 col-sm-12 -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>