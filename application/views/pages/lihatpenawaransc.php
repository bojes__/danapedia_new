<style>

    @media screen and (max-width: 768px) {
        .kl-header-bg{
            background:none !important;
        }
    }
</style>
<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="header_clearfix_1"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">

                <!-- Info Box v2 style 2 element - light theme -->
                <div class="ib2-style2 ib2-text-color-light-theme ib2-custom" style="background-color:#fff;">
                    <div class="ib2-inner">


                        <!-- Content -->
                        <div class="ib2-content" >

                            <div  class="row"  >
                                <div class="col-sm-3" style=" position:relative; height:200px;">
                                    <img class="img_v_align" src="<?php echo base_url() ?>dp/images/logo-leasing/logo_stancart.png" alt="" title="" />
                                </div>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h2 class="ib2-content--title">Standard Chartered Bank</h2>
                                            <br>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3 fs-16">
                                            <strong>Kantor Pusat</strong>
                                        </div>
                                        <div class="col-sm-9 fs-16">
                                            <span>Menara Standard Chartered Lt Dasar, Jalan Profesor Doktor Satrio</span><br>
                                            <span>No. 164, Setiabudi, Daerah Khusus Ibukota Jakarta 12930</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3 fs-16">
                                            <strong>Berdiri Sejak</strong>
                                        </div>
                                        <div class="col-sm-9 fs-16">
                                            <span> 1863 </span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3 fs-16">
                                            <strong>Jumlah Produk</strong>
                                        </div>
                                        <div class="col-sm-9 fs-16">
                                            <span>&nbsp;-&nbsp;</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3 fs-16">
                                            <strong>Jumlah Cabang (di Indonesia)</strong>
                                        </div>
                                        <div class="col-sm-9 fs-16">
                                            <span>27 cabang</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Info box v2 style 2 wrapper - .ib2-inner -->
                </div>
                <!--/ Info Box v2 style 2 element - light theme -->
            </div>
        </div>
    </div>
</section>

<section class="hg_section bg-white ptop-65">
    <div class="container">
        
        <div class="row">
            <div class="col-md-6">
                <h4 class=" tcolor f_bold">STRUKTUR PEMBIAYAAN</h4>
                <table class="table_content" >
                    <tbody>
                        <tr>
                            <td >Plafon Pinjaman</td>
                            <td >sd Rp 200.000.000</td>
                        </tr>
                        <tr>
                            <td>Tenor</td>
                            <td class="">3 bulan hingga 6 bulan</td>
                        </tr>
                        <tr>
                            <td >Lama Proses</td>
                            <td >3 hari</td>
                        </tr>
                        <tr>
                            <td >Area Domisili</td>
                            <td >Jabodetabek, Bandung, Surabaya, Solo, Yogyakarta, Bali</td>
                        </tr>
                    </tbody>
                </table>
                <br><br><br>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <h4 class=" tcolor f_bold">KRITERIA DEBITUR</h4>
                <table class="table_content" >
                    <thead>
                        <tr class="f_bold black">
                            <td class="black bg_color_1">Kriteria Umum</td>
                            <td class="black bg_color_3">Karyawan</td>
                            <td class="black bg_color_3">Wiraswasta</td>
                            <td class="black bg_color_3">Profesional</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr >
                            <td class="f_bold black" >WNI usia minimal 21 tahun dan maksimal 55 tahun</td>
                            <td class="tx_center">V</td>
                            <td class="tx_center">V</td>
                            <td class="tx_center">V</td>
                        </tr>
                        <tr >
                            <td class="f_bold black" >Penghasilan kotor minimal perbulan</td>
                            <td class="tx_center"> Rp. 3.000.000,- </td>
                            <td class="tx_center"> Rp. 5.000.000,- </td>
                            <td class="tx_center"> Rp. 5.000.000,- </td>
                        </tr>
                        <tr >
                            <td class="f_bold black" >Minimal limit kartu kredit</td>
                            <td class="tx_center"> Rp. 6.000.000,- </td>
                            <td class="tx_center"> Rp. 7.000.000,- </td>
                            <td class="tx_center"> Rp. 7.000.000,- </td>
                        </tr>
                        <tr >
                            <td class="f_bold black" >Minimal usaha atau bekerja</td>
                            <td class="tx_center"> 1 tahun dan status karyawan tetap </td>
                            <td class="tx_center"> 2 tahun </td>
                            <td class="tx_center"> 2 tahun </td>
                        </tr>
                        <tr >
                            <td class="f_bold black" >Masa aktif kartu kredit di bank lain</td>
                            <td class="tx_center"> 1 tahun </td>
                            <td class="tx_center"> 2 tahun </td>
                            <td class="tx_center"> 2 tahun </td>
                        </tr>
                    </tbody>
                </table>
                <br><br>

            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <h4 class=" tcolor f_bold">BIAYA - BIAYA YANG MUNCUL</h4>
                <table class="table_content" >
                    <tbody>
                        <tr >
                            <td rowspan="2" class="f_bold black" >Biaya Tahunan</td>
                            <td >Tahun ke 1</td>
                            <td >1,75% dari jumlah KTA yang disetujui DBS</td>
                        </tr>
                        <tr >
                            <td >Tahun ke 2 dst</td>
                            <td >Rp. 65.000,- per tahun</td>
                        </tr>
                        <tr >
                            <td class="f_bold black" colspan="2" >Biaya Administrasi</td>
                            <td >Rp. 399.000,-</td>
                        </tr>
                        <tr >
                            <td class="f_bold black" colspan="2" >Biaya transfer pencairan dana pinjaman**</td>
                            <td  >Rp. 30.000,-</td>
                        </tr>
                        <tr >
                            <td class="f_bold black" colspan="2" >Denda keterlambatan</td>
                            <td >Rp. 250.000,- per keterlambatan</td>
                        </tr>
                        <tr >
                            <td class="f_bold black" colspan="2" >Biaya pelunasan dipercepat</td>
                            <td >8% dari sisah pinjaman dan biaya lainnya</td>
                        </tr>
                        <tr >
                            <td class="f_bold black" colspan="2" >Biaya pembatalan</td>
                            <td >8% dari sisah pinjaman dan biaya lainnya</td>
                        </tr>
                    </tbody>
                </table>
                <br><br>
            </div>

            <div class="col-md-6">
                <h4 class=" tcolor f_bold">Pembayaran Angsuran</h4>
                <table class="table_content" >
                    <thead>
                        <tr >
                            <td class="f_bold black bg_color_1" >Tempat Pembayaran</td>
                            <td class="tx_center bg_color_3">Biaya Transaksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr >
                            <td class="f_bold black" >ATM BCA (menu transfer)</td>
                            <td class="tx_center">sesuai kebijakan bank terkait</td>
                        </tr>
                        <tr >
                            <td class="f_bold black" >Jaringan ALTO</td>
                            <td class="tx_center">Rp. 5.000,-</td>
                        </tr>
                        <tr >
                            <td class="f_bold black" >Jaringan ATM Bersama</td>
                            <td class="tx_center">sesuai kebijakan masing - masing bank</td>
                        </tr>
                        <tr >
                            <td class="f_bold black" >Transfer/Kliring*</td>
                            <td class="tx_center">sesuai kebijakan masing - masing bank</td>
                        </tr>
                        <tr >
                            <td class="f_bold black" >Counter PT Bank DBS Indonesia (Teller)*</td>
                            <td class="tx_center">Rp. 50.000,-</td>
                        </tr>
                    </tbody>
                </table>
                <br><br>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6 col-sm-6">
                <!-- Services box element modern style -->
                <div class="services_box services_box--modern sb--hasicon">
                    <!-- Service box wrapper -->
                    <div class="services_box__inner clearfix">
                        <!-- Icon content -->
                        <div class="services_box__icon">
                            <!-- Icon wrapper -->
                            <div class="services_box__icon-inner">
                                <!-- Icon = .icon-process3  -->
                                <span class="services_box__fonticon icon-process3"></span>
                            </div>
                            <!--/ Icon wrapper -->
                        </div>
                        <!--/ Icon content -->

                        <!-- Content -->
                        <div class="services_box__content">
                            <!-- Title -->
                            <h4 class="services_box__title" style="padding-bottom: 40px;">Keunggulan</h4>
                            <!--/ Title -->

                            <!-- List wrapper -->
                            <div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <!-- List -->
                                <ul class="services_box__list">
                                    <li><span class="services_box__list-text">Lebih simpel dan proses lebih cepat.</span></li>
                                    <li><span class="services_box__list-text">Bunga ringan (untuk pinjaman besar)</span></li>
                                    <li><span class="services_box__list-text">Persyaratan Lebih Simpel dan Proses Lebih Cepat 1 hari.</span></li>
                                    <li><span class="services_box__list-text">Data Bisa Dijemput Oleh Tim Marketing</span></li>
                                    <li><span class="services_box__list-text">Ada Banyak Promo Dan Crash Program</span></li>
                                </ul>
                                <!--/ List -->
                            </div>
                            <!--/ List wrapper -->
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Service box wrapper -->
                </div>
                <!--/ Services box element modern style -->
            </div>
            <!--/ col-md-6 col-sm-6 -->


        </div>

        <div class="row">
            <div class="col-md-6 col-sm-6">
                <!-- Services box element modern style -->
                <div class="services_box services_box--modern sb--hasicon">
                    <!-- Service box wrapper -->
                    <div class="services_box__inner clearfix">
                        <!-- Icon content -->
                        <div class="services_box__icon">
                            <!-- Icon wrapper -->
                            <div class="services_box__icon-inner">
                                <!-- Icon = .icon-process3  -->
                                <span class="services_box__fonticon icon-process3"></span>
                            </div>
                            <!--/ Icon wrapper -->
                        </div>
                        <!--/ Icon content -->

                        <!-- Content -->
                        <div class="services_box__content">
                            <!-- Title -->
                            <h4 class="services_box__title" style="padding-bottom: 40px;">Dokumen Persyaratan</h4>
                            <!--/ Title -->

                            <!-- List wrapper -->
                            <div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <!-- List -->
                                <ul class="services_box__list">
                                    <li><span class="services_box__list-text">Mengisi Form Aplikasi Resmi</span></li>
                                    <li><span class="services_box__list-text">Fotocopy KTP</span></li>
                                    <li><span class="services_box__list-text">Fotocopy Kartu Kredit</span></li>
                                    <li><span class="services_box__list-text">Fotocopy NPWP</span></li>
                                </ul>
                                <!--/ List -->
                            </div>
                            <!--/ List wrapper -->
                        </div>
                        <!--/ Content -->
                    </div>
                    <!--/ Service box wrapper -->
                </div>
                <!--/ Services box element modern style -->
            </div>
            <!--/ col-md-6 col-sm-6 -->

            <div class="col-md-6 col-sm-6">
                <h4 class="services_box__title" style="padding-bottom: 40px;">Lama Proses</h4>
                <strong>1 Hari Kerja</strong>
            </div>

        </div>


    </div>
    <!--/ container -->
</section>


<?php $this->load->view('pages/pengajuancepattanpajaminan');?>