<?php

//if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pagepost extends CI_Controller {

    var $data = array();

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }

    function pinjamantanpajaminan() {

        $is_ajax_request = $this->input->is_ajax_request();

        if ($is_ajax_request == false) {
            redirect();
        } else {
            $this->form_validation->set_rules('tx_nama', 'Nama', 'required');
            $this->form_validation->set_rules('tx_alamat', 'Alamat', 'required');
            $this->form_validation->set_rules('tx_hp', 'HP', 'required');
            $this->form_validation->set_rules('rad_penghasilan_perbulan', 'Penghasilan Perbulan', 'required');
            $this->form_validation->set_rules('tx_jumlah_cc', 'Jumlah CC', 'required');
            $this->form_validation->set_rules('rad_usia_cc', 'Usia CC', 'required');
            $this->form_validation->set_rules('tx_limit_cc', 'Limit CC', 'required');
            $this->form_validation->set_rules('tx_pinjaman', 'Jumlah Pinjaman', 'required');
            $this->form_validation->set_rules('tx_tenor', 'Tenor', 'required');
            $this->form_validation->set_rules('ck_kta[]', 'KTA', 'required');
            
            

            if ($this->form_validation->run() == FALSE) {

                $actobj['isSucces'] = FALSE;
                $actobj['msg'] =  'Data tidak lengkap';
                $enJSON = json_encode($actobj);
                echo $enJSON;
            } else {
                //trigger exception in a "try" block
                $actobj['isSucces'] = TRUE;
                $actobj['msg'] =  'Data berhasil dikirim';
                
                try {
                    $data = array(
                        'nama'	=> $this->input->post('tx_nama'),
                        'alamat'	=> $this->input->post('tx_alamat'),
                        'hp'	=> $this->input->post('tx_hp'),
                        'wa'	=> $this->input->post('tx_wa'),
                        'bbm'	=> $this->input->post('tx_bbm'),
                        'facebook'	=> $this->input->post('tx_fb'),

                        'penghasilanPerbulan' => $this->input->post('rad_penghasilan_perbulan'),
                        'jumlahKartuKreditAktif'	=> $this->input->post('tx_jumlah_cc'),
                        'usiaKartuKredit'	=> $this->input->post('rad_usia_cc'),
                        'limitKartuKredit'	=> $this->input->post('tx_limit_cc'),
                        'jumlahPinjaman'	=> $this->input->post('tx_pinjaman'),

                        'tenor'	=> $this->input->post('tx_tenor'),
                        'produkKTA'	=> implode(", ", $this->input->post('ck_kta[]'))
                    );
		 
                    $this->db->insert( 'tb_formonlinetanpajaminan',$data);

                }catch(Exception $e) {
                    $actobj['isSucces'] = FALSE;
                    $actobj['msg'] =  'Data tidak valid';
                }
                
                $enJSON = json_encode($actobj);
                echo $enJSON;
            }
        }
    }
    
    function pinjamandenganjaminan() {

        $is_ajax_request = $this->input->is_ajax_request();

        if ($is_ajax_request == false) {
            redirect();
        } else {
            $this->form_validation->set_rules('tx_nama', 'Nama', 'required');
            $this->form_validation->set_rules('tx_alamat', 'Alamat', 'required');
            $this->form_validation->set_rules('tx_hp', 'HP', 'required');
            /*$this->form_validation->set_rules('rad_penghasilan_perbulan', 'Penghasilan Perbulan', 'required');
            $this->form_validation->set_rules('ddl_kepemilikan_rumah', 'Kepemilikan Rumah', 'required');
            $this->form_validation->set_rules('rad_nilai_pinjaman', 'Nilai Pinjaman', 'required');
            $this->form_validation->set_rules('rad_tenor', 'Tenor', 'required');
            $this->form_validation->set_rules('ddl_jenis_agunan', 'Jenis Agunan', 'required');
            $this->form_validation->set_rules('tx_merek', 'Merke', 'required');
            $this->form_validation->set_rules('tx_tipe', 'Tipe', 'required');
            $this->form_validation->set_rules('tx_tahun', 'Tahun', 'required');
            $this->form_validation->set_rules('tx_plat_no', 'Plat No', 'required');
            $this->form_validation->set_rules('ddl_kondisi_pajak', 'Kondisi Pajak', 'required');
            $this->form_validation->set_rules('ddl_bpkb_atas_nama', 'BPKB Nama', 'required');
            $this->form_validation->set_rules('ddl_posisi_bpkb', 'Posisi BPKB', 'required');
            $this->form_validation->set_rules('ddl_kebutuhan_prioritas', 'Kebutuhan Prioritas', 'required');
            $this->form_validation->set_rules('ck_multifinance[]', 'Multifinance', 'required');
            $this->form_validation->set_rules('rad_penghasilan_perbulan', 'Penghasilan Perbulan', 'required');*/

            if($this->input->post('rad_nilai_pinjaman') == "nominal"){
                $this->form_validation->set_rules('tx_nilai_pinjaman', 'nilai pinjaman', 'required');
            }
            
            if ($this->form_validation->run() == FALSE) {

                $actobj['isSucces'] = FALSE;
                $actobj['msg'] =  'Data tidak lengkap';
                $enJSON = json_encode($actobj);
                echo $enJSON;
            } else {
                //trigger exception in a "try" block
                $actobj['isSucces'] = TRUE;
                $actobj['msg'] =  'Data berhasil dikirim';
                
                $jumlahPinjaman = $this->input->post('rad_nilai_pinjaman');
                
                if($jumlahPinjaman == "nominal"){
                    $jumlahPinjaman = $this->input->post('tx_nilai_pinjaman');
                }
                
                try {
                    $data = array(
                        'nama'	=> $this->input->post('tx_nama'),
                        'alamat' => $this->input->post('tx_alamat'),
                        'hp'	=> $this->input->post('tx_hp'),
                        'wa'	=> $this->input->post('tx_wa'),
                        'bbm'	=> $this->input->post('tx_bbm'),
                        'facebook' => $this->input->post('tx_fb'),

                        'penghasilanPerbulan' => $this->input->post('rad_penghasilan_perbulan'),
                        'kepemilikanRumah'  => $this->input->post('ddl_kepemilikan_rumah'),
                        'jumlahPinjaman'    => $jumlahPinjaman,
                        'tenor'	=> $this->input->post('rad_tenor'),
                        'jenisAgunan'	=> $this->input->post('ddl_jenis_agunan'),

                        'merek'	=> $this->input->post('tx_merek'),
                        'tipe'	=> $this->input->post('tx_tipe'),
                        'tahun'	=> $this->input->post('tx_tahun'),
                        'platNomor'	=> $this->input->post('tx_plat_no'),
                        'kondisiPajak'	=> $this->input->post('ddl_kondisi_pajak'),
                        'bpkbAtasNama'	=> $this->input->post('ddl_bpkb_atas_nama'),
                        'posisiBpkb'	=> $this->input->post('ddl_posisi_bpkb'),
                        'kebutuhanPrioritas'	=> $this->input->post('ddl_kebutuhan_prioritas'),
                        'multifinance'	=> implode(", ", $this->input->post('ck_multifinance[]')),
                    );
		 
                    $this->db->insert( 'tb_formonlinedenganjaminan',$data);

                }catch(Exception $e) {
                    $actobj['isSucces'] = FALSE;
                    $actobj['msg'] =  'Data tidak valid';
                }
                
                $enJSON = json_encode($actobj);
                echo $enJSON;
            }
        }
    }

    function careers() {

        $is_ajax_request = $this->input->is_ajax_request();

        if ($is_ajax_request == false) {
            redirect();
        } else {
            $this->form_validation->set_rules('tx_namadepan', 'Nama', 'required');
            $this->form_validation->set_rules('tx_namabelakang', 'Nama Belakang', 'required');
            $this->form_validation->set_rules('tx_telepon', 'HP', 'required');
            $this->form_validation->set_rules('tx_email', 'Email', 'required');
            $this->form_validation->set_rules('tx_subject', 'Subjek', 'required');
            $this->form_validation->set_rules('tx_pesan', 'Pesan', 'required');
            
            if ($this->form_validation->run() == FALSE) {

                $actobj['isSucces'] = FALSE;
                $actobj['msg'] =  'Data tidak lengkap';
                $enJSON = json_encode($actobj);
                echo $enJSON;
            } else {
                //trigger exception in a "try" block
                $actobj['isSucces'] = TRUE;
                $actobj['msg'] =  'Data berhasil dikirim';
                
                
                try {
                    $data = array(
                        'namaDepan'	=> $this->input->post('tx_namadepan'),
                        'namaBelakang' => $this->input->post('tx_namabelakang'),
                        'telepon'	=> $this->input->post('tx_telepon'),
                        'email'	=> $this->input->post('tx_email'),
                        'subjek'	=> $this->input->post('tx_subject'),
                        'pesan' => $this->input->post('tx_pesan')
                    );
		 
                    $this->db->insert( 'tb_careersform',$data);

                }catch(Exception $e) {
                    $actobj['isSucces'] = FALSE;
                    $actobj['msg'] =  'Data tidak valid';
                }
                
                $enJSON = json_encode($actobj);
                echo $enJSON;
            }
        }
    }

}

?>