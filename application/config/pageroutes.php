<?php 
$route["home"] = "page/index/home";
$route["home/(:any)"] = "page/index/home/%1";
$route["service"] = "page/index/service";
$route["service/(:any)"] = "page/index/service/%1";
$route["about-us"] = "page/index/about-us";
$route["about-us/(:any)"] = "page/index/about-us/%1";
$route["contact-us"] = "page/index/contact-us";
$route["contact-us/(:any)"] = "page/index/contact-us/%1";
$route["faq"] = "page/index/faq";
$route["faq/(:any)"] = "page/index/faq/%1";
$route["portpolio"] = "page/index/portpolio";
$route["portpolio/(:any)"] = "page/index/portpolio/%1";
$route["product1"] = "page/index/product1";
$route["product1/(:any)"] = "page/index/product1/%1";
$route["product2"] = "page/index/product2";
$route["product2/(:any)"] = "page/index/product2/%1";
$route["careers"] = "page/index/careers";
$route["careers/(:any)"] = "page/index/careers/%1";
$route["ajukan-sekarang-jaminan"] = "page/index/ajukan-sekarang-jaminan";
$route["ajukan-sekarang-jaminan/(:any)"] = "page/index/ajukan-sekarang-jaminan/%1";
$route["ajukan-sekarang-tanpa-jaminan"] = "page/index/ajukan-sekarang-tanpa-jaminan";
$route["ajukan-sekarang-tanpa-jaminan/(:any)"] = "page/index/ajukan-sekarang-tanpa-jaminan/%1";
$route["bandingkan-tanpa-jaminan"] = "page/index/bandingkan-tanpa-jaminan";
$route["bandingkan-tanpa-jaminan/(:any)"] = "page/index/bandingkan-tanpa-jaminan/%1";
$route["bandingkan-jaminan"] = "page/index/bandingkan-jaminan";
$route["bandingkan-jaminan/(:any)"] = "page/index/bandingkan-jaminan/%1";
$route["lihat-penawaran-cimb"] = "page/index/lihat-penawaran-cimb";
$route["lihat-penawaran-cimb/(:any)"] = "page/index/lihat-penawaran-cimb/%1";
$route["lihat-penawaran-dbs"] = "page/index/lihat-penawaran-dbs";
$route["lihat-penawaran-dbs/(:any)"] = "page/index/lihat-penawaran-dbs/%1";
$route["lihat-penawaran-mnc-finance"] = "page/index/lihat-penawaran-mnc-finance";
$route["lihat-penawaran-mnc-finance/(:any)"] = "page/index/lihat-penawaran-mnc-finance/%1";
$route["lihat-penawaran-olimpindo"] = "page/index/lihat-penawaran-olimpindo";
$route["lihat-penawaran-olimpindo/(:any)"] = "page/index/lihat-penawaran-olimpindo/%1";
$route["lihat-penawaran-sms-finance"] = "page/index/lihat-penawaran-sms-finance";
$route["lihat-penawaran-sms-finance/(:any)"] = "page/index/lihat-penawaran-sms-finance/%1";
$route["lihat-penawaran-bfi-finance"] = "page/index/lihat-penawaran-bfi-finance";
$route["lihat-penawaran-bfi-finance/(:any)"] = "page/index/lihat-penawaran-bfi-finance/%1";
$route["lihat-penawaran-radana"] = "page/index/lihat-penawaran-radana";
$route["lihat-penawaran-radana/(:any)"] = "page/index/lihat-penawaran-radana/%1";
$route["lihat-penawaran-permata-kta"] = "page/index/lihat-penawaran-permata-kta";
$route["lihat-penawaran-permata-kta/(:any)"] = "page/index/lihat-penawaran-permata-kta/%1";
$route["lihat-penawaran-qnb"] = "page/index/lihat-penawaran-qnb";
$route["lihat-penawaran-qnb/(:any)"] = "page/index/lihat-penawaran-qnb/%1";
$route["lihat-penawaran-sc"] = "page/index/lihat-penawaran-sc";
$route["lihat-penawaran-sc/(:any)"] = "page/index/lihat-penawaran-sc/%1";
?>